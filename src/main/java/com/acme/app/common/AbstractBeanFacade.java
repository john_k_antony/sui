package com.acme.app.common;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.BeanFacade;
import jka.suifwk.api.CallContext;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.web.security.UserProfileData;

import org.apache.commons.beanutils.PropertyUtils;

import com.acme.app.api.samplebean.AbstractBean;

public abstract class AbstractBeanFacade implements BeanFacade {
	
	private static List<String> defaultLightWeightObjectParams = null;
	
	static {
		defaultLightWeightObjectParams = new ArrayList<String>();
		PropertyDescriptor[] descs = PropertyUtils.getPropertyDescriptors(AbstractBean.class);
		if(descs != null && descs.length > 0) {
			for(PropertyDescriptor pDesc : descs) {
				if(pDesc.getPropertyType().isPrimitive() || Serializable.class.isAssignableFrom(pDesc.getPropertyType())) {
					defaultLightWeightObjectParams.add(pDesc.getName());
				}
			}
		}				
	}

	@Override
	public final APICallResponse read(CallContext context) {
		try {
			return readInternal(context);
		} catch(Throwable e) {
			throw new SUIRuntimeException(e);
		} finally {
			//do some resource cleanup
		}
	}	
	
	public UserProfileData getUserData(CallContext context) {
		Object userData = context.getContextParam(SUIConstants.ATTR_USER_DATA);
		if ((userData != null && userData instanceof UserProfileData) ) {
			return (UserProfileData)userData;
		}
		return null;
	}		
	
	public Map<String, Object> getLightWeightObject(Object bean, List<String> params) {
		List<String> allParams = new ArrayList<String>();
		allParams.addAll(getDefaultLightWeightObjectAttributes());		
		if(params != null) {
			allParams.addAll(params);
		}
		return APIUtils.getLightWeightObject(bean, allParams);
	}
	
	public List<String> getDefaultLightWeightObjectAttributes() {		
		return Collections.unmodifiableList(defaultLightWeightObjectParams);
	}
	
	protected abstract APICallResponse readInternal(CallContext context);
		
}
