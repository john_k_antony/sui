package com.acme.app.api.samplebean;

public enum EmployeeType {
	CEO,
	MANAGER,
	DEVELOPER,
	NOT_SET
}