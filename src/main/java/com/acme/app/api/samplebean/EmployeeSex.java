package com.acme.app.api.samplebean;

public enum EmployeeSex {
	MALE,
	FEMALE,
	NA
}
