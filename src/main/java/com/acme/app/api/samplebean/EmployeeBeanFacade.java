package com.acme.app.api.samplebean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.CallContext;
import jka.suifwk.exception.SUIUserException;

import com.acme.app.api.samplebean.EmployeeBean.EmployeeSkill;
import com.acme.app.common.AbstractBeanFacade;

public class EmployeeBeanFacade extends AbstractBeanFacade {

	private static final String FACADE_NAME = "employee";

	@SuppressWarnings("unused")
	private static final List<String> FIELDS_TO_IGNORE;

	private static final String EMPLOYEE_BEAN_PROPERTY_EMP = "employee";
	private static final String EMPLOYEE_BEAN_PROPERTY_STATE = "state";
	private static final String EMPLOYEE_STATE_PERMANENT = "permanent";
	private static final String MANAGER_NAME_FIELD = "managerName";

	private static final Map<Integer, EmployeeBean> employeeData;

	private static final AtomicInteger empIdCounter = new AtomicInteger(0);

	static {

		employeeData = readEmployeeData();

		List<String> tempList = new ArrayList<String>();
		tempList.add("id");
		FIELDS_TO_IGNORE = Collections.unmodifiableList(tempList);
	}

	private static synchronized void writeEmployeeData() {
		try {
			File f = new File("employeeData.ser");
			if (f.exists()) {
				f.delete();
			}
			FileOutputStream fout = new FileOutputStream("employeeData.ser");
			ObjectOutputStream oos = new ObjectOutputStream(fout);
			oos.writeObject(employeeData);
			oos.close();
			fout.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	private static synchronized Map<Integer, EmployeeBean> readEmployeeData() {
		Map<Integer, EmployeeBean> retData = new HashMap<Integer, EmployeeBean>();
		try {
			File f = new File("employeeData.ser");
			if (f.exists()) {
				FileInputStream fin = new FileInputStream("employeeData.ser");
				ObjectInputStream ois = new ObjectInputStream(fin);
				retData = (Map<Integer, EmployeeBean>) ois.readObject();
				Integer maxId = Collections.max(retData.keySet());
				empIdCounter.set(maxId);
				ois.close();
				fin.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return retData;
	}

	@Override
	protected APICallResponse readInternal(CallContext context) {
		long t1 = Calendar.getInstance().getTimeInMillis();
		List<String> propsToFetch = APIUtils.getPayloadDataAsList(context,
				"fields", String.class, false);
		int employeeId = APIUtils.getPayloadDataAsInteger(context, "id", false,
				-1);
		int managerId = APIUtils.getPayloadDataAsInteger(context, "managerId",
				false, -1);
		;

		List<Map<String, Object>> lwObjList = new ArrayList<Map<String, Object>>();
		Collection<EmployeeBean> employeeList = null;
		EmployeeBean employee = null;

		if (managerId > 0) {
			employeeList = getAllSubs(managerId);
		} else if (employeeId > 0) {
			employee = getEmployeeBean(employeeId);
		} else {
			employeeList = employeeData.values();
		}
		if (employeeList != null) {
			for (EmployeeBean em : employeeList) {
				Map<String, Object> lwObject = getLightWeightObject(em,
						propsToFetch);
				addManagerName(em, lwObject);
				lwObjList.add(lwObject);
			}
		} else if (employee != null) {
			Map<String, Object> lwObject = getLightWeightObject(employee,
					propsToFetch);
			addManagerName(employee, lwObject);
			lwObjList.add(lwObject);
		}

		Map<String, Object> metaData = new HashMap<String, Object>();
		long t2 = Calendar.getInstance().getTimeInMillis();
		metaData.put("TimeToExecute", (t2 - t1));

		return new APICallResponse(lwObjList, metaData);
	}

	protected void addManagerName(EmployeeBean emp, Map<String, Object> lwObject) {
		if (emp.getType().equals(EmployeeType.CEO)) {
			lwObject.put(MANAGER_NAME_FIELD, emp.getName());
		} else {
			if (emp.getManagerId() > 0) {
				EmployeeBean manager = getEmployeeBean(emp.getManagerId());
				if (manager != null) {
					lwObject.put(MANAGER_NAME_FIELD, manager.getName());
				}
			}
		}
	}

	@Override
	public APICallResponse create(CallContext context) {
		return update(context);
	}

	@Override
	public APICallResponse update(CallContext context) {
		List<Integer> updatedEmployeeIdList = new ArrayList<Integer>();

		Object dataMapObject = APIUtils
				.getPayloadData(context, "dataMap", true);

		if (dataMapObject != null) {
			if (dataMapObject instanceof List<?>) {
				@SuppressWarnings("unchecked")
				List<Map<String, Object>> propMapList = (List<Map<String, Object>>) dataMapObject;
				for (Map<String, Object> propMap : propMapList) {
					EmployeeBean updatedBean = updateEmployeeBeanProperties(propMap);
					if (updatedBean != null) {
						updatedEmployeeIdList.add(updatedBean.getId());
					}
				}
			} else if (dataMapObject instanceof Map<?, ?>) {
				@SuppressWarnings("unchecked")
				Map<String, Object> propMap = (Map<String, Object>) dataMapObject;
				EmployeeBean updatedBean = updateEmployeeBeanProperties(propMap);
				if (updatedBean != null) {
					updatedEmployeeIdList.add(updatedBean.getId());
				}
			}
		}
		writeEmployeeData();
		return new APICallResponse(updatedEmployeeIdList);
	}

	@Override
	public APICallResponse delete(CallContext context) {
		List<Integer> deletedEmployeeIdList = new ArrayList<Integer>();

		List<Integer> idList = APIUtils.getPayloadDataAsList(context, "id",
				Integer.class, false);

		if (idList != null) {
			for (int employeeId : idList) {
				if (employeeId != -1) {
					deleteEmployeeBean(employeeId);
					deletedEmployeeIdList.add(employeeId);
				}
			}
		}
		writeEmployeeData();
		return new APICallResponse(deletedEmployeeIdList);
	}

	protected EmployeeBean getEmployeeBean(int empId) {
		if (empId > 0) {
			EmployeeBean empBean = employeeData.get(empId);
			if (empBean == null) {
				throw new SUIUserException(
						"Cannot find employee. Employee might be deleted already!");
			} else {
				return empBean;
			}
		} else {
			return null;
		}
	}

	protected List<EmployeeBean> getAllSubs(int empId) {
		List<EmployeeBean> retList = new ArrayList<EmployeeBean>();
		Collection<EmployeeBean> allEmps = employeeData.values();
		for (EmployeeBean emp : allEmps) {
			if (emp.getManagerId() == empId) {
				retList.add(emp);
			}
		}
		return retList;
	}

	protected boolean deleteEmployeeBean(int empId) {
		EmployeeBean empToRemove = employeeData.remove(empId);
		if (empToRemove == null) {
			throw new SUIUserException(
					"Cannot delete employee. Employee not found!.");
		} else {
			if (empToRemove.getType().equals(EmployeeType.CEO)) {
				throw new SUIUserException("Cannot delete CEO!.");
			} else {
				int managerId = empToRemove.getManagerId();
				List<EmployeeBean> allSubs = getAllSubs(empToRemove.getId());
				if (allSubs != null && allSubs.size() > 0) {
					for (EmployeeBean sub : allSubs) {
						sub.setManagerId(managerId);
					}
				}
				employeeData.remove(empId);
			}
		}
		return empToRemove != null;
	}

	protected EmployeeBean updateEmployeeBean(EmployeeBean updatedEmployeeBean) {

		boolean isCreate = false;
		if (updatedEmployeeBean != null) {
			if (updatedEmployeeBean.getId() <= 0) {
				updatedEmployeeBean.setId(EmployeeBean.INVALID_ID);
				isCreate = true;
			} else {
				getEmployeeBean(updatedEmployeeBean.getId());
			}
		} else {
			throw new SUIUserException(
					"Unable to update employee info. Null employee info returned");
		}
		long currTime = Calendar.getInstance().getTimeInMillis();
		if (isCreate) {
			int employeeId = empIdCounter.addAndGet(1);
			updatedEmployeeBean.setId(employeeId);
			updatedEmployeeBean.setCreateTime(currTime);
			List<EmployeeSkill> skills = new ArrayList<EmployeeSkill>();
			// skills.add(new EmployeeSkill("Java", 8));
			// skills.add(new EmployeeSkill("C++", 6));
			updatedEmployeeBean.setSkills(skills);
		} else {
			updatedEmployeeBean.setUpdateTime(currTime);
		}

		if (updatedEmployeeBean.getType() == null
				|| updatedEmployeeBean.getType().equals(EmployeeType.NOT_SET)) {
			throw new SUIUserException(
					"Cannot update/create employee. Employee type not set!");
		}

		if (updatedEmployeeBean.getType().equals(EmployeeType.CEO)) {
			for (EmployeeBean empBean : employeeData.values()) {
				if (empBean.getType().equals(EmployeeType.CEO)
						&& empBean.getId() != updatedEmployeeBean.getId()) {
					throw new SUIUserException(
							"Cannot update/create employee. Only one CEO is allowed. There is already one CEO exist - '"
									+ empBean.getName() + "'!");
				}
			}
		}

		employeeData.put(updatedEmployeeBean.getId(), updatedEmployeeBean);
		return updatedEmployeeBean;
	}

	protected EmployeeBean updateEmployeeBeanProperties(
			Map<String, Object> dataMap) {

		int employeeId = (Integer)dataMap.get("id");

		EmployeeBean employeeBean = null;

		if (employeeId != -1) {
			employeeBean = getEmployeeBean(employeeId);
		}
		Iterator<String> dataMapKeys = dataMap.keySet().iterator();

		while (dataMapKeys.hasNext()) {
			String key = dataMapKeys.next();
			String dataValue = dataMap.get(key).toString();
			switch (key) {
			case EMPLOYEE_BEAN_PROPERTY_STATE:
				switch (dataValue) {
				case EMPLOYEE_STATE_PERMANENT:
					employeeBean.setState(EmployeeState.PERMANENT);
					employeeBean.setUpdateTime(Calendar.getInstance()
							.getTimeInMillis());
					break;
				}
				break;
			case EMPLOYEE_BEAN_PROPERTY_EMP:
				employeeBean = APIUtils.mergeOrCreateObjectFromSerializedData(
						dataValue, employeeBean, EmployeeBean.class);
				updateEmployeeBean(employeeBean);
				break;

			}
		}
		return employeeBean;
	}

	@Override
	public String getName() {
		return FACADE_NAME;
	}
}