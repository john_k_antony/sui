package com.acme.app.api.samplebean;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import jka.suifwk.exception.SUIRuntimeException;


@XmlRootElement
@XmlType(name = "employee")
@XmlAccessorType(XmlAccessType.FIELD)
public class EmployeeBean extends AbstractBean {
	
	public EmployeeBean() {
		this.orderedValues = new String[2];
		this.orderedValues[0] = "Val1";
		this.orderedValues[1] = "Val2";
	}
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 5740668961637168748L;
	
	public int getManagerId() {
		return managerId;
	}
	public void setManagerId(int managerId) {
		this.managerId = managerId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getDob() {
		return dob;
	}
	public void setDob(long dob) {
		this.dob = dob;
	}
	public EmployeeSex getSex() {
		return sex;
	}
	public void setSex(EmployeeSex sex) {
		this.sex = sex;
	}
	public EmployeeType getType() {
		return type;
	}
	public void setType(EmployeeType type) {
		this.type = type;
	}
	public EmployeeState getState() {
		return state;
	}
	public void setState(EmployeeState state) {
		this.state = state;
	}	
	public List<EmployeeSkill> getSkills() {
		return skills;
	}
	public void setSkills(List<EmployeeSkill> skills) {
		this.skills = skills;
	}	
	
	private int managerId;
	private String name;
	private long dob;
	private EmployeeSex sex = EmployeeSex.NA;
	private EmployeeType type = EmployeeType.NOT_SET;
	private EmployeeState state = EmployeeState.PROBATION;
	private List<EmployeeSkill> skills;
	
	@SuppressWarnings("unused")
	private String privateField = "PrivateFieldData";
	
	private String orderedValues[];
	
	  /**
	   * @return the orderedValues
	   */
	  public String[] getOrderedValues()
	  {		  
	    return orderedValues;
	  }

	  /**
	   * @param orderedValues
	   *          the orderedValues to set
	   * @throws MlpCheckedException
	   */
	  public void setOrderedValues(String[] orderedValues)
	  {
	    if ((orderedValues != null) &&
	        (orderedValues.length > 0) &&
	        !type.equals(EmployeeType.CEO))
	      throw new SUIRuntimeException("Issue with JAXB");
	    this.orderedValues = orderedValues;
	  }
	
	public static class EmployeeSkill implements Serializable {
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 8096801213433136667L;
		
		String skillName;
		int rating;
		
		public EmployeeSkill() {
			
		}
		
		public EmployeeSkill(String skillName, int rating) {
			super();
			this.skillName = skillName;
			this.rating = rating;
		}		
		public String getSkillName() {
			return skillName;
		}
		public void setSkillName(String skillName) {
			this.skillName = skillName;
		}
		public int getRating() {
			return rating;
		}
		public void setRating(int rating) {
			this.rating = rating;
		}
	}

}
