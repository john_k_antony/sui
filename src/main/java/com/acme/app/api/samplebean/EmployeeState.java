package com.acme.app.api.samplebean;

public enum EmployeeState {
	PROBATION,
	PERMANENT
}
