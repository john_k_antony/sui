package com.acme.app.api.samplebean;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "bean")
@XmlAccessorType(XmlAccessType.FIELD)
public abstract class AbstractBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -23176130396770502L;
	
	public static final int INVALID_ID = -1;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getCreateTime() {
		return createTime;
	}
	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}
	public long getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(long updateTime) {
		this.updateTime = updateTime;
	}
	private int id;
	private long createTime;
	private long updateTime;

}
