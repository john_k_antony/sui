package com.acme.app.api.test;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import com.acme.app.api.samplebean.EmployeeBean;
import com.acme.app.api.samplebean.EmployeeType;

public class EmployeeBeanTester {

	public static void main(String args[]) {
		try {
			new EmployeeBeanTester().testJaxB();
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void testJaxB() throws JAXBException {
		EmployeeBean ceo = new EmployeeBean();
		ceo.setName("CEO Employee");
		ceo.setType(EmployeeType.CEO);
		ceo.setId(1);

		StringWriter strWriter = new StringWriter();
		JAXBContext jaxbCtx = JAXBContext.newInstance(EmployeeBean.class);
		Marshaller m = jaxbCtx.createMarshaller();
		m.marshal(ceo, strWriter);
		System.out.println(strWriter.toString());
	}
}
