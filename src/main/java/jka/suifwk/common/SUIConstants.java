package jka.suifwk.common;

public interface SUIConstants {

	public static final String ATTR_USER_DATA = "ATTR_USER_DATA";
	public static final String ATTR_SESSION_ID = "ATTR_SESSION_ID";
	public static final String ATTR_WEB_MESSAGE_TRANSPORT = "ATTR_WEB_MESSAGE_TRANSPORT";
	public static final String REQUEST_QUERY_STRING = "REQUEST_QUERY_STRING";
	public static final String REQUEST_PATH = "REQUEST_PATH";
	public static final String REQUEST_PAYLOAD = "REQUEST_PAYLOAD";
	public static final String REQUEST_PAYLOAD_RAW = "REQUEST_PAYLOAD_RAW";
	public static final String REQUEST_FILES = "REQUEST_FILES";
	public static final String REQUEST_IS_MULTIPART = "REQUEST_IS_MULTIPART";
	public static final String JSON_NODE_IS_PARAMETRIZED_GET = "IS_PARAMETRIZED_GET";
	public static final String JSON_NODE_PAYLOAD_DATA = "payload";
	public static final String FILE_NAME = "FILE_NAME";
		
	public static final String LONG_RUNNING_TASK_UPDATE = "LR_UPDATE";
	public static final String LONG_RUNNING_TASK_START = "LR_START";
	
	public static final String INTERNAL_ERROR_MESSAGE = "An internal error has occured. Please contact your administrator.";
	
	public static final int STATUS_CODE_OK = 1200;
	public static final int STATUS_CODE_INTERNAL_SERVER_ERROR = 1500;
	public static final int STATUS_CODE_SESSION_INVALID = 1501;
	public static final int STATUS_CODE_CSRF_TOKEN_INVALID = 1502;	
	
	public static final int STATUS_CODE_API_CALL_ERROR = 3500;
		
	public static final int STATUS_CODE_LOGIN_ERROR_INVALID_CREDENTIALS = 2500;
	public static final int STATUS_CODE_LOGOUT_ERROR_CANNOT_CLEANUP_RESOURCES = 2501;	

	public static final int STATUS_CODE_USER_PROFILE_DATA_FOUND = 2502;
	public static final int STATUS_CODE_USER_PROFILE_DATA_NOT_FOUND = 2503;	
	
}
