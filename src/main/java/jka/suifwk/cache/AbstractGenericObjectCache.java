package jka.suifwk.cache;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/*
 * This cache object is used for maintaining an in memory cache of object
 * of type 'E'
 */
public abstract class AbstractGenericObjectCache<E> implements
		GenericObjectCache<E> {

	private List<E> cacheList;
	private int pageSize;
	private int totalNumberOfObjects;
	private CacheDataProvider<E> cacheDataProvider;
	private String cacheId;
	private int numOfPagesToCache;
	private int numOfPrevPagesToCache;
	private int numOfForwPagesToCache;

	private int cacheStartIndex = 0;
	private int cacheEndIndex = 0;

	private int statCacheHit = 0;
	private int statCacheFault = 0;

	@SuppressWarnings("unused")
	private static Logger logger = Logger.getLogger(AbstractGenericObjectCache.class.getName());

	public AbstractGenericObjectCache(String cacheId, int pageSize,
			CacheDataProvider<E> cacheDataProvider) {
		if (cacheId == null) {
			throw new IllegalArgumentException("CacheId cannot be null");
		}
		this.cacheId = cacheId;
		this.pageSize = pageSize;
		this.totalNumberOfObjects = cacheDataProvider.getTotalCount();
		this.numOfPagesToCache = getNumberOfPagesToCache();
		numOfPrevPagesToCache = (int) Math.round(Math
				.floor(this.numOfPagesToCache / 2));
		numOfForwPagesToCache = this.numOfPagesToCache - numOfPrevPagesToCache;
		this.cacheList = getCacheList(this.pageSize * this.numOfPagesToCache);
		this.cacheDataProvider = cacheDataProvider;
	}

	public String getCacheId() {
		return this.cacheId;
	}

	public void loadCache(String cacheId, int startIndex) {
		if (cacheId == null) {
			throw new IllegalArgumentException("CacheId cannot be null");
		}
		this.cacheId = cacheId;
		this.cacheList.clear();

		int updatedStartIndex = startIndex;

		if(startIndex > 0) {
			updatedStartIndex = startIndex - numOfPrevPagesToCache*pageSize;
			if(updatedStartIndex < 0) {
				updatedStartIndex = 0;
			}
		}

		int updatedEndIndex = updatedStartIndex + numOfPagesToCache*pageSize;

		if(updatedEndIndex > this.totalNumberOfObjects) {
			updatedEndIndex = this.totalNumberOfObjects;
		}

		if(updatedEndIndex > 0) {
			updatedEndIndex--;
		}

		this.cacheStartIndex = updatedStartIndex;
		this.cacheEndIndex = updatedEndIndex;

		List<E> data = null;
		if(updatedEndIndex > 0) {
			data = this.cacheDataProvider.getData(updatedStartIndex, (updatedEndIndex - updatedStartIndex + 1));
		}
		if(data != null) {
			this.cacheList.addAll(data);
		} else {

		}
	}

	public int getCacheStartIndex(String cacheId) {
		if (this.cacheId != null && this.cacheId.equals(cacheId)) {
			return this.cacheStartIndex;
		} else {
			throw new RuntimeException("The current cache id : '" + this.cacheId
					+ "' is different from requested cache id : '" + cacheId + "'");
		}
	}

	public int getCacheEndIndex(String cacheId) {
		if (this.cacheId != null && this.cacheId.equals(cacheId)) {
			return this.cacheEndIndex;
		} else {
			throw new RuntimeException("The current cache id : '" + this.cacheId
					+ "' is different from requested cache id : '" + cacheId + "'");
		}
	}

	public int getTotalCount(String cacheId) {
		if (this.cacheId != null && this.cacheId.equals(cacheId)) {
			return this.totalNumberOfObjects;
		} else {
			throw new RuntimeException("The current cache id : '" + this.cacheId
					+ "' is different from requested cache id : '" + cacheId + "'");			
		}		
	}
	
	public List<E> getData(String cacheId, final int startIndex, final int reqCount) {

		int count = reqCount;
		if(startIndex > this.totalNumberOfObjects) {
			throw new RuntimeException("Start index : " + startIndex + " is greater than total number of records : " + this.totalNumberOfObjects);
		}
		if((startIndex + reqCount) > this.totalNumberOfObjects) {
			count = this.totalNumberOfObjects - startIndex;
		}

		boolean getDataFromOriginalDataSource = false;

		if(this.cacheId != null && this.cacheId.equals(cacheId)) {

			//startIndex is the index of the item in the actual data set that is requested for.
			//this.cacheStartIndex is the actual index of the first item in the cache list.
			//cacheIndexOffsetStart is the index of the first requested item (starting from startIndex from the cache list.
			int cacheIndexOffsetStart = startIndex - this.cacheStartIndex;
			int cacheIndexOffsetEnd = cacheIndexOffsetStart + count;

			//the following variables tells whether the cache has been modified to service the currene request due to a pagefault in the current cache
			boolean updatedCacheForStartIndex = false;
			boolean updatedCacheForEndIndex = false;

			//negative cacheIndexOffsetStart denotes that the request has come for some objects that are not in the current cache.
			//the current cache is holding objects from index that is greater than requested start index
			//at this point, we need to reload the cache with respect to the current start index
			//reloading will fill the cache with 'numOfPrevPagesToCache' pages previous to the current start index
			// and numOfForwPagesToCache pages next to the current start index.
			if(cacheIndexOffsetStart < 0) {
				this.loadCache(this.cacheId, startIndex);
				//after realoading the cache, recompute the cacheIndexOffsetStart with the modified cacheStartIndex
				cacheIndexOffsetStart = startIndex - this.cacheStartIndex;
				updatedCacheForStartIndex = true;
				if(cacheIndexOffsetStart < 0) {
					//Something has gone wrong here. We cannot serve the request from cache.
					//Try to serve request from cache data provider.
					getDataFromOriginalDataSource = true;
				}
			}

			if(getDataFromOriginalDataSource != true) {
				//We have got the cacheIndexOffsetStart right.
				//Now compute the cacheIndexOffsetEnd
				cacheIndexOffsetEnd = cacheIndexOffsetStart + count;
				if((startIndex + count) - 1 > this.cacheEndIndex) {
					//request is for some data that is outside of the current cache.
					if(updatedCacheForStartIndex == true) {
						//if we have already modified loaded the cache to fix the start index and
						//now the end index is also out of bounds, some one is asking for a chunk of data
						//that cannot be server through cache.
						//Set the flag to get the data from original data source
						getDataFromOriginalDataSource = true;
					} else {
						//request is for data that is outside the current cache.
						//load the cache to load the next set of records
						this.loadCache(this.cacheId, startIndex);
						updatedCacheForEndIndex = true;
						if(this.cacheStartIndex > startIndex) {
							//now after the reload, if the cache start index is greater than the requested start index,
							//the updated cache cannot serve the request.
							//Set the flag to get the data from original data source
							getDataFromOriginalDataSource = true;
						} else {
							cacheIndexOffsetStart = startIndex - this.cacheStartIndex;
							cacheIndexOffsetEnd = cacheIndexOffsetStart + count;
							if((startIndex + count) - 1 > this.cacheEndIndex) {
								//again the current cache doesn't have enough data to serve the request
								//Set the flag to get the data from original data source
								getDataFromOriginalDataSource = true;
							}
						}
					}
				} else {
					if(cacheIndexOffsetEnd > this.cacheList.size()) {
						getDataFromOriginalDataSource = true;
					}
				}
			}
			if(getDataFromOriginalDataSource) {
				statCacheFault++;
				return this.cacheDataProvider.getData(startIndex, count);
			} else {				
				List<E> retData = new ArrayList<E>(this.cacheList.subList(cacheIndexOffsetStart, cacheIndexOffsetEnd));
				statCacheHit++;
				if(updatedCacheForEndIndex || updatedCacheForStartIndex) {
					//cache updated already, no need to update it again
				} else {
					if((((startIndex != this.cacheStartIndex) && (startIndex - this.cacheStartIndex) < pageSize))
							|| ((this.cacheEndIndex != startIndex) && (this.cacheEndIndex - startIndex) < pageSize)) {
						//update the cache to pivot around the current startIndex;
						this.loadCache(this.cacheId, startIndex);
					}
				}
				return retData;
			}
		} else {
			throw new RuntimeException("The current cache id : '" + this.cacheId
					+ "' is different from requested cache id : '" + cacheId + "'");
		}
	}

	protected abstract List<E> getCacheList(int initCapacity);

	protected abstract int getNumberOfPagesToCache();

	public String toString() {
		return "cacheId=" + this.cacheId + "; cacheStartIndex="
				+ this.cacheStartIndex + ";cacheSize=" + this.cacheList.size()
				+ "; pageSize=" + this.pageSize + "; totalNumberOfObjects="
				+ this.totalNumberOfObjects + "; numOfPagesToCache="
				+ this.numOfPagesToCache + "; numOfPrevPagesToCache="
				+ this.numOfPrevPagesToCache + "; numOfForwPagesToCache="
				+ this.numOfForwPagesToCache;
	}
}
