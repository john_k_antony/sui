package jka.suifwk.cache;

import java.util.List;

/*
 * This cache object is used for maintaining an in memory cache of object 
 * of type 'E'
 */
public interface GenericObjectCache<E> {
		
	public void loadCache(String cacheId, int startIndex);
	
	public int getCacheStartIndex(String cacheId);
	
	public int getCacheEndIndex(String cacheId);
	
	public int getTotalCount(String cacheId);
	
	public String getCacheId();
	
	public List<E> getData(String cacheId, int startIndex, int count);
			
	public static interface CacheDataProvider<E> {
		int getTotalCount();
		List<E> getData(int startIndex, int count);
	}	

}
