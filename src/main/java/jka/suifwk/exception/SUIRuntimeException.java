package jka.suifwk.exception;

public class SUIRuntimeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2883770021616652801L;
	
	public SUIRuntimeException(Throwable t) {
		super(t);
	}
	
	public SUIRuntimeException(String message) {
		super(message);
	}
	
	public SUIRuntimeException(String message, Throwable t) {
		super(message, t);
	}	

}
