package jka.suifwk.messaging;

import java.util.Calendar;
import java.util.concurrent.atomic.AtomicLong;

public enum MessageFactory {
	INSTANCE;
	
	private AtomicLong messageIdCounter;
	
	private MessageFactory() {
		messageIdCounter = new AtomicLong(0);
	}
		
	public Message create(String topic, Object data) {
		return new InternalWebMessage(topic, data);
		
	}
	
	public Message create(String id, String topic, Object data) {
		return new InternalWebMessage(id, topic, data);		
	}
		
	private class InternalWebMessage implements Message {
		
		long timestamp;
		String topic;
		Object data;
		String id;
				
		protected InternalWebMessage(String topic, Object data) {
			this("WM:" + messageIdCounter.incrementAndGet(), topic, data);
		}
		
		protected InternalWebMessage(String id, String topic, Object data) {
			this.timestamp = Calendar.getInstance().getTimeInMillis();
			this.topic = topic;
			this.data = data;
			this.id = id;
		}
		
		@Override
		public String getMessageId() {
			return this.id;
		}

		@Override
		public Object getMessageData() {
			return this.data;
		}

		@Override
		public String getMessageTopic() {
			return this.topic;
		}

		@Override
		public long getMessageTimestamp() {
			return this.timestamp;
		}
	}
}
