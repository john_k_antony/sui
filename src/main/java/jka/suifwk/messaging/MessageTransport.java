package jka.suifwk.messaging;

import jka.suifwk.api.CallContext;
import jka.suifwk.web.session.ApplicationSubject;

public interface MessageTransport {

	public void push(CallContext context, Message message);
	public void push(ApplicationSubject subject, Message message);
	public void pushToAll(Message message);
		
	public void subscribe(String topic, MessageListener subscriber);
	
	public void unsubscribe(String topic, MessageListener subscriber);
	
}
