package jka.suifwk.messaging;

public interface MessageListener {
	public void onWebMessage(Message message);
}
