package jka.suifwk.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

import jka.suifwk.exception.SUIUserException;

import org.apache.commons.codec.binary.Base64;

public enum Utils {
	INSTANCE;

	public Throwable getRootCause(final Throwable th) {
		Throwable t = th;
		while (t.getCause() != null && t.getCause() instanceof SUIUserException) {
			t = t.getCause();
		}
		return t;
	}

	/**
	 * Serializes the input object (including any referenced objects
	 * transitively) using the specified serialization method, optionally
	 * compresses it and encodes the serialized bytes into a base-64
	 * representation. Returns null if the input is null.
	 * 
	 * @param inputObject
	 *            - input object to be marshalled
	 * @param compress
	 *            - whether the serialized object should be compressed
	 * @param serializationType
	 *            - serialization method
	 * @return base-64 encoded string of the serialized object
	 * @throws Exception
	 */
	public static String marshallBase64(Object inputObject, boolean compress)
			throws Exception {
		/* serialize */
		byte[] objectByteStream = null;
		objectByteStream = serializeUsingJava(inputObject);
		/* compress */
		if (compress) {
			objectByteStream = compress(objectByteStream);
		}
		/* encode and return */
		return marshallByteArrayToBase64(objectByteStream);
	}

	/**
	 * Decodes the base-64 encoded string, optionally decompresses it and
	 * deserializes it into an object using the specified serialization method.
	 * Returns null if the input string is null.
	 * 
	 * @param base64String
	 *            - base-64 string to be unmarshalled
	 * @param decompress
	 *            - whether the decoded string should be decompressed
	 * @param serializationType
	 *            - serialization method
	 * @return deserialized object
	 * @throws Exception
	 */
	public static Object unmarshallBase64(String base64String,
			boolean decompress) throws Exception {
		Object actualObj = null;

		/* decode */
		byte[] objectByteStream = unmarshallBase64ToByteArray(base64String);

		/* decompress */
		if (decompress) {
			objectByteStream = decompress(objectByteStream);
		}
		actualObj = deSerializeUsingJava(objectByteStream);
		return actualObj;
	}

	/**
	 * Compresses the input bytes and encodes them into a base-64 string.
	 * Returns null if the input is null.
	 * 
	 * @param byteStream
	 *            - input bytes
	 * @return compressed and encoded string
	 */
	public static String compressAndmarshallToBase64(byte[] byteStream)
			throws IOException {
		String base64String = null;

		if (byteStream != null) {
			base64String = Base64.encodeBase64String(compress(byteStream));
		}

		return base64String;
	}

	/**
	 * Decodes a base-64 string and uncompresses it. Returns null if the input
	 * is null.
	 * 
	 * @param base64String
	 *            - input string
	 * @return decoded and uncompressed bytes
	 */
	public static byte[] unmarshallAndDeCompressBase64(String base64String)
			throws IOException, DataFormatException {
		byte[] byteStream = null;

		if (base64String != null) {
			byte[] decodedBytes = Base64.decodeBase64(base64String);
			byteStream = decompress(decodedBytes);
		}
		return byteStream;
	}

	public static boolean matchWild(String wild, String name) {
		if (wild.equals("*")) {
			return true;
		}
		wild = wild.replaceAll("\\.", "\\.");
		wild = wild.replaceAll("\\?", ".");
		wild = wild.replaceAll("\\\\", "\\\\");
		wild = wild.replaceAll("\\/", "\\/");
		wild = wild.replaceAll("\\*", "(.+?)");
		return name.matches(wild);
	}

	private static byte[] serializeUsingJava(Object inputObject)
			throws IOException {
		byte[] output = null;

		if (inputObject != null) {
			ByteArrayOutputStream bout = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(bout);
			out.writeObject(inputObject);
			output = bout.toByteArray();
		}

		return output;
	}

	private static Object deSerializeUsingJava(byte[] objectByteStream)
			throws IOException, ClassNotFoundException {
		Object output = null;

		if (objectByteStream != null) {
			ObjectInputStream oin = new ObjectInputStream(
					new ByteArrayInputStream(objectByteStream));
			output = oin.readObject();
		}

		return output;
	}

	private static byte[] compress(byte[] data) throws IOException {
		byte[] output = null;

		if (data != null && data.length > 0) {
			Deflater deflater = new Deflater();
			deflater.setInput(data);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(
					data.length);

			deflater.finish();
			byte[] buffer = new byte[1024];
			while (!deflater.finished()) {
				int count = deflater.deflate(buffer); // returns the generated
														// code... index
				if (count == 0) {
					deflater.finish();
					break;
				}
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
			output = outputStream.toByteArray();

			deflater.end();
		}

		return output;
	}

	private static byte[] decompress(byte[] data) throws IOException,
			DataFormatException {
		byte[] output = null;

		if (data != null && data.length > 0) {
			Inflater inflater = new Inflater();
			inflater.setInput(data);

			ByteArrayOutputStream outputStream = new ByteArrayOutputStream(
					data.length);
			byte[] buffer = new byte[1024];
			while (!inflater.finished()) {
				int count = inflater.inflate(buffer);
				if (count == 0) {
					break;
				}
				outputStream.write(buffer, 0, count);
			}
			outputStream.close();
			output = outputStream.toByteArray();

			inflater.end();
		}

		return output;
	}

	private static String marshallByteArrayToBase64(byte[] byteStream)
			throws IOException {
		String base64String = null;

		if (byteStream != null) {
			base64String = Base64.encodeBase64String(byteStream);
		}

		return base64String;
	}

	private static byte[] unmarshallBase64ToByteArray(String base64String)
			throws IOException {
		byte[] byteStream = null;

		if (base64String != null) {
			byteStream = Base64.decodeBase64(base64String);
		}
		return byteStream;
	}
}
