package jka.suifwk.web.api;

import javax.servlet.http.HttpServletRequest;

import jka.suifwk.api.CallContext;


public interface ApplicationCallContextHandler {
	public void init();
	public CallContext getCallContext(HttpServletRequest request);
}
