package jka.suifwk.web.api;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jka.suifwk.api.CallContext;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.web.session.SessionManager;
import jka.suifwk.web.utils.WebUtils;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.codehaus.jackson.JsonNode;

public enum CallContextManager {

	INSTANCE;

	private static Logger logger = Logger.getLogger(CallContextManager.class
			.getName());

	protected ApplicationCallContextHandler callContextHandler = null;

	private CallContextManager() {

	}

	public void setHandler(ApplicationCallContextHandler callContextHandler) {
		callContextHandler.init();
		this.callContextHandler = callContextHandler;
	}

	public void setHandlerClassName(String callContextHandlerClassName) {
		if (callContextHandlerClassName != null) {
			try {
				setHandler((ApplicationCallContextHandler) Class.forName(
						callContextHandlerClassName).newInstance());
			} catch (ReflectiveOperationException e) {
				logger.log(Level.SEVERE,
						"Cannot load call context handler class : "
								+ callContextHandlerClassName, e);
			}
		} else {
			logger.log(Level.SEVERE,
					"Cannot load call context handler class. Null handler class name provided.");
		}
	}

	public CallContext getCallContext(HttpServletRequest request) {
		if (this.callContextHandler == null) {
			this.callContextHandler = new DefaultCallContextHandler();
		}
		return this.callContextHandler.getCallContext(request);
	}

	private class DefaultCallContextHandler implements
			ApplicationCallContextHandler {

		@Override
		public void init() {
		}

		private JsonNode getRequestPayloadJson(String jsonString) {
			try {
				return WebUtils.getJsonObject(jsonString);
			} catch (Exception e) {
				// TODO:: ignore
			}
			return null;
		}

		@Override
		public CallContext getCallContext(final HttpServletRequest request) {
			return new CallContext() {

				HttpSession session = SessionManager.INSTANCE.getSession(
						request, false);

				String requestPayload;
				JsonNode jsonPayload;

				final boolean isMultiPart = ServletFileUpload
						.isMultipartContent(request);

				List<FileItem> multiPartItems = null;
				List<FileItem> fileItems = null;
				boolean isFilesProcessed = false;
				boolean isMultiPartProcessed = false;
				boolean isRequestPayloadProcessed = false;

				private void processRequestPayload() {
					if (!isMultiPart) {
						if (!isRequestPayloadProcessed) {
							requestPayload = WebUtils.getPayloadRaw(request);
							jsonPayload = getRequestPayloadJson(requestPayload);
							isRequestPayloadProcessed = true;
						}
					}
				}

				@Override
				public Object getContextParam(String param) {
					try {
						if (param.equals(SUIConstants.REQUEST_IS_MULTIPART)) {
							return isMultiPart;
						}
						if (param.equals(SUIConstants.REQUEST_QUERY_STRING)) {
							return request.getQueryString();
						} else if (param.equals(SUIConstants.REQUEST_PATH)) {
							return request.getPathInfo().substring(1);
						} else if (param
								.equals(SUIConstants.REQUEST_PAYLOAD_RAW)) {
							processRequestPayload();
							return requestPayload;
						} else if (param.equals(SUIConstants.REQUEST_PAYLOAD)) {
							processRequestPayload();
							return jsonPayload;
						} else {
							if (isMultiPart && !isMultiPartProcessed) {
								try {
									multiPartItems = new ServletFileUpload(
											new DiskFileItemFactory())
											.parseRequest(request);
									isMultiPartProcessed = true;
								} catch (Exception e) {
									logger.log(
											Level.WARNING,
											"Cannot process multipart request!",
											e);
								}
							}
							if (param.equals(SUIConstants.REQUEST_FILES)) {
								if (isMultiPart && !isFilesProcessed) {
									fileItems = new ArrayList<FileItem>();
									for (FileItem item : multiPartItems) {
										if (!item.isFormField()) {
											fileItems.add(item);
										}
									}
									isFilesProcessed = true;
								}
								return fileItems;
							} else {
								Object fieldvalue = null;
								if (isMultiPart) {
									for (FileItem item : multiPartItems) {
										if (item.isFormField()) {
											String fieldname = item
													.getFieldName();
											if (fieldname.equals(param)) {
												fieldvalue = item.getString();
												break;
											}
										}
									}
								} else {
									fieldvalue = request.getParameter(param);
								}
								if (fieldvalue == null) {
									fieldvalue = request.getAttribute(param);
								}
								if (fieldvalue == null) {
									if (session != null) {
										fieldvalue = session
												.getAttribute(param);
									}
								}
								return fieldvalue;
							}
						}
					} catch (Exception e) {
						return null;
					}
				}

				@Override
				public Object getSessionParam(String param) {
					if (session != null) {
						return session.getAttribute(param);
					} else {
						return null;
					}
				}

				@Override
				public boolean setSessionParam(String param, Object o) {
					return setSessionParam(param, o, -1L);
				}

				@Override
				public boolean setSessionParam(String param, Object o,
						long expirationTime) {
					if (session != null) {
						session.setAttribute(param, o);
						return true;
					} else {
						return false;
					}
				}
			};
		}
	}
}
