package jka.suifwk.web.security;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jka.suifwk.api.BeanFacadeRegistry;
import jka.suifwk.api.CommandHandlerRegistry;
import jka.suifwk.utils.Utils;
import jka.suifwk.web.auth.AuthenticationManager;
import jka.suifwk.web.session.SessionManager;

public class ApplicationAccessHandlerFilter implements Filter {

	private static final String CONFIG_LOGIN_PAGE = "login-form";
	private static final String CONFIG_EXCLUDE_URI_PATTERNS = "exclude-uri-patterns";

	private static final String CONFIG_APPLICATION_SESSION_HANDLER_CLASS_NAME = "app-session-handler-class";
	private static final String CONFIG_APPLICATION_AUTHENTICATION_HANDLER_CLASS_NAME = "app-auth-handler-class";

	private String loginPage = null;
	private String appSessionHandlerClass = null;
	private String appAuthHandlerClass = null;

	private List<String> excludeUrlPatterns = new LinkedList<String>();

	private static Logger logger = Logger
			.getLogger(ApplicationAccessHandlerFilter.class.getName());

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest servletRequest,
			ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {

		HttpServletRequest httpRequest = ((HttpServletRequest) servletRequest);
		HttpSession existingSession = SessionManager.INSTANCE.getSession(
				httpRequest, false);

		String uri = httpRequest.getRequestURI();

		if (isExcluded(uri)) {
			// URI is part of excluded / "free access". Let it go.
			logger.log(Level.INFO, "URI : " + uri
					+ " is part of exluded list. Letting it go.");
			filterChain.doFilter(servletRequest, servletResponse);
			return;
		} else {
		}

		HttpServletResponse httpResponse = ((HttpServletResponse) servletResponse);

		httpResponse.setDateHeader("Expires", 0);
		httpResponse.setHeader("Cache-Control",
				"no-cache, no-store, must-revalidate");
		httpResponse.setHeader("Pragma", " no-cache");

		if (existingSession != null
				&& AuthenticationManager.INSTANCE.isAutherized(httpRequest)) {
			// There is a valid session. Continue with filtering
			logger.log(Level.INFO,
					"HTTP session found and the user is autherized. Letting it go.");
			filterChain.doFilter(servletRequest, servletResponse);
		} else {
			// There is a no valid session. Forward login page to the user.
			logger.log(
					Level.INFO,
					"NO Valid HTTP session found or there is no autherized user in the session!. Forwarding to login page.");
			httpResponse.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
			RequestDispatcher rd = httpRequest.getRequestDispatcher(loginPage);
			rd.forward(servletRequest, servletResponse);
			return;
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

		loginPage = filterConfig.getInitParameter(CONFIG_LOGIN_PAGE);
		appSessionHandlerClass = filterConfig
				.getInitParameter(CONFIG_APPLICATION_SESSION_HANDLER_CLASS_NAME);
		appAuthHandlerClass = filterConfig
				.getInitParameter(CONFIG_APPLICATION_AUTHENTICATION_HANDLER_CLASS_NAME);

		if (appSessionHandlerClass != null) {
			SessionManager.INSTANCE.setHandlerClassName(appSessionHandlerClass);
		}

		if (appAuthHandlerClass != null) {
			AuthenticationManager.INSTANCE
					.setHandlerClassName(appAuthHandlerClass);
		}

		String excludeUriPatterns = filterConfig
				.getInitParameter(CONFIG_EXCLUDE_URI_PATTERNS);
		StringTokenizer st = new StringTokenizer(excludeUriPatterns, ",");
		while (st.hasMoreElements()) {
			String pattern = st.nextToken();
			excludeUrlPatterns.add(pattern.trim());
		}

		BeanFacadeRegistry.INSTANCE.scanBeanFacades();
		CommandHandlerRegistry.INSTANCE.scanCommandHandlers();
	}

	private boolean isExcluded(String uri) {
		for (String pattern : excludeUrlPatterns) {
			if (Utils.matchWild(pattern, uri)) {
				return true;
			}
			// if (uri.indexOf(pattern) > -1) {
			// return true;
			// }
		}
		return false;
	}
}
