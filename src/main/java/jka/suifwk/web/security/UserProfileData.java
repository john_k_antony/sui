package jka.suifwk.web.security;

import java.io.Serializable;
import java.util.List;
import java.util.Locale;

public class UserProfileData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9190142528155517034L;

	public String getUserName() {
		return userName;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public String getUserLocale() {
		return userLocale;
	}

	public String getUserLanguage() {
		return userLanguage;
	}

	public UserNameSpace getUserNameSpace() {
		return userNameSpace;
	}

	public String getCurrentRole() {
		return currentRole;
	}

	public List<String> getUserPrivileges() {
		return userPrivileges;
	}

	public List<String> getUserRoles() {
		return userRoles;
	}

	public boolean isSuperUser() {
		return isSuperUser;
	}

	public String userName;
	public String userFirstName;
	public String userLastName;
	public String userEmail;
	public String userLocale;
	public String userLanguage;
	public UserNameSpace userNameSpace;	

	// For persona based UI
	public String currentRole;

	public List<String> userPrivileges;
	public List<String> userRoles;
	public boolean isSuperUser;

	public UserProfileData(String userName, String userFirstName, String userLastName,
			String userEmail, UserNameSpace userNameSpace, String currentRole,
			List<String> userPrivileges, List<String> userRoles,
			boolean isSuperUser) {
		this(userName, userFirstName, userLastName, userEmail, Locale.getDefault().toString(),
				Locale.getDefault().getLanguage(), userNameSpace, currentRole,
				userPrivileges, userRoles, isSuperUser);
	}

	public UserProfileData(String userName, String userFirstName, String userLastName, 
			String userEmail, String userLocale, String userLanguage,
			UserNameSpace userNameSpace, String currentRole,
			List<String> userPrivileges, List<String> userRoles,
			boolean isSuperUser) {
		super();
		this.userName = userName;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.userEmail = userEmail;
		this.userLocale = userLocale;
		this.userLanguage = userLanguage;
		this.currentRole = currentRole;
		this.userNameSpace = userNameSpace;
		this.userPrivileges = userPrivileges;
		this.userRoles = userRoles;
		this.isSuperUser = isSuperUser;
	}
	
	public static class UserNameSpace implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = -5138005418462599715L;

		public String getNamespace() {
			return namespace;
		}

		public int getNamespaceId() {
			return namespaceId;
		}

		String namespace;
		int namespaceId;
		
		public UserNameSpace(int namespaceId, String namespace) {
			this.namespace = namespace;
			this.namespaceId = namespaceId;
		}
	}

}
