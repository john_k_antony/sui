package jka.suifwk.web.security;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jka.suifwk.web.auth.AuthenticationManager;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.utils.WebUtils;

public class LoginServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2201911013280894851L;

	private static final String STATUS_CODE_LOGIN_ERROR_INVALID_COMMAND = "Login.Error.1";

	private static final String COMMAND_LOGIN = "doLogin";
	private static final String COMMAND_LOGOUT = "doLogout";

	private static Logger logger = Logger.getLogger(LoginServlet.class
			.getName());

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException {

		WebResponse webResponse = null;

		String loginCommand = getLoginCommand(request);

		if (loginCommand != null) {
			try {
				if (loginCommand.equals(COMMAND_LOGIN)) {
					webResponse = AuthenticationManager.INSTANCE.login(request);
				} else if (loginCommand.equals(COMMAND_LOGOUT)) {
					webResponse = AuthenticationManager.INSTANCE
							.logout(request);
				}
			} catch (Exception e) {
				logger.log(Level.SEVERE, "Login Exception", e);
				webResponse = WebUtils.generateInternalErrorResponse(e);
			}
		} else {
			webResponse = WebUtils
					.generateInternalErrorResponse(new IllegalArgumentException(
							STATUS_CODE_LOGIN_ERROR_INVALID_COMMAND));
		}

		WebUtils.writeWebResponse(response, webResponse, true);
	}

	protected String getLoginCommand(HttpServletRequest request) {
		String contextPath = request.getContextPath();
		String reqURI = request.getRequestURI();

		if (reqURI.startsWith(contextPath)) {
			return reqURI.substring(contextPath.length() + 1);
		} else {
			return null;
		}
	}
}
