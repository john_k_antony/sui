package jka.suifwk.web.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.exception.SUIUserException;
import jka.suifwk.utils.Utils;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.security.UserProfileData;
import jka.suifwk.web.session.ApplicationSubject;
import jka.suifwk.web.session.ApplicationSubjectType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class WebUtils {

	private static Logger logger = Logger.getLogger(WebUtils.class.getName());

	public static String getJsonString(Object o)
			throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		return om.writeValueAsString(o);
	}

	public static JsonNode getJsonObject(String json)
			throws JsonGenerationException, JsonMappingException, IOException {
		if (json != null && json.trim().length() > 0) {
			ObjectMapper om = new ObjectMapper();
			return om.readTree(json);
		} else {
			return null;
		}
	}

	public static String getPayloadRaw(HttpServletRequest request) {
		try {
			ServletInputStream inputStream = request.getInputStream();
			InputStreamReader reader = new InputStreamReader(inputStream);
			StringBuffer out = new StringBuffer();
			try {
				char[] chars = new char[8092];
				int n = 0;
				while ((n = reader.read(chars)) != -1) {
					out.append(chars, 0, n);
				}
			} finally {
			}
			return out.toString();
		} catch (IOException ioe) {
			logger.log(Level.WARNING, "Unable to get request payload.", ioe);
		}
		return null;
	}

	public static JsonNode getJsonPayload(HttpServletRequest request)
			throws JsonGenerationException, JsonMappingException, IOException {
		return getJsonObject(getPayloadRaw(request));
	}

	public static Map<String, String> getRequestDataMap(
			HttpServletRequest request) throws JsonGenerationException,
			JsonMappingException, IOException {
		JsonNode inputJsonPayload = WebUtils.getJsonPayload(request);
		JsonNode inputJsonData = null;
		if (inputJsonPayload != null) {
			inputJsonData = inputJsonPayload
					.get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
		}
		Map<String, String> retDataMap = new HashMap<String, String>();

		if (inputJsonData != null) {
			Iterator<String> keys = inputJsonData.getFieldNames();
			while (keys.hasNext()) {
				String k = keys.next();
				retDataMap.put(k, inputJsonData.get(k).getTextValue());
			}
		}
		return retDataMap;
	}

	public static void writeWebResponse(HttpServletResponse response,
			WebResponse webResponse, boolean writeCacheControl) {

		try {

			PrintWriter writer = response.getWriter();

			writer.print(getJsonString(webResponse));

			if (writeCacheControl) {
				// do not cache response
				response.setDateHeader("Expires", 0);
				response.setHeader("Cache-Control",
						"no-cache, no-store, must-revalidate");
				response.setHeader("Pragma", " no-cache");
			}
		} catch (Exception ex) {
			logger.log(Level.SEVERE, "Could not write web response", ex);
		}
	}

	public static WebResponse generateInternalErrorResponse(Throwable e) {
		logger.log(Level.SEVERE, "Internal Error", e);
		Throwable rootCause = getRootCause(e);
		String errorMessage = null;
		if (rootCause instanceof SUIUserException) {
			errorMessage = rootCause.getLocalizedMessage();
		} else {
			errorMessage = SUIConstants.INTERNAL_ERROR_MESSAGE;
		}
		return new WebResponse(SUIConstants.STATUS_CODE_INTERNAL_SERVER_ERROR,
				errorMessage);
	}

	public static Throwable getRootCause(final Throwable th) {
		return Utils.INSTANCE.getRootCause(th);
	}

	public static Object createWebResponse(
			final APICallResponse apiCallResponse, HttpServletRequest request,
			HttpServletResponse response) {
		return createWebResponse(apiCallResponse, null, request, response);
	}

	public static Object createWebResponse(
			final APICallResponse apiCallResponse, String redirectURL,
			HttpServletRequest request, HttpServletResponse response) {
		if (apiCallResponse != null) {
			Map<String, Object> metadata = apiCallResponse.getMetaData(false);
			if (metadata != null && response != null) {
				Set<String> keys = metadata.keySet();
				for (String key : keys) {
					Object val = metadata.get(key);
					if (val != null) {
						response.setHeader(key, val.toString());
					}
				}
			}
			if (apiCallResponse.getData() instanceof InputStream) {
				String fileName = (metadata != null && metadata
						.get(SUIConstants.FILE_NAME) != null) ? metadata.get(
						SUIConstants.FILE_NAME).toString() : null;
				response.setHeader(
						"Content-Disposition",
						"attachment; filename=\""
								+ fileName.replaceAll("<>\\\"/:|\\?\\*", "_")
								+ "\"");
				StreamingOutput stream = new StreamingOutput() {
					@Override
					public void write(OutputStream output) throws IOException,
							WebApplicationException {
						try {
							pipe((InputStream) apiCallResponse.getData(),
									output);
						} catch (Exception e) {
							throw new WebApplicationException(e);
						}
					}
				};
				return stream;
			} else {
				WebResponse wr = new WebResponse(apiCallResponse, redirectURL);
				return wr;
			}
		} else {
			return generateInternalErrorResponse(new SUIRuntimeException(
					"Null API Response!"));
		}
	}

	public static ApplicationSubject getUserSubject(String userName,
			String nameSpace) {
		return new ApplicationSubject(ApplicationSubjectType.USER, userName,
				nameSpace);
	}

	public static ApplicationSubject getUserSubject(
			ApplicationSubjectType type, String userName, String nameSpace) {
		return new ApplicationSubject(type, userName, nameSpace);
	}

	public static ApplicationSubject getUserSubject(UserProfileData upd) {
		return getUserSubject(ApplicationSubjectType.USER, upd.getUserName(),
				upd.getUserNameSpace() != null ? upd.getUserNameSpace()
						.getNamespace() : null);
	}

	private static void pipe(InputStream is, OutputStream os)
			throws IOException {
		int n;
		byte[] buffer = new byte[1024];
		while ((n = is.read(buffer)) > -1) {
			os.write(buffer, 0, n); // Don't allow any extra bytes to creep in,
									// final write
		}
		is.close();
		os.close();
	}
}
