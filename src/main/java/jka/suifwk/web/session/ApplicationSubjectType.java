package jka.suifwk.web.session;

public enum ApplicationSubjectType {
	USER,
	GROUP
}
