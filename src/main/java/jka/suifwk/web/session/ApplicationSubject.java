package jka.suifwk.web.session;

public class ApplicationSubject {
	protected ApplicationSubjectType type;
	protected String name;
	protected String nameSpace;
	
	public ApplicationSubject(ApplicationSubjectType type, String name, String nameSpace) {
		this.type = type;
		this.name = name;
		this.nameSpace = nameSpace;
	}
	
	public ApplicationSubjectType getType() {
		return type;
	}
	public String getName() {
		return name;
	}
	public String getNameSpace() {
		return nameSpace;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((nameSpace == null) ? 0 : nameSpace.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApplicationSubject other = (ApplicationSubject) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (nameSpace == null) {
			if (other.nameSpace != null)
				return false;
		} else if (!nameSpace.equals(other.nameSpace))
			return false;
		if (type != other.type)
			return false;
		return true;
	}	
	
}
