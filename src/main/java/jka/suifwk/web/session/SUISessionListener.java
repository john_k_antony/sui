package jka.suifwk.web.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

public class SUISessionListener implements HttpSessionListener {

	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		SessionManager.INSTANCE.onSessionCreated(sessionEvent.getSession());
		
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		SessionManager.INSTANCE.onSessionDestroyed(sessionEvent.getSession());		
	}

}
