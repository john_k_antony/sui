package jka.suifwk.web.session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.web.security.UserProfileData;
import jka.suifwk.web.utils.WebUtils;

public enum SessionManager {
	INSTANCE;

	protected ApplicationSessionHandler sessionHandler;

	private static Logger logger = Logger.getLogger(SessionManager.class
			.getName());

	private HashMap<String, WebContext> webContextMap;
	private HashMap<ApplicationSubject, Set<String>> subjectSessionMap;

	private SessionManager() {
		webContextMap = new HashMap<String, WebContext>();
		subjectSessionMap = new HashMap<ApplicationSubject, Set<String>>();
	}

	public void setHandler(ApplicationSessionHandler sessionHandler) {
		this.sessionHandler = sessionHandler;
	}

	public void setHandlerClassName(String sessionHandlerClassName) {
		if (sessionHandlerClassName != null) {
			try {
				setHandler((ApplicationSessionHandler) Class.forName(
						sessionHandlerClassName).newInstance());
			} catch (ReflectiveOperationException e) {
				logger.log(Level.SEVERE, "Cannot load session handler class : "
						+ sessionHandlerClassName, e);
			}
		} else {
			logger.log(Level.SEVERE,
					"Cannot load session handler class. Null handler class name provided.");
		}
	}

	public HttpSession getSession(HttpServletRequest request,
			boolean createNewSession) {
		if (this.sessionHandler != null) {
			HttpSession session = this.sessionHandler.getSession(request,
					createNewSession);
			if (session != null) {
				session.setAttribute(SUIConstants.ATTR_SESSION_ID,
						session.getId());
			}
			return session;
		} else {
			throw new RuntimeException(
					"Cannot create session. No Application Session Handler defined.");
		}

	}

	void onSessionCreated(HttpSession session) {
		WebContext wc = new InternalWebContext(session);
		this.webContextMap.put(session.getId(), wc);
	}

	void onSessionDestroyed(HttpSession session) {
		this.webContextMap.remove(session.getId());
	}

	public WebContext getWebContext(HttpServletRequest request) {
		return request != null ? getWebContext(request.getSession(false))
				: null;
	}

	public WebContext getWebContext(HttpSession s) {
		if (s != null) {
			return getWebContext(s.getId());
		} else {
			return null;
		}
	}

	public WebContext getWebContext(String sessionId) {
		return this.webContextMap.get(sessionId);
	}

	public List<WebContext> getWebContext(ApplicationSubject subject) {
		List<WebContext> wcs = new ArrayList<WebContext>();
		Set<String> sIds = this.subjectSessionMap.get(subject);
		for (String sId : sIds) {
			wcs.add(this.getWebContext(sId));
		}
		return wcs;
	}

	public void registerUser(UserProfileData upd, String sId) {
		if (sId != null) {
			WebContext wc = this.getWebContext(sId);
			if (wc != null) {
				HttpSession session = wc.getSession();
				if (session != null) {
					session.setAttribute(SUIConstants.ATTR_USER_DATA, upd);
					ApplicationSubject subject = WebUtils.getUserSubject(upd);
					Set<String> sIds = this.subjectSessionMap.get(subject);
					if (sIds == null) {
						sIds = new LinkedHashSet<String>();
						this.subjectSessionMap.put(subject, sIds);
					}
					sIds.add(sId);
				}
			}
		}
	}

	public void unregisterUser(UserProfileData upd, String sId) {
		if (sId != null) {
			ApplicationSubject subject = WebUtils.getUserSubject(upd);
			Set<String> sIds = this.subjectSessionMap.get(subject);
			if (sIds != null) {
				sIds.remove(sId);
			}
		}
	}

	private class InternalWebContext implements WebContext {

		HttpSession session;

		protected InternalWebContext(HttpSession session) {
			this.session = session;
		}

		@Override
		public HttpSession getSession() {
			return this.session;
		}
	}

}
