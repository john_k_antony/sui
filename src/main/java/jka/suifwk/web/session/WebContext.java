package jka.suifwk.web.session;

import javax.servlet.http.HttpSession;

public interface WebContext {
	public HttpSession getSession();
}