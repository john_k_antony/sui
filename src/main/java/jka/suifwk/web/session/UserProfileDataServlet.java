package jka.suifwk.web.session;

import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.utils.WebUtils;

public class UserProfileDataServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2201911013280894851L;

	@SuppressWarnings("unused")
	private static Logger logger = Logger
			.getLogger(UserProfileDataServlet.class.getName());

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException {

		WebResponse webResponse = null;

		Object userProfileData = request.getSession().getAttribute(
				SUIConstants.ATTR_USER_DATA);

		if (userProfileData != null) {
			webResponse = new WebResponse(SUIConstants.STATUS_CODE_OK, null, userProfileData);
		} else {
			webResponse = WebUtils
					.generateInternalErrorResponse(new RuntimeException("User profile not found!."));
		}
		WebUtils.writeWebResponse(response, webResponse, true);
	}
}
