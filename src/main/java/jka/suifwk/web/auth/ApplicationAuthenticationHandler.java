package jka.suifwk.web.auth;

import javax.servlet.http.HttpServletRequest;

import jka.suifwk.web.rest.WebResponse;

public interface ApplicationAuthenticationHandler {
	public void init();
	public WebResponse login(HttpServletRequest request);
	public WebResponse logout(HttpServletRequest request);	
}
