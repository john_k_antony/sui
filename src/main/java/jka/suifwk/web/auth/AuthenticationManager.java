package jka.suifwk.web.auth;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.web.rest.WebResponse;
import jka.suifwk.web.security.UserProfileData;
import jka.suifwk.web.session.SessionManager;

public enum AuthenticationManager {
	INSTANCE;

	private static Logger logger = Logger.getLogger(AuthenticationManager.class
			.getName());

	protected ApplicationAuthenticationHandler authHandler;

	private AuthenticationManager() {

	}

	public void setHandler(ApplicationAuthenticationHandler authHandler) {
		authHandler.init();
		this.authHandler = authHandler;
	}

	public void setHandlerClassName(String authHandlerClassName) {
		if (authHandlerClassName != null) {
			try {
				setHandler((ApplicationAuthenticationHandler) Class.forName(
						authHandlerClassName).newInstance());
			} catch (ReflectiveOperationException e) {
				logger.log(Level.SEVERE,
						"Cannot load authentication handler class : "
								+ authHandlerClassName, e);
			}
		} else {
			logger.log(Level.SEVERE,
					"Cannot load authentication handler class. Null handler class name provided.");
		}
	}

	public WebResponse login(HttpServletRequest request) {
		if (this.authHandler != null) {
			// Put the user data and CSRF Token in session
			WebResponse webResponse = authHandler.login(request);
			if (webResponse.getData() != null) {
				if (webResponse.getData() instanceof UserProfileData) {
					UserProfileData upd = (UserProfileData) webResponse
							.getData();
					SessionManager.INSTANCE.registerUser(upd,
							SessionManager.INSTANCE.getSession(request, false)
									.getId());
				} else {
					logger.log(
							Level.WARNING,
							"User data received from authentication handler is not right!. User Data received : "
									+ webResponse.getData()
									+ webResponse.getData() != null ? ", Class : "
									+ webResponse.getData().getClass()
											.getName()
									: "");
					SessionManager.INSTANCE.getSession(request, false)
							.setAttribute(SUIConstants.ATTR_USER_DATA,
									webResponse.getData());
				}
			}
			return webResponse;
		} else {
			throw new RuntimeException(
					"Cannot authenticate user. No Application Authentication Handler defined.");
		}
	}

	public WebResponse logout(HttpServletRequest request) {
		if (this.authHandler != null) {
			UserProfileData upd = getLoggedInUser(request);
			String sId = null;
			HttpSession s = SessionManager.INSTANCE.getSession(request, false);
			if (s != null) {
				sId = s.getId();
			}
			WebResponse wr = authHandler.logout(request);
			SessionManager.INSTANCE.unregisterUser(upd, sId);
			return wr;
		} else {
			throw new RuntimeException(
					"Cannot authenticate user. No Application Authentication Handler defined.");
		}
	}

	public UserProfileData getLoggedInUser(HttpServletRequest request) {
		return (UserProfileData) SessionManager.INSTANCE.getSession(request,
				false).getAttribute(SUIConstants.ATTR_USER_DATA);
	}

	public boolean isAutherized(HttpServletRequest request) {
		return (this.authHandler == null) || (getLoggedInUser(request) != null);
	}
}
