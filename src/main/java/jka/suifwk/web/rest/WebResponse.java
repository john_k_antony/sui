package jka.suifwk.web.rest;

import java.io.Serializable;
import java.util.Map;

import jka.suifwk.api.APICallResponse;

public class WebResponse extends APICallResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7620682229655489185L;
	
	protected String redirectURL;

	public String getRedirectURL() {
		return redirectURL;
	}

	public WebResponse(int responseStatusCode, String responseMessage) {
		this(responseStatusCode, responseMessage, null, null, null);
	}

	public WebResponse(int responseStatusCode, String responseMessage, Object data) {
		this(responseStatusCode, responseMessage, null, data, null);
	}

	public WebResponse(int responseStatusCode, String responseMessage, Object data,
			Map<String, Object> metaData) {
		this(responseStatusCode, responseMessage, null, data, metaData);
	}

	public WebResponse(int responseStatusCode, String responseMessage,
			String redirectURL, Object data, Map<String, Object> metaData) {
		super(responseStatusCode, responseMessage, data, metaData);
		if(redirectURL != null) {
			this.redirectURL = redirectURL;
		}
	}
	
	public WebResponse(APICallResponse apiCallResponse, String redirectURL) {
		this(apiCallResponse.getResponseStatusCode(),
		apiCallResponse.getResponseMessage(), redirectURL,
		apiCallResponse.getData(), apiCallResponse
				.getMetaData(false));
		this.setIsLongRunning(apiCallResponse.isLongRunning());
		this.setLongRunningTaskId(apiCallResponse.getLongRunningTaskId());
		this.setPercentCompleted(apiCallResponse.getPercentCompleted());
		this.setIsDone(apiCallResponse.isDone());
		this.setIsCancelled(apiCallResponse.isCancelled());
		this.setAsyncState(apiCallResponse.getAsyncState());	
		this.setLastModifiedTime(apiCallResponse.getLastModifiedTime());
		this.setStartedByUserId(apiCallResponse.getStartedByUserId());
	}

}
