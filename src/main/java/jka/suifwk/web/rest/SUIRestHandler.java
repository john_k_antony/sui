package jka.suifwk.web.rest;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.BeanFacade;
import jka.suifwk.api.BeanFacadeRegistry;
import jka.suifwk.api.CallContext;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.web.api.CallContextManager;
import jka.suifwk.web.utils.WebUtils;

import org.codehaus.jackson.JsonNode;

@Path("/")
public class SUIRestHandler {

	private static Logger logger = Logger.getLogger(SUIRestHandler.class
			.getName());

	@Context
	UriInfo uriInfo;

	@Context
	HttpServletRequest request;
	
	@Context
	HttpServletResponse response;

	@GET
	@Path("{resourceName}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM })
	public Object handleGetRestCall(
			@PathParam("resourceName") String resourceName) {
		try {
			InvocationData iData = getInvocationData(resourceName, request);
			APICallResponse apiCallResponse = iData.getBeanFacade().read(
					iData.getContext());
			return WebUtils.createWebResponse(apiCallResponse, request, response);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					"Cannot process REST request for resource : "
							+ resourceName, e);
			return WebUtils.generateInternalErrorResponse(e);
		}
	}

	@PUT
	@Path("{resourceName}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Object handlePutRestCall(
			@PathParam("resourceName") String resourceName) {
		try {
			InvocationData iData = getInvocationData(resourceName, request);
			APICallResponse apiCallResponse = iData.getBeanFacade().update(
					iData.getContext());
			return WebUtils.createWebResponse(apiCallResponse, request, response);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					"Cannot process REST request for resource : "
							+ resourceName, e);
			return WebUtils.generateInternalErrorResponse(e);
		}
	}

	@DELETE
	@Path("{resourceName}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Object handleDeleteRestCall(
			@PathParam("resourceName") String resourceName) {
		try {
			InvocationData iData = getInvocationData(resourceName, request);
			APICallResponse apiCallResponse = iData.getBeanFacade().delete(
					iData.getContext());
			return WebUtils.createWebResponse(apiCallResponse, request, response);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					"Cannot process REST request for resource : "
							+ resourceName, e);
			return WebUtils.generateInternalErrorResponse(e);
		}
	}

	@POST
	@Path("{resourceName}")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM })
	public Object handlePostRestCall(
			@PathParam("resourceName") String resourceName) {
		try {
			InvocationData iData = getInvocationData(resourceName, request);
			Object payload = iData.getContext().getContextParam(
					SUIConstants.REQUEST_PAYLOAD);
			JsonNode jsonPayload = null;
			if (payload != null && payload instanceof JsonNode) {
				jsonPayload = (JsonNode) payload;
			}
			boolean isParametrizedGet = false;
			if (jsonPayload != null
					&& jsonPayload
							.get(SUIConstants.JSON_NODE_IS_PARAMETRIZED_GET) != null
					&& jsonPayload.get(
							SUIConstants.JSON_NODE_IS_PARAMETRIZED_GET)
							.getBooleanValue() == true) {
				isParametrizedGet = true;
			}
			APICallResponse apiCallResponse = null;
			if (isParametrizedGet) {
				apiCallResponse = iData.getBeanFacade()
						.read(iData.getContext());
			} else {
				apiCallResponse = iData.getBeanFacade().create(
						iData.getContext());
			}
			return WebUtils.createWebResponse(apiCallResponse, request, response);
		} catch (Exception e) {
			logger.log(Level.SEVERE,
					"Cannot process REST request for resource : "
							+ resourceName, e);
			return WebUtils.generateInternalErrorResponse(e);
		}
	}

	protected InvocationData getInvocationData(String resourceName,
			HttpServletRequest request) {
		BeanFacade beanFacade = null;
		CallContext context = null;
		if (resourceName != null) {
			beanFacade = BeanFacadeRegistry.INSTANCE.getFacade(resourceName);
			if (beanFacade != null) {
				context = CallContextManager.INSTANCE.getCallContext(request);
			} else {
				throw new SUIRuntimeException("Resource handler for '"
						+ resourceName + "' not found!");
			}
		} else {
			throw new SUIRuntimeException("Null Resource handler request!");
		}
		return new InvocationData(beanFacade, context);
	}

	protected class InvocationData {
		private final BeanFacade beanFacade;
		private final CallContext context;

		public InvocationData(BeanFacade beanFacade, CallContext context) {
			this.beanFacade = beanFacade;
			this.context = context;
		}

		public BeanFacade getBeanFacade() {
			return beanFacade;
		}

		public CallContext getContext() {
			return context;
		}
	}

	@GET
	@Path("longRunningRequest/{waitTime}")
	@Produces({ MediaType.APPLICATION_JSON })
	public WebResponse longRunningRequest(@PathParam("waitTime") int waitTime) {

		try {
			Thread.sleep(waitTime);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		return new WebResponse(SUIConstants.STATUS_CODE_OK, null,
				waitTime);
	}

}