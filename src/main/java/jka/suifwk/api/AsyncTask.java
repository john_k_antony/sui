package jka.suifwk.api;

import java.io.Serializable;


public interface AsyncTask extends Serializable {

	public String getId();
	public void setId(String id);
	
	public String getStartedByUserId();
	public void setStartedByUserId(String userId);
		
	public long getLastModifiedTime();
	public void	setLastModifiedTime(long lastModifiedTime);
	
	public String getMessage();
	
	public State getState();
	
	public float getPercentCompleted();
	
	public APICallResponse getAPICallResponse();
	public void setAPICallResponse(APICallResponse apiCallResponse);
	
	public void registerChangeCallback(ChangeListener listener);
	
	public void cancel();
	
	public enum State {
		CREATED,
	    STARTED,
	    INTERRUPTED,
	    CANCELLED,
	    FAILED,
	    COMPLETED
	}
	
	public interface ChangeListener {
		public void onAsyncTaskChange(AsyncTask asyncResponse);
	}
}
