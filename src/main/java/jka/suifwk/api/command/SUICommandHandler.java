package jka.suifwk.api.command;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.CallContext;
import jka.suifwk.api.CommandHandler;
import jka.suifwk.api.LongRunningTaskManager;
import jka.suifwk.api.resource.ResourceUtils;
import jka.suifwk.exception.SUIRuntimeException;

public class SUICommandHandler implements CommandHandler {

	private static Logger logger = Logger.getLogger(SUICommandHandler.class
			.getName());

	protected Map<String, Object[]> enumCache = new HashMap<String, Object[]>();

	@Override
	public String getNamespace() {
		return "sui";
	}

	@Override
	public APICallResponse executeCommand(String commandName,
			CallContext context) {
		switch (commandName) {
		case "listEnum":
			Object enumClassName = APIUtils
					.getPayloadData(context, "enumClass");
			try {
				Object[] enumList = enumCache.get(enumClassName);
				if (enumList == null) {
					@SuppressWarnings("rawtypes")
					Class c = Class.forName(enumClassName.toString());
					if (c.isEnum()) {
						enumList = c.getEnumConstants();
						enumCache.put(enumClassName.toString(), enumList);
					}
				}
				if (enumList != null) {
					return new APICallResponse(enumList);
				} else {
					logger.log(Level.WARNING, "Unable to get enum list for : "
							+ enumClassName);
					return null;
				}
			} catch (Exception e) {
				throw new SUIRuntimeException("Cannot fetch enum list : "
						+ enumClassName, e);
			}
		case "getPublicResourcePath":
			return new APICallResponse(ResourceUtils.getPublicResourcePath(
					context, APIUtils.getPayloadDataAsString(context,
							"resourceName", false)));
		case "getPrivateResourcePath":
			return new APICallResponse(ResourceUtils.getPrivateResourcePath(
					context, APIUtils.getPayloadDataAsString(context,
							"resourceName", false)));
		case "getPublicResourceName":
			return new APICallResponse(ResourceUtils.getPublicResourceName(
					context, APIUtils.getPayloadDataAsString(context,
							"resourcePath", false)));
		case "getPrivateResourceName":
			return new APICallResponse(ResourceUtils.getPrivateResourceName(
					context, APIUtils.getPayloadDataAsString(context,
							"resourcePath", false)));
		case "getResourceName":
			return new APICallResponse(ResourceUtils.getResourceName(context,
					APIUtils.getPayloadDataAsString(context, "resourcePath",
							false)));
		case "listLongRunningTasks":
			return LongRunningTaskManager.INSTANCE.getLongRunningTaskList(context);			
		case "cancelLongRunningTask":
			String longRunningTaskId = APIUtils.getPayloadDataAsString(context, "longRunningTaskId",
					false);
			return LongRunningTaskManager.INSTANCE.cancel(longRunningTaskId);
		}
		return null;
	}
}