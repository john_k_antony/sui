package jka.suifwk.api;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import jka.suifwk.api.AsyncTask.State;
import jka.suifwk.common.SUIConstants;

public class APICallResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4867153201998763009L;

	protected int responseStatusCode;
	protected String responseMessage;
	protected String userId;
	protected long lastModTime;
	protected Object data;
	protected Map<String, Object> metaData;
	protected boolean isLongRunning = false;
	protected String longRunningTaskId;
	protected float percentCompleted = 100;
	protected boolean isDone = true;
	protected boolean isCancelled = false;
	protected State asyncState;

	public boolean isLongRunning() {
		return this.isLongRunning;
	}

	public void setIsLongRunning(boolean isLongRunning) {
		this.isLongRunning = isLongRunning;
	}

	public void setPercentCompleted(float percent) {
		if(percent < 0.0) {
			this.percentCompleted = 0;
		} else if(percent > 100.0) {
			this.percentCompleted = 100;
		} else {
			this.percentCompleted = percent;
		}
	}

	public float getPercentCompleted() {		
		return this.percentCompleted;
	}
	
	public void setAsyncState(State state) {
		this.asyncState = state;
	}
	
	public State getAsyncState() {
		return this.asyncState;
	}

	public void attachToLongRunningTask(CallContext context,
			AsyncTask asyncTask) {
		LongRunningTaskManager.INSTANCE.attachLongRunningTaskToResponse(context,
				this, asyncTask);
	}

	// public boolean isCancelled() {
	// return this.isLongRunning ?
	// LongRunningTaskManager.INSTANCE.isCancelled(this.longRunningTaskId) :
	// false;
	// }
	//
	// public boolean isDone() {
	// return this.isLongRunning ?
	// LongRunningTaskManager.INSTANCE.isDone(this.longRunningTaskId) : true;
	// }

	public boolean isCancelled() {
		return this.isCancelled;
	}

	public boolean isDone() {
		return this.isDone;
	}

	public void setIsCancelled(boolean cancelled) {
		this.isCancelled = cancelled;
	}

	public void setIsDone(boolean done) {
		this.isDone = done;
	}

	public String getLongRunningTaskId() {
		return this.longRunningTaskId;
	}

	public void setLongRunningTaskId(String longRunningTaskId) {
		this.longRunningTaskId = longRunningTaskId;
	}

	public void setResponseStatusCode(int responseStatusCode) {
		this.responseStatusCode = responseStatusCode;
	}

	public int getResponseStatusCode() {
		return responseStatusCode;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public Object getData() {
		return data;
	}
	
	public String getStartedByUserId() {
		return this.userId;
	}

	public void setStartedByUserId(String userId) {
		this.userId = userId;
	}

	public long getLastModifiedTime() {
		return this.lastModTime;
	}

	public void setLastModifiedTime(long lastModifiedTime) {
		this.lastModTime = lastModifiedTime;

	}
	

	public Map<String, Object> getMetaData() {
		return metaData;
	}

	public Map<String, Object> getMetaData(boolean needsToModify) {
		if (metaData == null) {
			return null;
		}
		if (needsToModify) {
			return metaData;
		} else {
			return Collections.unmodifiableMap(metaData);
		}
	}

	public void addMetadata(String key, Object value) {
		if (metaData == null) {
			metaData = new HashMap<String, Object>();
		}
		metaData.put(key, value);
	}

	public APICallResponse(Object data) {
		this(SUIConstants.STATUS_CODE_OK, null, data, null);
	}

	public APICallResponse(Object data, Map<String, Object> metaData) {
		this(SUIConstants.STATUS_CODE_OK, null, data, metaData);
	}

	public APICallResponse(int responseStatusCode, String responseMessage) {
		this(responseStatusCode, responseMessage, null, null);
	}

	public APICallResponse(int responseStatusCode, String responseMessage,
			Object data) {
		this(responseStatusCode, responseMessage, data, null);
	}

	public APICallResponse(int responseStatusCode, String responseMessage,
			Object data, Map<String, Object> metaData) {
		super();
		this.responseStatusCode = responseStatusCode;
		this.responseMessage = responseMessage;
		this.data = data;
		this.metaData = metaData;
	}
}
