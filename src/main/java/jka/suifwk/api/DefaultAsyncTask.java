package jka.suifwk.api;

import java.util.concurrent.Future;

public class DefaultAsyncTask extends AbstractAsyncTask {

	private static final long serialVersionUID = 6612266878936399850L;

	protected transient Future<Object> f;
	
	protected String message;
	protected float percent;
	protected State state;

	public DefaultAsyncTask() {
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setPercentCompleted(float percent) {
		this.percent = percent;
	}

	public void setState(State state) {
		this.state = state;
	}

	public void setFuture(Future<Object> f) {
		this.f = f;
	}

	@Override
	public String getMessage() {
		return this.message;
	}

	@Override
	public State getState() {
		return this.state;
	}

	@Override
	public float getPercentCompleted() {
		return this.percent;
	}

	public Future<Object> getFuture() {
		return f;
	}

	@Override
	public void cancel() {
		this.f.cancel(true);
		this.setState(State.CANCELLED);
		this.notifyChanges();
	}

}
