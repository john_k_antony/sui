package jka.suifwk.api;

public interface CommandHandler {
	
	public String getNamespace();
	
	public APICallResponse executeCommand(String commandName, CallContext context);
	
}
