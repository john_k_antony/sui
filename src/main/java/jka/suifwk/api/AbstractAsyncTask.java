package jka.suifwk.api;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;

public abstract class AbstractAsyncTask implements AsyncTask {

	private static final long serialVersionUID = 6612266878936399850L;

	
	protected transient List<ChangeListener> listeners;
	protected transient Future<Object> f;
	
	protected String id;
	protected String userId;
	protected long lastModTime;
	protected APICallResponse apiCallResponse;

	public AbstractAsyncTask() {
		listeners = new ArrayList<ChangeListener>();
	}

	@Override
	public abstract String getMessage();

	@Override
	public abstract State getState();

	@Override
	public abstract float getPercentCompleted();

	@Override
	public void registerChangeCallback(ChangeListener listener) {
		listeners.add(listener);
	}

	public void notifyChanges() {
		this.notifyListeners();
	}

	protected void notifyListeners() {
		for (ChangeListener l : this.listeners) {
			l.onAsyncTaskChange(this);
		}
	}

	@Override
	public abstract void cancel();
	
	@Override
	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String getId() {
		return this.id;
	}

	@Override
	public String getStartedByUserId() {
		return this.userId;
	}

	@Override
	public void setStartedByUserId(String userId) {
		this.userId = userId;
	}

	@Override
	public long getLastModifiedTime() {
		return this.lastModTime;
	}

	@Override
	public void setLastModifiedTime(long lastModifiedTime) {
		this.lastModTime = lastModifiedTime;

	}

	@Override
	public APICallResponse getAPICallResponse() {
		return this.apiCallResponse;
	}

	@Override
	public void setAPICallResponse(APICallResponse apiCallResponse) {
		this.apiCallResponse = apiCallResponse;		
	}

}
