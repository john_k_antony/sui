package jka.suifwk.api;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.exception.SUIUserException;
import jka.suifwk.utils.Utils;
import jka.suifwk.web.security.UserProfileData;

import org.apache.commons.beanutils.PropertyUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

public class APIUtils {

	protected static ObjectMapper objMapper;

	static {
		objMapper = new ObjectMapper();
		objMapper
				.configure(
						DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES,
						false);
		AnnotationIntrospector jaxbAnnotInterospector = new JaxbAnnotationIntrospector();
		objMapper.getSerializationConfig().setAnnotationIntrospector(
				jaxbAnnotInterospector);
		objMapper.getDeserializationConfig().setAnnotationIntrospector(
				jaxbAnnotInterospector);
	}

	public static List<String> getAllPayloadFields(CallContext context) {
		List<String> retList = null;
		JsonNode payload = (JsonNode) context
				.getContextParam(SUIConstants.REQUEST_PAYLOAD);
		if (payload != null && !payload.isNull()) {
			JsonNode reqPayload = payload
					.get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
			if (reqPayload != null && !reqPayload.isNull()
					&& reqPayload.isObject()) {
				retList = new ArrayList<String>();
				Iterator<String> fieldNames = reqPayload.getFieldNames();
				while (fieldNames.hasNext()) {
					retList.add(fieldNames.next());
				}
			}
		}
		return retList;
	}

	public static <T> List<T> getPayloadDataAsObject(CallContext context,
			String fieldName, Class<T> objectClass) {
		List<T> retObjList = null;
		JsonNode payload = (JsonNode) context
				.getContextParam(SUIConstants.REQUEST_PAYLOAD);
		if (payload != null && !payload.isNull()) {
			JsonNode reqPayload = payload
					.get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
			if (reqPayload != null && !reqPayload.isNull()
					&& reqPayload.isObject()) {
				JsonNode fieldValue = reqPayload.get(fieldName);
				if (fieldValue != null && !fieldValue.isNull()) {
					retObjList = new ArrayList<T>();
					if (fieldValue.isArray()) {
						for (JsonNode node : fieldValue) {
							retObjList.add(getObjectFromJsonDataJackson(node,
									objectClass));
						}
					} else {
						retObjList.add(getObjectFromJsonDataJackson(fieldValue,
								objectClass));
					}
				}
			}
		}
		return retObjList;
	}

	public static void mergePayloadDataIntoObject(CallContext context,
			String fieldName, Object targetObject) {
		if (targetObject != null) {
			JsonNode payload = (JsonNode) context
					.getContextParam(SUIConstants.REQUEST_PAYLOAD);
			if (payload != null && !payload.isNull()) {
				JsonNode reqPayload = payload
						.get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
				if (reqPayload != null && !reqPayload.isNull()
						&& reqPayload.isObject()) {
					JsonNode fieldValue = reqPayload.get(fieldName);
					if (fieldValue != null && !fieldValue.isNull()) {
						if (fieldValue.isArray()) {
							throw new SUIRuntimeException(
									"Input data cannot be an array for merging into target object.");
						} else {
							try {
								objMapper.updatingReader(targetObject)
										.readValue(fieldValue);
							} catch (Exception e) {
								throw new SUIRuntimeException(e);
							}
						}
					}
				}
			}
		}
	}

	public static void mergeObjects(Object sourceObject, Object targetObject) {
		if (sourceObject != null && targetObject != null) {
			try {
				objMapper.updatingReader(targetObject).readValue(
						objMapper.writeValueAsBytes(sourceObject));
			} catch (Exception e) {
				throw new SUIRuntimeException(e);
			}
		}
	}

	public static void mergeSerializedDataIntoObject(String sourceData,
			Object targetObject) {
		if (sourceData != null && targetObject != null) {
			try {
				objMapper.updatingReader(targetObject).readValue(sourceData);
			} catch (Exception e) {
				throw new SUIRuntimeException(e);
			}
		}
	}

	public static <T> T getObjectFromSerializedData(String sourceData,
			Class<T> objectClass) {
		if (sourceData != null && objectClass != null) {
			return getObjectFromJsonDataJackson(sourceData, objectClass);
		} else {
			return null;
		}
	}

	public static <T> T mergeOrCreateObjectFromSerializedData(
			String sourceData, T targetObject, Class<T> objectClass) {
		if (targetObject != null) {
			APIUtils.mergeSerializedDataIntoObject(sourceData, targetObject);
		} else {
			targetObject = APIUtils.getObjectFromSerializedData(sourceData,
					objectClass);
		}
		return targetObject;
	}

	public static String getPayloadDataAsString(CallContext context,
			String fieldName, boolean strictConversion) {
		try {
			Object payloadStrData = getPayloadData(context, fieldName);
			if (payloadStrData instanceof String) {
				return getPayloadData(context, fieldName).toString();
			} else {
				return null;
			}
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return null;
	}

	public static Integer getPayloadDataAsInteger(CallContext context,
			String fieldName, boolean strictConversion, Integer defaultValue) {
		try {
			return getPayloadDataAs(context, fieldName, Integer.class,
					strictConversion, defaultValue);
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	public static Long getPayloadDataAsLong(CallContext context,
			String fieldName, boolean strictConversion, Long defaultValue) {
		try {
			return getPayloadDataAs(context, fieldName, Long.class,
					strictConversion, defaultValue);
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	public static Double getPayloadDataAsDouble(CallContext context,
			String fieldName, boolean strictConversion, Double defaultValue) {
		try {
			getPayloadDataAs(context, fieldName, Double.class,
					strictConversion, defaultValue);
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	public static Short getPayloadDataAsShort(CallContext context,
			String fieldName, boolean strictConversion, Short defaultValue) {
		try {
			return getPayloadDataAs(context, fieldName, Short.class,
					strictConversion, defaultValue);
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	public static Boolean getPayloadDataAsBoolean(CallContext context,
			String fieldName, boolean strictConversion, Boolean defaultValue) {
		try {
			return getPayloadDataAs(context, fieldName, Boolean.class,
					strictConversion, defaultValue);
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	public static Float getPayloadDataAsFloat(CallContext context,
			String fieldName, boolean strictConversion, Float defaultValue) {
		try {
			return getPayloadDataAs(context, fieldName, Float.class,
					strictConversion, defaultValue);
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	@SuppressWarnings("unchecked")
	public static <T> T getPayloadDataAs(CallContext context, String fieldName,
			Class<T> type, boolean strictConversion, T defaultValue) {
		try {
			Object payloadData = getPayloadData(context, fieldName);
			if (payloadData == null) {
				return defaultValue;
			} else {
				return (T) payloadData;
			}
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return defaultValue;
	}

	@SuppressWarnings("unchecked")
	public static <T> List<T> getPayloadDataAsList(CallContext context,
			String fieldName, Class<T> listType, boolean strictConversion) {
		try {
			Object payloadData = getPayloadData(context, fieldName);
			return (List<T>) payloadData;
		} catch (Exception e) {
			if (strictConversion) {
				throw e;
			}
		}
		return null;
	}

	public static Object getPayloadData(CallContext context, String fieldName) {
		return getPayloadData(context, fieldName, false);
	}

	public static Object getPayloadData(CallContext context, String fieldName,
			boolean isGetObjectValueAsSerializedString) {
		Object retVal = context.getContextParam(fieldName);
		if (retVal == null) {
			JsonNode payload = (JsonNode) context
					.getContextParam(SUIConstants.REQUEST_PAYLOAD);
			if (payload != null && !payload.isNull()) {
				JsonNode reqPayload = payload
						.get(SUIConstants.JSON_NODE_PAYLOAD_DATA);
				if (reqPayload != null && !reqPayload.isNull()
						&& reqPayload.isObject()) {
					JsonNode fieldValue = reqPayload.get(fieldName);
					retVal = getFieldValue(fieldValue,
							isGetObjectValueAsSerializedString);
				}
			}
		}
		return retVal;
	}

	public static Map<String, Object> getLightWeightObject(Object bean,
			List<String> params) {
		return getLightWeightObjectUsingBeanJaxb(bean, params);
	}

	public static Object convertToSerializableFormat(Object srcObject) {
		return srcObject;
	}

	/* Private Methods */

	private static <T> T getObjectFromJsonDataJackson(JsonNode jsonNode,
			Class<T> objectClass) {
		T retObj = null;
		if (jsonNode != null && !jsonNode.isNull() && jsonNode.isObject()) {
			try {
				retObj = objMapper.readValue(jsonNode, objectClass);
			} catch (Exception e) {
				throw new SUIRuntimeException("Unable to read object data", e);
			}
		} else {
			throw new SUIRuntimeException(
					"Trying to get object value for a non object item.");
		}
		return retObj;
	}

	private static <T> T getObjectFromJsonDataJackson(String objectData,
			Class<T> objectClass) {
		T retObj = null;
		try {
			retObj = objMapper.readValue(objectData, objectClass);
		} catch (Exception e) {
			throw new SUIRuntimeException("Unable to read object data", e);
		}
		return retObj;
	}

	@SuppressWarnings("unused")
	private static Map<String, Object> getLightWeightObjectUsingBeanUtils(
			Object bean, List<String> params) {
		Map<String, Object> lwObj = new HashMap<String, Object>();
		if (params != null) {
			for (String propName : params) {
				if (propName == null || propName.trim().length() == 0) {
					continue;
				}
				try {
					lwObj.put(propName,
							convertToSerializableFormat(PropertyUtils
									.getProperty(bean, propName)));
				} catch (Exception e) {
					lwObj.put(propName, "undefined");
					// TODO:: handle later
				}
			}
		}
		return lwObj;
	}

	private static Map<String, Object> getLightWeightObjectUsingBeanJaxb(
			Object bean, List<String> params) {
		Map<String, Object> lwObj = new HashMap<String, Object>();
		JsonNode rootNode = objMapper.convertValue(bean, JsonNode.class);
		if (params != null && rootNode != null) {
			for (String propName : params) {
				if (propName == null || propName.trim().length() == 0) {
					continue;
				}
				try {
					lwObj.put(propName, objMapper.readValue(
							rootNode.get(propName), Object.class));
				} catch (Exception e) {
					lwObj.put(propName, "undefined");
					// TODO:: handle later
				}
			}
		}
		return lwObj;
	}

	private static Object getFieldValue(JsonNode fieldValue,
			boolean isGetObjectValueAsSerializedString) {
		Object retVal = null;
		if (fieldValue != null && !fieldValue.isNull()) {
			if (fieldValue.isValueNode()) {
				retVal = getPrimitiveValue(fieldValue);
			} else if (fieldValue.isObject()) {
				retVal = getObjectValue(fieldValue,
						isGetObjectValueAsSerializedString);
			} else if (fieldValue.isArray()) {
				List<Object> retList = new ArrayList<Object>();
				for (JsonNode node : fieldValue) {
					retList.add(getFieldValue(node,
							isGetObjectValueAsSerializedString));
				}
				retVal = retList;
			}
		}
		return retVal;
	}

	@SuppressWarnings("unused")
	private static List<Object> getArrayValues(JsonNode fieldValue,
			boolean isGetObjectValueAsSerializedString) {
		if (fieldValue != null && !fieldValue.isNull() && fieldValue.isArray()) {
			List<Object> retList = new ArrayList<Object>();
			for (JsonNode node : fieldValue) {
				retList.add(getFieldValue(node,
						isGetObjectValueAsSerializedString));
			}
			return retList;
		} else {
			return null;
		}
	}

	private static Object getPrimitiveValue(JsonNode fieldValue) {
		if (fieldValue != null && !fieldValue.isNull()) {
			if (fieldValue.isBinary()) {
				return fieldValue.getBooleanValue();
			}
			if (fieldValue.isBinary()) {
				try {
					return fieldValue.getBinaryValue();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if (fieldValue.isBigInteger()) {
				return fieldValue.getBigIntegerValue();
			}

			if (fieldValue.isBigDecimal()) {
				return fieldValue.getDecimalValue();
			}
			if (fieldValue.isInt()) {
				return fieldValue.getIntValue();
			}
			if (fieldValue.isDouble()) {
				return fieldValue.getDoubleValue();
			}
			if (fieldValue.isLong()) {
				return fieldValue.getLongValue();
			}
			if (fieldValue.isNumber()) {
				return fieldValue.getNumberValue();
			}
			if (fieldValue.isTextual()) {
				return fieldValue.getTextValue();
			}

			return fieldValue.getValueAsText();

		} else {
			return null;
		}
	}

	private static Object getObjectValue(JsonNode fieldValue,
			boolean isGetObjectValueAsSerializedString) {
		Map<String, Object> valueMap = new HashMap<String, Object>();
		Iterator<String> nodeFields = fieldValue.getFieldNames();
		while (nodeFields.hasNext()) {
			String k = nodeFields.next();
			JsonNode v = fieldValue.get(k);
			if (!v.isNull()) {
				if (v.isObject() && isGetObjectValueAsSerializedString) {
					valueMap.put(k, v.toString());
				} else {
					valueMap.put(
							k,
							getFieldValue(v, isGetObjectValueAsSerializedString));
				}
			}
		}
		return valueMap;
	}

	public static UserProfileData getUserData(CallContext context) {
		Object userData = context.getContextParam(SUIConstants.ATTR_USER_DATA);
		if ((userData != null && userData instanceof UserProfileData)) {
			return (UserProfileData) userData;
		}
		return null;
	}

	public static APICallResponse generateInternalErrorResponse(Throwable e) {
		Throwable rootCause = Utils.INSTANCE.getRootCause(e);
		String errorMessage = null;
		if (rootCause instanceof SUIUserException) {
			errorMessage = rootCause.getLocalizedMessage();
		} else {
			errorMessage = SUIConstants.INTERNAL_ERROR_MESSAGE;
		}
		return new APICallResponse(SUIConstants.STATUS_CODE_API_CALL_ERROR,
				errorMessage);
	}
}