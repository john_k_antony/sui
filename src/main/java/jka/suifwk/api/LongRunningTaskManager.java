package jka.suifwk.api;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import jka.suifwk.api.AsyncTask.ChangeListener;
import jka.suifwk.api.AsyncTask.State;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIRuntimeException;
import jka.suifwk.exception.SUIUserException;
import jka.suifwk.messaging.MessagingManager;
import jka.suifwk.web.security.UserProfileData;

public enum LongRunningTaskManager {
	INSTANCE;

	Map<String, FutureTaskEntry> futureTasks;
	Map<String, List<AsyncTask>> futureTasksForContextMap;

	private LongRunningTaskManager() {
		futureTasks = new HashMap<String, FutureTaskEntry>();
		futureTasksForContextMap = new HashMap<String, List<AsyncTask>>();
	}

	public APICallResponse attachLongRunningTaskToResponse(CallContext context,
			APICallResponse response, AsyncTask asyncTask) {
		FutureTaskEntry entry = new FutureTaskEntry(context, response,
				asyncTask);
		String id = UUID.randomUUID().toString();
		Object sessionId = context
				.getSessionParam(SUIConstants.ATTR_SESSION_ID);
		if (sessionId != null) {
			UserProfileData userData = APIUtils.getUserData(context);
			String sessionIdStr = sessionId.toString();
			asyncTask.setId(id);
			asyncTask.setAPICallResponse(response);
			asyncTask.setLastModifiedTime(Calendar.getInstance()
					.getTimeInMillis());
			if (userData != null) {
				asyncTask.setStartedByUserId(userData.getUserName());
			}
			response.setLastModifiedTime(asyncTask.getLastModifiedTime());
			response.setStartedByUserId(asyncTask.getStartedByUserId());
			response.setLongRunningTaskId(id);
			response.setIsLongRunning(true);
			response.setIsDone(false);
			response.setIsCancelled(false);
			response.setPercentCompleted(asyncTask.getPercentCompleted());
			response.setResponseMessage(asyncTask.getMessage());
			futureTasks.put(id, entry);
			List<AsyncTask> longRunningTaskIds = futureTasksForContextMap
					.get(sessionIdStr);
			if (longRunningTaskIds == null) {
				longRunningTaskIds = new ArrayList<AsyncTask>();
				futureTasksForContextMap.put(sessionIdStr, longRunningTaskIds);
			}
			longRunningTaskIds.add(asyncTask);
			MessagingManager.INSTANCE.publish(context,
			SUIConstants.LONG_RUNNING_TASK_START, response);
			
		}
		return response;
	}

	public APICallResponse cancel(String futureId) {
		FutureTaskEntry entry = futureTasks.get(futureId);
		if (entry != null && entry.getAsyncTask() != null) {
			AsyncTask asyncTask = entry.getAsyncTask();
			asyncTask.cancel();
			asyncTask.setLastModifiedTime(Calendar.getInstance()
					.getTimeInMillis());
			entry.getResponse().setLastModifiedTime(asyncTask.getLastModifiedTime());
			entry.getResponse().setIsCancelled(true);
			return entry.getResponse();
		} else {
			return APIUtils
					.generateInternalErrorResponse(new SUIUserException(
							"Cannot cancel the task. Task not found."));
		}
	}

	public APICallResponse getLongRunningTaskList(CallContext context) {
		Object sessionId = context
				.getSessionParam(SUIConstants.ATTR_SESSION_ID);
		if (sessionId != null) {
			List<APICallResponse> longRunnigResponses = new ArrayList<APICallResponse>();
			List<AsyncTask> tasks = this.futureTasksForContextMap.get(sessionId.toString());
			if(tasks != null) {
				for(AsyncTask at : tasks) {
					longRunnigResponses.add(at.getAPICallResponse());
				}
			}			
			return new APICallResponse(longRunnigResponses);
		} else {
			return APIUtils
					.generateInternalErrorResponse(new SUIRuntimeException(
							"No valid session available"));
		}
	}

	private class FutureTaskEntry implements ChangeListener {

		APICallResponse response;
		CallContext context;
		AsyncTask asyncTask;

		protected FutureTaskEntry(CallContext context,
				APICallResponse response, AsyncTask asyncTask) {
			this.context = context;
			this.response = response;
			this.asyncTask = asyncTask;
			this.asyncTask.registerChangeCallback(this);
		}

		protected APICallResponse getResponse() {
			return this.response;
		}

		protected AsyncTask getAsyncTask() {
			return this.asyncTask;
		}

		@Override
		public void onAsyncTaskChange(AsyncTask asyncTask) {

			asyncTask.setLastModifiedTime(Calendar.getInstance()
					.getTimeInMillis());
			response.setLastModifiedTime(asyncTask.getLastModifiedTime());
			response.setResponseMessage(asyncTask.getMessage());
			response.setPercentCompleted(asyncTask.getPercentCompleted());
			response.setAsyncState(asyncTask.getState());

			boolean isCancelled = asyncTask.getState() == State.CANCELLED
					|| asyncTask.getState() == State.INTERRUPTED;
			boolean isDone = asyncTask.getState() == State.COMPLETED;
			boolean isError = asyncTask.getState() == State.FAILED;

			if (isError) {
				response.setResponseStatusCode(SUIConstants.STATUS_CODE_API_CALL_ERROR);
			}

			response.setIsCancelled(isCancelled);
			response.setIsDone(isDone);

			if (isCancelled || isDone || isError) {
				response.setIsLongRunning(false);
				futureTasks.remove(response.getLongRunningTaskId());
			}
			MessagingManager.INSTANCE.publish(context,
			SUIConstants.LONG_RUNNING_TASK_UPDATE, response);		
		}
	}
}
