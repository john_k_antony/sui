package jka.suifwk.api;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class APICriteria {
	
	public static enum APICriteriaOperator {
		EQ,
		NOT_EQ,
		GT,
		LT,
		GT_EQ,
		LT_EQ
	}

	private List<APIClause> clauses;
	
	public APICriteria() {
		clauses = new ArrayList<APIClause>();
	}
	
	public void addCriteria(APIClause clause) {
		synchronized(clauses) {
			clauses.add(clause);
		}
	}
	
	public List<APIClause> getClauses() {
		synchronized(clauses) {
			return Collections.unmodifiableList(this.clauses);
		}
	}
		
	public static class APIClause {
		private String key;
		private Object value;
		private APICriteriaOperator op;
		
		public APIClause(String key, Object value, APICriteriaOperator op) {
			this.key = key;
			this.value = value;
			this.op = op;
		}
		
		public String getKey() {
			return key;
		}

		public Object getValue() {
			return value;
		}

		public APICriteriaOperator getOp() {
			return op;
		}		
	}
}