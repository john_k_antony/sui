package jka.suifwk.api;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum CommandHandlerRegistry {

	INSTANCE;
	
	private static Logger logger = Logger.getLogger(CommandHandlerRegistry.class
			.getName());	
		
	Map<String, CommandHandler> commandHandlerRegistry = null;
	
	private CommandHandlerRegistry() {
		this.commandHandlerRegistry = new HashMap<String, CommandHandler>();
	}
	
	public void scanCommandHandlers() {
		 ServiceLoader<CommandHandler> commandHandlerLoader
	     = ServiceLoader.load(CommandHandler.class); 
		 for(CommandHandler commandHandler : commandHandlerLoader) {
			 registerCommandHandler(commandHandler);
		 }
	}
	
	public void registerCommandHandler(CommandHandler commandHandler) {
		if(commandHandler != null && commandHandler.getNamespace() != null && commandHandler.getNamespace().trim().length() > 0) {
			commandHandlerRegistry.put(commandHandler.getNamespace(), commandHandler);
		} else {
			 logger.log(Level.WARNING, "Cannot register command handler : " + commandHandler);
		}
	}
	
	public CommandHandler getCommandHandler(String namespace) {
		return this.commandHandlerRegistry.get(namespace);
	}
}
