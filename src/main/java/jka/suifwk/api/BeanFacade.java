package jka.suifwk.api;

public interface BeanFacade {
	
	public String getName();
	
	public APICallResponse create(CallContext context);
	public APICallResponse read(CallContext context);
	public APICallResponse update(CallContext context);
	public APICallResponse delete(CallContext context);		
}
