package jka.suifwk.api;

import java.util.HashMap;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;

public enum BeanFacadeRegistry {

	INSTANCE;
	
	private static Logger logger = Logger.getLogger(BeanFacadeRegistry.class
			.getName());	
		
	Map<String, BeanFacade> facadeRegistry = null;
	
	private BeanFacadeRegistry() {
		this.facadeRegistry = new HashMap<String, BeanFacade>();
	}
	
	public void scanBeanFacades() {
		 ServiceLoader<BeanFacade> beanFacadeLoader
	     = ServiceLoader.load(BeanFacade.class); 
		 for(BeanFacade facade : beanFacadeLoader) {
			 registerBeanFacade(facade);
		 }
	}
	
	public void registerBeanFacade(BeanFacade facade) {
		if(facade != null && facade.getName() != null && facade.getName().trim().length() > 0) {
			facadeRegistry.put(facade.getName(), facade);
		} else {
			 logger.log(Level.WARNING, "Cannot register facade : " + facade);
		}
	}
	
	public BeanFacade getFacade(String name) {
		return this.facadeRegistry.get(name);
	}
}
