package jka.suifwk.api;

import java.util.HashMap;
import java.util.Map;

public class APIPayload {

	private Map<String, Object> payload;
	
	public APIPayload() {
		payload = new HashMap<String, Object>();
	}
	
	public void addItem(String key, Object value) {
		payload.put(key, value);
	}
	
	public Object getItem(String key) {
		return payload.get(key);
	}
}
