package jka.suifwk.api;

public interface CallContext {
	
	public Object getContextParam(String param);
	
	public Object getSessionParam(String param);	
	public boolean setSessionParam(String param, Object o);	
	public boolean setSessionParam(String param, Object o, long expirationTime);
	
}
