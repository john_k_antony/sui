package jka.suifwk.api.resource;

import java.io.File;

import jka.suifwk.api.BeanFacade;
import jka.suifwk.api.BeanFacadeRegistry;
import jka.suifwk.api.CallContext;
import jka.suifwk.api.resource.AbstractResourceBeanFacade.BasicFileVisibility;
import jka.suifwk.api.resource.AbstractResourceBeanFacade.FileVisibility;

public class ResourceUtils {
	
	/**
	 * Gets the absolute private resource path for a given resource name
	 * 
	 * @param context
	 * @param resourceName
	 * @return absolute private resource path
	 */
	public static String getPrivateResourcePath(CallContext context, String resourceName) {
		return getResourcePath(context, resourceName, BasicFileVisibility.PRIVATE);
	}
	
	/**
	 * Gets the absolute public resource path for a given resource name.
	 * @param context
	 * @param resourceName
	 * @return absolute public resource path
	 */
	public static String getPublicResourcePath(CallContext context, String resourceName) {
		return getResourcePath(context, resourceName, BasicFileVisibility.PUBLIC);
	}
	
	/**
	 * Gets the absolute resource path for a give resource name and a visibility option
	 * @param context
	 * @param resourceName
	 * @param visibility
	 * @return absolute resource path
	 */
	public static String getResourcePath(CallContext context, String resourceName, FileVisibility visibility) {
		BeanFacade resourceBeanFacade = BeanFacadeRegistry.INSTANCE.getFacade("resource");
		if(resourceBeanFacade != null && resourceBeanFacade instanceof AbstractResourceBeanFacade) {
			AbstractResourceBeanFacade arBeanFacade = (AbstractResourceBeanFacade)resourceBeanFacade;
			return arBeanFacade.getFileStoreDirectory(context, visibility) + File.separator + resourceName;
		} else {
			return null;
		}
	}
	
	/**
	 * Gets the relative resource name from an absolute private resource path
	 * @param context
	 * @param resourcePath
	 * @return relative resource name
	 */
	public static String getPrivateResourceName(CallContext context, String resourcePath) {
		return getResourceName(context, resourcePath, BasicFileVisibility.PRIVATE);
	}
	
	/**
	 * Gets the relative resource name from an absolute public resource path
	 * @param context
	 * @param resourcePath
	 * @return relative resource name
	 */
	public static String getPublicResourceName(CallContext context, String resourcePath) {
		return getResourceName(context, resourcePath, BasicFileVisibility.PUBLIC);
	}

	/**
	 * Gets the relative resource name from an absolute resource path. 
	 * This API tries to figure out whether the absolute path is private / public and get the relative resource name automatically
	 * @param context
	 * @param resourcePath
	 * @return relative resource name
	 */	
	public static String getResourceName(CallContext context, String resourcePath) {
		if(isResourcePrivate(context, resourcePath)) {
			return getPrivateResourceName(context, resourcePath);
		} else if(isResourcePublic(context, resourcePath)) {
			return getPublicResourceName(context, resourcePath);
		} else {
			return null;
		}
	}	
	
	/**
	 * Gets the relative resource name from an absolute resource path and a given visibility 
	 * @param context
	 * @param resourcePath
	 * @param visibility
	 * @return relative resource name
	 */	
	public static String getResourceName(CallContext context, String resourcePath, FileVisibility visibility) {
		BeanFacade resourceBeanFacade = BeanFacadeRegistry.INSTANCE.getFacade("resource");
		if(resourceBeanFacade != null && resourceBeanFacade instanceof AbstractResourceBeanFacade) {
			AbstractResourceBeanFacade arBeanFacade = (AbstractResourceBeanFacade)resourceBeanFacade;			
			String resourceDir = arBeanFacade.getFileStoreDirectory(context, visibility) + File.separator;			
			int nameIndex = resourcePath.indexOf(resourceDir);
			if(nameIndex > -1) {
				return resourcePath.substring(resourceDir.length());
			} else {
				return null;
			}			
		} else {
			return null;
		}
	}	
	
	/**
	 * Checks whether a given absolute resource path is for a public resource
	 * @param context
	 * @param resourcePath
	 * @return
	 */
	public static boolean isResourcePublic(CallContext context, String resourcePath) {
		return isResourceOfVisibility(context, resourcePath, BasicFileVisibility.PUBLIC);
	}
	
	/**
	 * Checks whether a given absolute resource path is for a private resource
	 * @param context
	 * @param resourcePath
	 * @return
	 */
	public static boolean isResourcePrivate(CallContext context, String resourcePath) {
		return isResourceOfVisibility(context, resourcePath, BasicFileVisibility.PRIVATE);
	}
		
	/**
	 * Checks whether a given absolute resource path is of the given visibility
	 * @param context
	 * @param resourcePath
	 * @param visibility
	 * @return
	 */
	public static boolean isResourceOfVisibility(CallContext context, String resourcePath, FileVisibility visibility) {
		BeanFacade resourceBeanFacade = BeanFacadeRegistry.INSTANCE.getFacade("resource");
		if(resourceBeanFacade != null && resourceBeanFacade instanceof AbstractResourceBeanFacade) {
			AbstractResourceBeanFacade arBeanFacade = (AbstractResourceBeanFacade)resourceBeanFacade;			
			String resourceDir = arBeanFacade.getFileStoreDirectory(context, visibility) + File.separator;			
			int nameIndex = resourcePath.indexOf(resourceDir);
			if(nameIndex > -1) {
				return true;
			} else {
				return false;
			}			
		} else {
			return false;
		}
	}	
}
