package jka.suifwk.api.resource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import jka.suifwk.api.APICallResponse;
import jka.suifwk.api.APIUtils;
import jka.suifwk.api.BeanFacade;
import jka.suifwk.api.CallContext;
import jka.suifwk.common.SUIConstants;
import jka.suifwk.exception.SUIUserException;

import org.apache.commons.fileupload.FileItem;

public abstract class AbstractResourceBeanFacade implements BeanFacade {

	private static final String ATTR_FILE_NAME = "fileName";
	private static final String ATTR_VISIBILITY = "visibility";
	private static final String ATTR_FROM_VISIBILITY = "from_visibility";
	private static final String ATTR_TO_VISIBILITY = "to_visibility";

	private static final String FACADE_NAME = "resource";

	private static final int FILE_READ_CHUNK_SIZE = 1024;

	private static Logger logger = Logger.getLogger(AbstractResourceBeanFacade.class
			.getName());
	
	/**
	 *
	 * @deprecated use {@link #getFileStoreDirectory(CallContext context, FileVisibility visibility)} instead.  
	 */
	@Deprecated
	public abstract String getFileStoreDirectory(CallContext context, FileVisibility visibility, String fileName);
	
	public abstract String getFileStoreDirectory(CallContext context, FileVisibility visibility);

	@Override
	public APICallResponse create(CallContext context) {
		return update(context);
	}

	@Override
	public APICallResponse read(CallContext context) {
		List<ResourceFileMetaData> resouces = new ArrayList<ResourceFileMetaData>();
		String fileName = APIUtils.getPayloadDataAsString(context, ATTR_FILE_NAME,
				false);
		String visibility = APIUtils.getPayloadDataAsString(context, ATTR_VISIBILITY,
				false);
		if (fileName != null && fileName.trim().length() > 0) {
			String fileStoreLocation = getFileStoreDirectory(context, BasicFileVisibility.visibilityFor(visibility));
			File f = new File(fileStoreLocation + File.separator + fileName);
			if (f.exists() && f.isFile()) {
				try {
					FileInputStream fin = new FileInputStream(f.getPath());
					APICallResponse apiResponse = new APICallResponse(fin);
					apiResponse
							.addMetadata(SUIConstants.FILE_NAME, f.getName());
					return apiResponse;
				} catch (Exception e) {
					throw new SUIUserException(
							"Unable to process requested file - " + fileName, e);
				}
			} else {
				throw new SUIUserException("Unable to find requested file - "
						+ fileName);
			}
		} else {
			try {
				Collection<FileVisibility> allVisibilities = BasicFileVisibility.getAllFileVisibilities();
				for(FileVisibility visibilityOpt : allVisibilities) {
					File outputDir = new File(getFileStoreDirectory(context, visibilityOpt));
					if (outputDir.exists() && outputDir.isDirectory()) {
						File[] fileList = outputDir.listFiles();
						for (File f : fileList) {
							if (f.isFile()) {
								ResourceFileMetaData resFileMetaData = new ResourceFileMetaData(
										f, visibilityOpt);
								resouces.add(resFileMetaData);
							}
						}
					}					
				}
			} catch (Exception e) {
				throw new SUIUserException(
						"Unable to get list of resources : ", e);
			}
			return new APICallResponse(resouces);
		}
	}

	@Override
	public APICallResponse update(CallContext context) {
		boolean isMultiPart = (Boolean) context
				.getContextParam(SUIConstants.REQUEST_IS_MULTIPART);
		if (isMultiPart) {
			List<FileItem> fileItems = APIUtils.getPayloadDataAsList(context,
					SUIConstants.REQUEST_FILES, FileItem.class, true);
			String visibility = APIUtils.getPayloadDataAsString(context, ATTR_VISIBILITY, false);
			String fileStoreDirectory = getFileStoreDirectory(context, BasicFileVisibility.visibilityFor(visibility));
			File outputDir = new File(fileStoreDirectory);
			if (!outputDir.exists()) {
				outputDir.mkdirs();
			}
			FileOutputStream fout = null;
			for (FileItem fi : fileItems) {
				try {
					String fileName = fi.getName();
					String fullPath = fileStoreDirectory
							+ File.separator + fileName;
					File newFile = new File(fullPath);
					if (newFile.exists()) {
						newFile.delete();
					}
					byte[] readChunk = new byte[FILE_READ_CHUNK_SIZE];
					fout = new FileOutputStream(fullPath, true);
					InputStream fiInputStream = fi.getInputStream();
					int readCount = 0;
					while ((readCount = fiInputStream.read(readChunk)) > -1) {
						fout.write(readChunk, 0, readCount);
					}
					fout.flush();
				} catch (Exception e) {
					logger.log(Level.WARNING, "Cannot store uploaded file - "
							+ fi.getName() + "!", e);
				} finally {
					if (fout != null) {
						try {
							fout.close();
						} catch (Exception e) {
							// TODO: ignore;
						}
					}
				}
			}
			return new APICallResponse(null);
		} else {
			Object returnData = null;
			
			Object dataMapObject = APIUtils.getPayloadData(context, "dataMap",
					false);

			if (dataMapObject != null) {
				if (dataMapObject instanceof List<?>) {
					@SuppressWarnings("unchecked")
					List<Map<String, Object>> propMapList = (List<Map<String, Object>>) dataMapObject;
					List<Object> updatedResources = new ArrayList<Object>();
					for (Map<String, Object> propMap : propMapList) {
						Object retData = updateResourceBeanProperties(propMap, context);
						updatedResources.add(retData);
					}
					returnData = updatedResources;
				} else if (dataMapObject instanceof Map<?, ?>) {
					@SuppressWarnings("unchecked")
					Map<String, Object> propMap = (Map<String, Object>) dataMapObject;
					returnData = updateResourceBeanProperties(propMap, context);
				}
			}
			return new APICallResponse(returnData);
		}
	}

	@Override
	public APICallResponse delete(CallContext context) {
		List<String> fileNameList = APIUtils.getPayloadDataAsList(context,
				"fileName", String.class, false);
		List<String> deletedResources = new ArrayList<String>();
		if (fileNameList != null) {
			for (String fileName : fileNameList) {
				try {
					String visibility = APIUtils.getPayloadDataAsString(context, ATTR_VISIBILITY, false);
					String fileStoreDirectory = getFileStoreDirectory(context, BasicFileVisibility.visibilityFor(visibility));
					
					File f = new File(fileStoreDirectory
							+ File.separator + fileName);
					if (f.exists() && f.isFile()) {
						f.delete();
						deletedResources.add(fileName);
					}
				} catch (Exception e) {
					logger.log(Level.WARNING, "Unable to delete resource - "
							+ fileName, e);
				}
			}
		}
		return new APICallResponse(deletedResources);
	}

	@SuppressWarnings("rawtypes")
	protected Object updateResourceBeanProperties(Map<String, Object> dataMap, CallContext context) {

		List<ResourceFileMetaData> updatedResources = new ArrayList<ResourceFileMetaData>();

		Object fileNames = dataMap.get(ATTR_FILE_NAME);
		String fromVisibility = (String) dataMap.get(ATTR_FROM_VISIBILITY);
		String toVisibility = (String) dataMap.get(ATTR_TO_VISIBILITY);

		String toFileStoreDirectory = getFileStoreDirectory(context, BasicFileVisibility.visibilityFor(toVisibility));
		File outputDir = new File(toFileStoreDirectory);
		if (!outputDir.exists()) {
			outputDir.mkdirs();
		}
		String fromFileStoreDirectory = getFileStoreDirectory(context, BasicFileVisibility.visibilityFor(fromVisibility));
		if (fileNames != null && fileNames instanceof List) {
			for (Object fileName : (List) fileNames) {
				File f = new File(fromFileStoreDirectory
						+ File.separator + fileName);
				if (f.exists() && f.isFile()) {
					File publicFile = new File(toFileStoreDirectory
							+ File.separator + fileName);
					f.renameTo(publicFile);
					ResourceFileMetaData rmd = new ResourceFileMetaData(
							publicFile, BasicFileVisibility.visibilityFor(toVisibility));
					updatedResources.add(rmd);
				}
			}
		}
		return updatedResources;
	}

	@Override
	public String getName() {
		return FACADE_NAME;
	}

	@XmlRootElement
	@XmlType(name = "resourceFile")
	@XmlAccessorType(XmlAccessType.PUBLIC_MEMBER)
	public static class ResourceFileMetaData {

		public ResourceFileMetaData() {

		}

		public ResourceFileMetaData(File f) {
			this.fileName = f.getName();
			this.filePath = f.getPath();
			this.fileSize = f.length();
			int lastIndexOfDot = f.getName().lastIndexOf('.');
			if (lastIndexOfDot > -1) {
				try {
					this.fileType = f.getName().substring(lastIndexOfDot + 1)
							.toUpperCase();
				} catch (Exception e) {
					// TODO:: ignore
				}
			}
		}

		public ResourceFileMetaData(File f, FileVisibility fv) {
			this(f);
			this.fileVisibility = fv;
		}

		public String getFileName() {
			return fileName;
		}

		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		public String getFileType() {
			return fileType;
		}

		public void setFileType(String fileType) {
			this.fileType = fileType;
		}

		public Long getFileSize() {
			return fileSize;
		}

		public void setFileSize(Long fileSize) {
			this.fileSize = fileSize;
		}

		public String getFilePath() {
			return filePath;
		}

		public void setFilePath(String filePath) {
			this.filePath = filePath;
		}

		public FileVisibility getFileVisibility() {
			return fileVisibility;
		}

		public void setFileVisibility(FileVisibility fileVisibility) {
			this.fileVisibility = fileVisibility;
		}
		
		public String getFileVisibilityValue() {
			return this.fileVisibility != null ? this.fileVisibility.getValue() : null;
		}

		String fileName;
		String fileType;
		Long fileSize;
		String filePath;
		FileVisibility fileVisibility;
	}

	public static interface FileVisibility {
		public String getValue();
	}

	public static enum BasicFileVisibility implements FileVisibility {
		PRIVATE("private"), PUBLIC("public");

		private static Map<String, FileVisibility> map = new TreeMap<String, FileVisibility>();
		static {
			for (FileVisibility visibility : values()) {
				map.put(visibility.getValue(), visibility);
			}
		}

		String value;

		private BasicFileVisibility(String value) {
			this.value = value;
		}

		public static FileVisibility visibilityFor(String value) {
			if(value == null) {
				return null;
			} else {
				return map.get(value);
			}
		}

		public static void addVisibility(FileVisibility visibility) {
			if (!map.containsKey(visibility.getValue())) {
				map.put(visibility.getValue(), visibility);
			}
		}
		
		public static Collection<FileVisibility> getAllFileVisibilities() {
			return map.values();
		}

		@Override
		public String getValue() {
			return this.value;
		}
		
		@Override
		public String toString() {
			return this.value;
		}
	}
}