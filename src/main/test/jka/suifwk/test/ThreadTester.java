package jka.suifwk.test;

import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.FutureTask;

public class ThreadTester {
	public static void main(String args[]) {
		new ThreadTester().testCancel();
	}	
	
	public void testCancel() {
		
		Callable<String> lroLoop = new LongRunningOperationLoop();
		Callable<String> lroWait = new LongRunningOperationWait();
		FutureTask<String> ft1 = new FutureTask<>(lroLoop);
		FutureTask<String> ft2 = new FutureTask<>(lroWait);
		
		CancelCallable cc1 = new CancelCallable(ft1);
		CancelCallable cc2 = new CancelCallable(ft2);
		
		System.out.println("Long runnnig operations starting.");
		ExecutorService execService = Executors.newFixedThreadPool(5);
		execService.submit(ft1);
		execService.submit(ft2);
		System.out.println("Long runnnig operations submitted.");
		
		execService.submit(cc1);
		execService.submit(cc2);		
		
		try {
			System.out.println("Waiting for long runnnig operations to complete.");
			System.out.println("Result from long running loop operation : " + ft1.get());
			System.out.println("Result from long running wait operation : " + ft2.get());
		} catch (InterruptedException e) {
			System.out.println("Long runnnig operations interrupted.");
			e.printStackTrace();
		} catch (ExecutionException e) {
			System.out.println("Long runnnig operations exception.");
			e.printStackTrace();
		} catch (CancellationException e) {
			System.out.println("Long runnnig operations cancelled.");
			e.printStackTrace();
		}
		
		execService.shutdown();
		System.out.println("Executor shutdown. Exiting.");
	}
		
	public static class LongRunningOperationLoop implements Callable<String> {
		
		@Override
		public String call() throws Exception {			
			System.out.println("Long runnnig loop started.");
			int counter = 0;
			try {
				while(counter < 10) {
					Thread.sleep(1000);
					counter++;
				}				
				System.out.println("Long runnnig loop exited.");
			} catch(Exception e) {				
				System.out.println("Long runnnig loop stopped with exception.");
				e.printStackTrace();				
			}
			return ""+counter;
		}
	}
	
	public static class LongRunningOperationWait implements Callable<String> {
		
		@Override
		public String call() throws Exception {			
			System.out.println("Long runnnig wait started.");
			int counter = 0;
			try {
				synchronized(this) {
					this.wait(10*1000);
				}
				System.out.println("Long runnnig wait exited.");
			} catch(Exception e) {				
				System.out.println("Long runnnig wait stopped with exception.");
				e.printStackTrace();				
			}
			return ""+counter;
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	public static class CancelCallable implements Callable<Void> {
					
		Future ft = null;
		public CancelCallable(Future ft) {
			this.ft = ft;
		}
		
		@Override
		public Void call() throws Exception {			
			int counter = 0;
			try {
				while(counter < 4) {
					Thread.sleep(1000);
					counter++;
				}
				this.ft.cancel(true);
			} catch(Exception e) {				
				e.printStackTrace();				
			}
			return null;
		}		
	}	
}