package jka.suifwk.test;

import java.io.IOException;
import java.util.zip.DataFormatException;

import jka.suifwk.utils.Utils;

public class GenericTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			GenericTester gt = new GenericTester();
			gt.testBase64Marshaller();
			@SuppressWarnings("unused")
			int i = Integer.parseInt("23");			
			System.out.println("Primitive : " + gt.checkPrimitive(5));
			gt.testIntToString();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean checkPrimitive(Object o) {
		return o.getClass().isPrimitive();
	}

	public void testIntToString() {
		int number = 105345;
		System.out.println(printIntToString(number));
	}

	public String printIntToString(int number) {
		StringBuilder sb = new StringBuilder();
		if (number < 0) {
			sb.append("negative ");
			number = number * -1;
		}

		int count = 0;
		int[] digits = new int[20];

		int base = 10;

		while (number > 0) {
			int remainder = number % base;
			digits[count] = remainder;
			number = (number - remainder) / base;
			count++;
		}

		if (count == 0) {
			sb.append("zero");
		} else {
			StringBuilder tempBuilder = new StringBuilder();
			StringBuilder temp = new StringBuilder();
			for (int counter = 0; counter < count; counter++) {
				switch (counter) {
				case 0:
					tempBuilder.append(getUnitsString(digits[counter]));
					break;
				case 1:
					if (digits[counter] > 1) {
						sb.append(getTensString(digits[counter])).append(' ')
								.append(getUnitsString(digits[counter - 1]))
								.append(' ');
						tempBuilder.delete(0, tempBuilder.length());
					} else if (digits[counter] > 0) {
						sb.append(getTensString(10 + digits[counter - 1]))
								.append(' ');
						tempBuilder.delete(0, tempBuilder.length());
					}
					break;
				case 2:
					if (digits[counter] > 0) {
						temp.delete(0, temp.length());
						temp.append(getUnitsString(digits[counter])).append(
								" hundred ");
						sb.insert(0, tempBuilder.toString());
						sb.insert(0, temp.toString());
						tempBuilder.delete(0, tempBuilder.length());
					}
					break;
				case 3:
					if (digits[counter] > 0) {
						tempBuilder.append(getUnitsString(digits[counter]))
								.append(" thousand ");
					}
					break;
				case 4:
					temp.delete(0, temp.length());
					if (digits[counter] > 1) {
						temp.append(getTensString(digits[counter])).append(' ')
								.append(getUnitsString(digits[counter - 1]))
								.append(" thousand ");
						sb.insert(0, temp.toString());
						tempBuilder.delete(0, tempBuilder.length());
					} else if (digits[counter] > 0) {
						temp.append(getTensString(10 + digits[counter - 1]))
								.append(" thousand ");
						sb.insert(0, temp.toString());
						tempBuilder.delete(0, tempBuilder.length());
					}
					break;
				case 5:
					if (digits[counter] > 0) {
						temp.delete(0, temp.length());
						temp.append(getUnitsString(digits[counter])).append(
								" lakh ");
						sb.insert(0, tempBuilder.toString());
						sb.insert(0, temp.toString());
						tempBuilder.delete(0, tempBuilder.length());
					}
					break;
				}
			}
			sb.insert(0, tempBuilder.toString());
		}

		return sb.toString();
	}

	protected String getUnitsString(int position) {
		switch (position) {
		case 0:
			return "";
		case 1:
			return "one";
		case 2:
			return "two";
		case 3:
			return "three";
		case 4:
			return "four";
		case 5:
			return "five";
		case 6:
			return "six";
		case 7:
			return "seven";
		case 8:
			return "eight";
		case 9:
			return "nine";
		default:
			return "";
		}
	}

	protected String getTensString(int position) {
		switch (position) {
		case 0:
			return "";
		case 2:
			return "twenty";
		case 3:
			return "thirty";
		case 4:
			return "forty";
		case 5:
			return "fifty";
		case 6:
			return "sixty";
		case 7:
			return "seventy";
		case 8:
			return "eighty";
		case 9:
			return "ninety";
		case 10:
			return "ten";
		case 11:
			return "eleven";
		case 12:
			return "twelve";
		case 13:
			return "thirteen";
		case 14:
			return "fourteen";
		case 15:
			return "fifteen";
		case 16:
			return "sixteen";
		case 17:
			return "seventeen";
		case 18:
			return "eighteen";
		case 19:
			return "nineteen";
		default:
			return "";
		}
	}

	public void testBase64Marshaller() throws IOException, DataFormatException {

		String input = "Hello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World Hello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello WorldHello World";

		System.out.println("Input Length : " + input.length());
		String compressedB64 = Utils.compressAndmarshallToBase64(input
				.getBytes());
		System.out.println("Compressed Base 64 : " + compressedB64);
		System.out.println("Compressed Base 64 Length : "
				+ compressedB64.length());

		byte[] uncompressedInputRetrieved = Utils
				.unmarshallAndDeCompressBase64(compressedB64);

		String inputRetrieved = new String(uncompressedInputRetrieved);

		if (inputRetrieved.equals(input)) {
			System.out.println("Compress + Uncompress Successful!");
		}
	}
}
