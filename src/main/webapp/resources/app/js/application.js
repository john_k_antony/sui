var SUIApplication = function(SUIService) {
'use strict';

	this.getApplicationContext = function() {
		var that = this;
		return {
			appName: "<TODO>",
			title: "<TODO>",
			loginView:'resources/app/templates/html/login.html',
			logoutView:'resources/app/templates/html/login.html',
			leftMainView:null,
			appLogo:null,
			showHeader:true,
			showSecondaryHeader: false,
			showLeftMain: false,
			showFooter:false,
			initialContext:{},
			workspaceSwitcherType: 'pills',
			applicationStartupServerCommand: null,
			applicationControllerResources:[],
			applicationCommandResources:[],
			bootstrapApplication: function() {
			},
			getWorkspaces: function() {
				return getWorkspaces();
			},
			getBootstrapCommands: function () {
				return getBootstrapCommands();
			}
		}
	}

	var getWorkspaces =  function() {
		var workspaces = [];
		//TODO:
		return workspaces;
	}

	var getBootstrapCommands = function() {
		var bootstrapCommands = [];
		//TODO:
		return bootstrapCommands;
	}
}