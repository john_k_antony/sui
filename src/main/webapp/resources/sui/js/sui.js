'use strict';

// Declare sui app level module which depends on filters, and services
angular.module('SUIApplication', [
        'ngRoute',
        'ngSanitize',
        'SUIApplication.filters',
        'SUIApplication.services',
        'SUIApplication.charting_service',
        'SUIApplication.directives',
        'SUIApplication.controllers',
        'ui.bootstrap',
        'ngGrid',
        'ngJsTree',
        'blueimp.fileupload'
    ])
    .config(['$routeProvider',
        function($routeProvider) {
            $routeProvider
                .when('/:workspace', {
                    controller: 'SUIWorkspaceRouteController',
                    template: '<div/>'
                }).otherwise({
                    controller: 'SUIWorkspaceRouteController',
                    template: '<div/>'
                });
        }
    ])
    .run([
        'SUIService',
        'SUIServiceMessaging',
        '$rootScope',
        '$window',
        '$timeout',
        '$q',
        function(SUIService, SUIServiceMessaging, $rootScope, $window, $timeout, $q) {

            SUIServiceMessaging.init();

            $rootScope.suiService = SUIService;

            $window['SUIService'] = SUIService;

            angular.element($window).bind('resize', function() {
                $timeout(function() {
                    $rootScope.$broadcast("WINDOW_RESIZED");
                });
            });

            SUIService.registerResourceBundle("SUI_BUNDLE", SUI_BUNDLE);

            var suiApplication = new SUIApplication(SUIService);
            var applicationContext = suiApplication.getApplicationContext();
            if (applicationContext) {
                SUIService.setApplicationContext(applicationContext);
                $q.when(SUIService.loadSUITemplates()).then(function() {
                    SUIService.loadApplicationCSS();
                    var commands = applicationContext.getBootstrapCommands();
                    if (commands) {
                        angular.forEach(commands, function(cmd, idx) {
                            SUIService.registerCommandObject(cmd);
                        });
                    }
                });
            }
        }
    ]);
