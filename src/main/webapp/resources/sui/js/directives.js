'use strict';

/* Directives */


angular.module('SUIApplication.directives', []).
directive('suiVersion', ['version',
    function(version) {
        return function(scope, elm, attrs) {
            elm.text(version);
        };
    }
]).filter('unsafe_html', ['$sce', function($sce) {
    return function(val) {
        return $sce.trustAsHtml(val);
    };
}]).directive('autoFocus', function($timeout) {
    return {
        restrict: 'AC',
        link: function(scope, element, attrs) {
            if (attrs.autoFocus === true || attrs.autoFocus === "true") {
                $timeout(function() {
                    element[0].focus();
                }, 0);

                scope.$on("reset-focus", function() {
                    $timeout(function() {
                        element[0].focus();
                    }, 0);
                });
            }
        }
    };
}).directive('autoFocusConditional', ["$timeout", function($timeout) {
    return {
        restrict: 'AC',
        link: {
            post: function(scope, element, attr) {
                if (attr.autoFocusConditional === true || attr.autoFocusConditional === "true") {
                    angular.element(element[0]).focus();
                    $timeout(function() {
                        element[0].focus();
                    }, 5);
                }
                scope.$on("reset-focus", function() {
                    $timeout(function() {
                        element[0].focus();
                    }, 5);
                });
            }
        }
    }
}]).directive('autoValidate', function($timeout) {
    function performInputValidation(input) {
        if (input) {
            if ($(input).hasClass('ng-invalid')) {
                $(input).closest('.form-group').addClass('has-error');
                var validationMessage = input.validationMessage;
                if (!validationMessage) {
                    if ($(input).hasClass('ng-invalid-required')) {
                        validationMessage = "Please fill out this field.";
                    }
                }
                if (validationMessage) {
                    $(input).next('div.sui-form-item-error-container').show();
                    $(input).next('div.sui-form-item-error-container').children('span.text-danger').text(validationMessage);
                }
            } else {
                $(input).closest('.form-group').removeClass('has-error');
                $(input).next('div.sui-form-item-error-container').hide();
            }
        }
    }

    function setupFormValidation(element) {
        $.each($(element).find('sui-form-item, input, select, textarea'), function(idx, input) {
            $(input).bind('blur change input _sui_val_changed', function(event) {
                $timeout(function() {
                    performInputValidation(input);
                }, 250);
            });
        });
    }

    function validateForm(element) {
        $.each($(element).find('input, select, textarea'), function(idx, input) {
            performInputValidation(input);
        });
    }

    function resetForm(element) {
        $.each($(element).find('input, select, textarea'), function(idx, input) {
            $(input).addClass('ng-pristine');
            $(input).closest('.form-group').removeClass('has-error');
            $(input).next('div.sui-form-item-error-container').hide();
        });
    }

    return {
        restrict: 'AC',
        link: {
            post: function(scope, element) {
                $timeout(function() {
                    setupFormValidation(element);
                })
                scope.$on('do-validate', function() {
                    validateForm(element);
                })
                scope.$on('do-reset', function() {
                    resetForm(element);
                })
            }
        }
    };

}).directive('onElementInit', ['$rootScope',
    function($rootScope) {
        return {
            restrict: 'A',
            link: {
                post: function(scope, element, attrs) {
                    if (scope.onElementInit) {
                        $rootScope.$evalAsync(function() {
                            scope.onElementInit({
                                "element": element
                            });
                        })
                    }
                }
            },
            controller: function($scope) {},
            scope: {
                onElementInit: "&",
            },
        }
    }
]).directive('contextObject', [
    function() {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) {
                scope.contextObject = scope.$eval(attrs.contextObject);
                scope.$watch(attrs.contextObject, function() {
                    scope.contextObject = scope.$eval(attrs.contextObject);
                });
            }
        }
    }
]).directive('compileTemplate', ["$compile", "$parse",
    function($compile, $parse) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                var parseFN = $parse(attr.ngBindHtml);

                function value() {
                    return (parseFN(scope) || '').toString();
                }

                scope.$watch(value, function() {
                    $compile(element, null, -9999)(scope);
                });
            }
        };
    }
]).directive('suifwkCompile', ['$compile',
    function($compile) {
        return function(scope, elem, attr) {
            var evalVal = scope.$eval(attr.suifwkCompile);
            elem.html(evalVal);
            $compile(elem.contents())(scope);
        }
    }
]).directive('compileData', ['$compile',
    function($compile) {
        return {
            scope: true,
            link: function(scope, element, attrs) {
                var elmnt;
                attrs.$observe('template', function(myTemplate) {
                    if (angular.isDefined(myTemplate)) {
                        // compile the provided template against the current scope
                        elmnt = $compile(myTemplate)(scope);
                        element.html(""); // dummy "clear"
                        element.append(elmnt);
                    }
                });
            }
        };
    }
]).directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.keyCode === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
}).directive('ngEscape', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.keyCode === 27) {
                scope.$apply(function() {
                    scope.$eval(attrs.ngEscape);
                });
                event.preventDefault();
            }
        });
    };
}).directive('suiPortletContainer', ['SUIService',
    function(SUIService) {

        function link(scope, element, attrs, ctrl, transcludeFn) {
            element.css({
                padding: 0,
                margin: 0,
                display: "inherit"
            })
        }

        function controller($scope) {
            if ($scope.onLoad) {
                $scope.onLoad();
            }
        }
        return {
            restrict: 'E',
            link: link,
            controller: controller,
            scope: {
                onLoad: "&"
            }
        }
    }
]).directive('suiPortlet', ['SUIService',
    function(SUIService) {

        function link(scope, element, attrs, ctrl, transcludeFn) {
            element.css({
                padding: 0,
                margin: 0,
                display: "inherit",
                position: "relative"
            })
        }

        function controller($scope) {
            if ($scope.onLoad) {
                $scope.onLoad();
            }
        }
        return {
            restrict: 'E',
            link: link,
            controller: controller,
            scope: {
                onLoad: "&"
            }
        }
    }
]).directive('suiCommandItem', ['SUIService', '$timeout',
    function(SUIService, $timeout) {
        function link(scope, element, attrs, ctrl, transcludeFn) {

        };

        function controller($scope) {

            $scope.suiService = SUIService;
            $scope.executionScope = $scope;

            $scope.isItemCommandEnabled = function(cmdObject) {
                if (!cmdObject) {
                    return false;
                }
                if (!cmdObject.enabled) {
                    return false;
                }
                if ($scope.isEnabled) {
                    var isEnb = $scope.isEnabled({
                        'cmdName': cmdObject.name
                    });
                    return isEnb
                }
            }

            $scope.executeItemCommand = function(cmd, isShowBusy) {
                if ($scope.execute) {
                    return $scope.execute({
                        'cmdName': cmd,
                        'isShowBusy': isShowBusy
                    });
                }
            }

            $scope.externalExecuteItemCommand = function(menuItem, cmd) {
                if ($scope.isItemCommandEnabled({
                        enabled: true,
                        name: cmd
                    })) {
                    $scope.containerScope[menuItem.name].radioModel = cmd;
                    $scope.executeItemCommand(cmd, false);
                }
            }

            $scope.fixPosition = function(open) {
                var popupUL = $("ul#CMD_LIST_VIEW_" + ($scope.containerScope.parentViewID ? $scope.containerScope.parentViewID + '_' : '') + $scope.cmdObject.name);
                if (open) {
                    if (popupUL && popupUL.length > 0) {
                        popupUL.css({
                            display: "none"
                        });
                        $timeout(function() {
                            popupUL.css({
                                display: "block"
                            });
                            SUIService.fixPopupPosition(popupUL);
                        });
                    }
                } else {
                    popupUL.css({
                        display: "none"
                    });
                }
                $scope.$broadcast("reset-focus");
            }
        }

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            replace: true,
            scope: {
                isEnabled: "&",
                execute: "&",
                cmdObject: "=",
                containerScope: "=",
                isHide: "="
            },
            templateUrl: 'resources/sui/templates/html/commandItem.html'
        }
    }
]).directive('suiCommandButton', ['SUIService',
    function(SUIService) {
        function link(scope, element, attrs, ctrl, transcludeFn) {

        };

        function controller($scope) {
            $scope.suiService = SUIService;
        }

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            replace: true,
            scope: {
                cmdObject: "=",
                regularItem: "="
            },
            templateUrl: 'resources/sui/templates/html/commandButton.html'
        }
    }
]).directive('suiView', ['$rootScope', 'SUIService', '$timeout',
    function($rootScope, SUIService, $timeout) {
        function doViewContentAutoResize(element) {
            var panel = element.children("div.sui-panel"),
                panelTitle = panel.children("div.sui-view-panel-heading"),
                panelContent = panel.children("div.sui-view-panel-body");

            if (panelTitle.is(':visible')) {
                panelContent.css({
                    "padding-top": panelTitle.outerHeight() + "px"
                });
            }
        }

        function link(scope, element, attrs, ctrl, transcludeFn) {

            if (scope.expandFullSize) {
                element.css({
                    padding: scope.externalPadding ? scope.externalPadding : "8px",
                    margin: 0,
                    position: "absolute",
                    height: scope.viewHeight ? scope.viewHeight : "100%",
                    width: scope.viewWidth ? scope.viewWidth : "100%",
                    overflow: "visible"
                });
            } else {
                element.css({
                    padding: 0,
                    margin: 0,
                    height: scope.viewHeight ? scope.viewHeight : null,
                    width: scope.viewWidth ? scope.viewWidth : null,
                    overflow: "visible"
                });
            }

            if (scope.expandFullSize && scope.showTitle) {
                $rootScope.$on("WINDOW_RESIZED", function() {
                    if (element.is(':visible')) {
                        doViewContentAutoResize(element);
                    }
                });
                $rootScope.$on(SUIService.VIEW_STATE_CHANGE, function() {
                    $timeout(function() {
                        $(window).trigger('resize');
                    }, 150);
                });
                $timeout(function() {
                    doViewContentAutoResize(element);
                }, 150);
            }
        };

        function controller($scope) {
            $scope.UVID = SUIService.registerView($scope);
            $scope.suiService = SUIService;
            $scope.containerScope = $scope.$parent;
            $scope._titleAlign = $scope.titleAlign ? $scope.titleAlign : "left";
            $scope._localCommandList = {};

            $scope.executeCommand = function(name, isShowBusy) {
                SUIService.executeCommand(name, [$scope.viewContextObject], {
                    viewId: $scope.UVID,
                    scope: $scope.$parent,
                    dontShowBusy: SUIService.isNull(isShowBusy) ? false : !isShowBusy
                });
            }

            $scope.isCommandEnabled = function(name) {
                if ($scope.$parent.isCommandEnabled) {
                    return $scope.$parent.isCommandEnabled(name, $scope.viewContextObject);
                } else {
                    return true;
                }
            }

            $scope.busyModalButtonClick = function(buttonId) {
                if (buttonId === 'cancel') {
                    SUIService.cancelCommandExecution($scope.longRunningTaskId);
                }
            }

            $scope.getCommand = function(name, debugInfo) {
                if (!$scope._localCommandList[name]) {
                    $scope._localCommandList[name] = SUIService.getCommand(name);
                }
                return $scope._localCommandList[name];
            }

            var scopeToTrancludedDOM = $scope.$parent;
            if (!scopeToTrancludedDOM.parentViewIDs) {
                scopeToTrancludedDOM.parentViewIDs = [];
            }
            scopeToTrancludedDOM.parentViewIDs.push($scope.UniqueVewID);
            scopeToTrancludedDOM.parentViewID = $scope.UniqueVewID;

            if ($scope.onViewLoad) {
                $scope.onViewLoad();
            }
        };

        return {
            restrict: 'E',
            transclude: true,
            link: link,
            controller: controller,
            replace: false,
            scope: {
                UniqueVewID: "@viewId",
                viewTitle: "@",
                viewTitleClass: "@",
                showTitle: "=",
                titleAlign: "@",
                hidePanelBorder: "=",
                viewContainerClassOverride: "@",
                expandFullSize: "=",
                viewHeight: "=",
                viewWidth: "=",
                onViewLoad: "&",
                commandList: "=",
                viewContextObject: "=",
                externalPadding: "=",
                viewTooltip: "="
            },
            templateUrl: 'resources/sui/templates/html/suiView.html'
        };
    }
]).directive('busyModalDialog', ['SUIService',
    function(SUIService) {

        function link(scope, element, attrs, ctrl, transcludeFn) {};

        function controller($scope, SUIService) {
            $scope.suiService = SUIService;
            $scope.buttonClicked = function(buttonId) {
                $scope.busyModalButtonClick({
                    buttonId: buttonId
                });
            }
        };

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            scope: {
                canCancelAction: "=",
                waitMessage: "=",
                busyModalButtonClick: "&busyModalButtonClick",
                progressPercent: "=",
                showProgressPercent: "="
            },
            templateUrl: function() {
                var templateURL = 'resources/sui/templates/html/busyModalDialog.html';
                var appContext = SUIService.getApplicationContext();
                if (appContext && appContext["busyModalDialog.html"]) {
                    templateURL = appContext["busyModalDialog.html"];
                }
                return templateURL
            }
        };
    }
]).directive('suiChart', ['SUIService', 'ChartFactory',
    function(SUIService, ChartFactory) {

        ChartFactory.initialize();

        function link(scope, element, attrs, ctrl, transcludeFn) {

            element.css({
                "height": "100%",
                "width": "100%",
                "display": "block",
                "position": "relative"
            });

            scope.chartUID = SUIService.getNextID("SuiChart");
            scope.containerElement = element;
            scope.$watch("type", function(val) {
                scope.redrawChart();
            });
            scope.$watch("data", function(val) {
                scope.redrawChart();
            });
        };

        function controller($scope, $timeout) {

            $scope.getChartSnapShotSVG = function() {
                return $($scope.containerElement).html();
            }

            $scope.redrawChart = function() {

                var chartDim = {
                    minH: !SUIService.isNull($scope.minHeight) ? $scope.minHeight : "",
                    minW: !SUIService.isNull($scope.minWidth) ? $scope.minWidth : "",
                    maxH: !SUIService.isNull($scope.maxHeight) ? $scope.maxHeight : "",
                    maxW: !SUIService.isNull($scope.maxWidth) ? $scope.maxWidth : ""
                }

                if (SUIService.isNull($scope.type) ||
                    SUIService.isNull($scope.data)) {
                    return;
                }

                var config = null;
                if (!$scope.config) {
                    config = {};
                } else {
                    config = angular.copy($scope.config);
                }

                if ($scope.field) {
                    config.field = $scope.field;
                }

                if ($scope.fields) {
                    config.fields = $scope.fields;
                }

                if ($scope.containerClass) {
                    config.containerClass = $scope.containerClass;
                }

                if ($scope.legendContainerClass) {
                    config.legendContainerClass = $scope.legendContainerClass;
                }

                config.chartUID = $scope.chartUID;

                config.showLegend = $scope.showLegend;

                config.chartType = $scope.type;

                if (!$($scope.containerElement).is(':visible')) {
                    //TODO: Check why _getChartContainer is called here!! Commenting it out now
                    //ChartFactory._getChartContainer($scope.containerElement[0], chartDim, config, true);
                    $timeout(function() {
                        $scope.redrawChart();
                    }, 2000);
                } else {
                    ChartFactory.createChart($scope.type, $scope.containerElement[0], chartDim, $scope.data, config);
                }

                $scope.config.chartAPI = {
                    redrawChart: $scope.redrawChart,
                    getChartSnapShotSVG: $scope.getChartSnapShotSVG
                }
            }
        };

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            scope: {
                showLegend: "=",
                type: "=",
                data: "=",
                field: "@",
                fields: "=",
                config: "=",
                minHeight: "@",
                minWidth: "@",
                maxHeight: "@",
                maxWidth: "@",
                containerClass: "@",
                legendContainerClass: "@"
            },
            template: ""
        };
    }
]).directive('suiPropertyViewer', ['SUIService',
    function(SUIService) {

        function link(scope, element, attrs, ctrl, transcludeFn) {};

        function controller($scope) {
            $scope.fieldContainerOverride = $scope.fieldContainerClass;
            $scope.fieldContainerEvenOverride = $scope.enableEvenOddMarking ? ($scope.fieldContainerEvenClass || "sui-property-viewer-even-item") : "";
            $scope.fieldContainerOddOverride = $scope.enableEvenOddMarking ? ($scope.fieldContainerOddClass || "sui-property-viewer-odd-item") : "";
            $scope.labelContainerOverride = $scope.labelContainerClass || "col-sm-3";
            $scope.valueContainerOverride = $scope.valueContainerClass || "col-sm-9";

            $scope.fieldsToDisplay = $scope.fields;
            $scope.suiService = SUIService;
            $scope.parentScope = $scope.$parent;
            if (!$scope.fieldsToDisplay || !angular.isArray($scope.fieldsToDisplay)) {
                $scope.fieldsToDisplay = [];
                angular.forEach($scope.bean, function(value, key) {
                    if (!angular.isObject(value) && !angular.isArray(value)) {
                        $scope.fieldsToDisplay.push(key);
                    }
                });
            }

            $scope.getPropertyName = function(field) {
                var _fName = field.name || field,
                    _pName = $scope.parentScope.formatPropertyName ? $scope.parentScope.formatPropertyName(_fName) : _fName;
                return _pName;
            }

            $scope.getPropertyValue = function(field) {
                var _fName = field.name || field,
                    _oValue = SUIService.getObjectProperty($scope.bean, _fName),
                    _pValue = $scope.parentScope.formatPropertyValue ? $scope.parentScope.formatPropertyValue(_fName, $scope.bean, _oValue) : _oValue;
                return _pValue;
            }
        };

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            scope: {
                fields: "=",
                bean: "=",
                fieldContainerClass: "=",
                fieldContainerEvenClass: "=",
                fieldContainerOddClass: "=",
                labelContainerClass: "=",
                valueContainerClass: "=",
                enableEvenOddMarking: "="
            },
            templateUrl: 'resources/sui/templates/html/propertyViewerForm.html'
        };

    }
]).directive('suiEditorDialog', ['SUIService', '$timeout',
    function(SUIService, $timeout) {

        function link(scope, element, attrs, ctrl, transcludeFn) {};

        function controller($scope) {

            $scope.showErrorMessages = false;
            $scope.showCustomMessages = false;
            $scope.isCustomValid = true;
            $scope.customMessages = {};

            $scope.$parent.innerFormContainer = {};

            if (SUIService.isNull($scope.showDialogHeader)) {
                $scope.showHeader = true;
            } else {
                $scope.showHeader = $scope.showDialogHeader;
            }
            if (SUIService.isNull($scope.dialogResetTitle)) {
                $scope.resetTitle = "Reset";
            } else {
                $scope.resetTitle = $scope.dialogResetTitle;
            }
            if (SUIService.isNull($scope.dialogOkTitle)) {
                $scope.okTitle = "Ok";
            } else {
                $scope.okTitle = $scope.dialogOkTitle;
            }
            if (SUIService.isNull($scope.dialogCancelTitle)) {
                $scope.cancelTitle = "Cancel";
            } else {
                $scope.cancelTitle = $scope.dialogCancelTitle;
            }

            $scope.ok = function() {
                var isInvalid = false;
                $scope.$parent.$broadcast("do-validate");
                angular.forEach($scope.$parent.innerFormContainer, function(v, k) {
                    if (!isInvalid && v.$invalid) {
                        isInvalid = true;
                    }
                });
                if (isInvalid) {
                    $scope.$parent.$broadcast("reset-focus");
                    // SUIService.showDialogMessage({
                    //     isModal: true,
                    //     dialogTitle: "Validation Error",
                    //     dialogMessage: "Please correct the errors!",
                    //     dialogType: SUIService.DIALOG_TYPE_ERROR,
                    //     callbackFn: function(dialogResult) {
                    //         $scope.$broadcast("reset-focus");
                    //     }
                    // });
                } else {
                    $scope.showErrorMessages = true;
                    if ($scope.isCustomValid && $scope.dialogOkHandler) {
                        $scope.dialogOkHandler();
                    } else {}
                }
            };

            $scope.cancel = function() {
                if ($scope.dialogCancelHandler) {
                    $scope.dialogCancelHandler();
                }
            };

            $scope.reset = function() {
                SUIService.showDialogMessage({
                    isModal: true,
                    dialogSize: "md",
                    dialogTitle: "Reset Confirmation",
                    dialogMessage: "Are you sure you want to reset the values?",
                    dialogType: SUIService.DIALOG_TYPE_CONFIRM,
                    callbackFn: function(dialogResult) {
                        if (dialogResult === SUIService.DIALOG_OK) {
                            $scope.showErrorMessages = false;
                            if ($scope.dialogResetHandler) {
                                $scope.dialogResetHandler();
                            }
                            $timeout(function() {
                                angular.forEach($scope.$parent.innerFormContainer, function(v, k) {
                                    v.$setPristine();
                                    $scope.$parent.$broadcast("do-reset");
                                });
                            }, 100);
                        }
                        $scope.$parent.$broadcast("reset-focus");
                    }
                });
            };

            $scope.onEnterKeyPress = function($event) {
                if (!$scope.onEnterKeyAction) {
                    $scope.ok();
                } else {
                    switch ($scope.onEnterKeyAction) {
                        case SUIService.DIALOG_OK:
                            $scope.ok();
                            break;
                        case SUIService.DIALOG_CANCEL:
                            $scope.cancel();
                            break;
                        case SUIService.DIALOG_RESET:
                            $scope.reset();
                            break;
                    }
                }
            };

            $scope.getExtraButtonClass = function(extButton) {
                return extButton.class || "btn btn-primary";
            }

            $scope.handleExtraButtonClick = function(extButton) {
                if (extButton.click) {
                    extButton.click();
                }
                $scope.$parent.$broadcast("reset-focus");
            }

            if ($scope.dialogApi) {
                $scope.dialogApi.clearCustomMessage = function() {
                    $scope.showCustomMessages = false;
                    $scope.customMessages = {};
                }

                $scope.dialogApi.removeCustomMessage = function(messageId) {
                    try {
                        delete $scope.customMessages[messageId];
                    } catch (e) {

                    }
                }

                $scope.dialogApi.addCustomMessage = function(messageData) {
                    if (messageData && messageData.id) {
                        $scope.customMessages[messageData.id] = messageData;
                        $scope.showCustomMessages = true;
                    }
                }
                $scope.dialogApi.setIsCustomValid = function(flag) {
                    $scope.isCustomValid = flag;
                }
            }
        };

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            transclude: true,
            scope: {
                dialogTitle: "=",
                dialogOkTitle: "@",
                dialogCancelTitle: "@",
                dialogResetTitle: "@",
                dialogOkHandler: "&",
                dialogCancelHandler: "&",
                dialogResetHandler: "&",
                dialogCommandName: "@",
                dialogBodyClass: "=",
                dialogHeaderClass: "=",
                dialogHeaderTitleClass: "=",
                dialogFooterClass: "=",
                showDialogHeader: "=",
                onEnterKeyAction: "=",
                dialogExtraButtons: "=",
                dialogApi: "=",
                isHideOk: "=",
                isHideCancel: "=",
                isHideReset: "=",
            },
            templateUrl: 'resources/sui/templates/html/formEditDialog.html'
        };
    }
]).directive('suiForm', ['SUIService',
    function(SUIService) {

        function link(scope, element, attrs, ctrl, transcludeFn) {};

        function controller($scope) {
            $scope.$watch("formName", function() {
                var formObj = $scope[$scope.formName];
                if ($scope.$parent.innerFormContainer) {
                    $scope.$parent.innerFormContainer[$scope.formName] = formObj;
                    $scope.$parent.innerFormContainer.DEFAULT_FORM = formObj;
                }
            })
        };

        return {
            restrict: 'E',
            link: link,
            controller: controller,
            transclude: true,
            scope: {
                formName: "@",
            },
            templateUrl: 'suiForm.html'
        };
    }
]).directive('suiFormItem', ['SUIService', '$parse',
    function(SUIService, $parse) {

        function link(scope, element, attrs, ctrl, transcludeFn) {
            scope.itemName = attrs.itemName;
            scope.itemType = attrs.itemType;
            scope.itemDataModel = scope.$eval(attrs.itemDataModel);
            scope.itemDataValue = scope.$eval(attrs.itemDataValue);
            scope.$watch(attrs.itemDataModel, function() {
                scope.internalDataModel.dataItem = scope.$eval(attrs.itemDataModel);
                $.each($(element).find('input, select, textarea'), function(idx, input) {
                    $(input).trigger("_sui_val_changed");
                });
            });

            scope.$watch(attrs.itemDataValue, function() {
                scope.internalDataModel.dataValue = scope.$eval(attrs.itemDataValue);
            });

            if (attrs.itemHint) {
                scope.$watch(attrs.itemHint, function() {
                    scope.itemHint = scope.$eval(attrs.itemHint);
                });
            }

            scope.itemChange = attrs.itemChange;
            scope.itemOptions = attrs.itemOptions;
            scope.itemLabel = attrs.itemLabel;
            scope.itemSubText = attrs.itemSubText;
            scope.itemHint = scope.$eval(scope.itemHint);
            scope.itemPrompt = attrs.itemPrompt;
            scope.itemRequired = scope.$eval(attrs.itemRequired);
            scope.itemAutoFocus = scope.$eval(attrs.itemAutoFocus);
            scope.itemReadonly = scope.$eval(attrs.itemReadonly);

            scope.itemContainerClass = scope.$eval(attrs.itemContainerClass);
            scope.itemLabelClass = scope.$eval(attrs.itemLabelClass);
            scope.itemControlClass = scope.$eval(attrs.itemControlClass);

            scope.type = "text";
            scope.prompt = scope.itemLabel ? ("Enter " + scope.itemLabel) : "";
            scope.labelClass = "col-sm-3";
            scope.controlClass = "col-sm-9";

            if (!SUIService.isNull(scope.itemType)) {
                scope.type = scope.itemType;
            }
            if (!SUIService.isNull(scope.itemPrompt)) {
                scope.prompt = scope.itemPrompt;
            }
            if (!SUIService.isNull(scope.itemLabelClass)) {
                scope.labelClass = scope.itemLabelClass;
            }
            if (!SUIService.isNull(scope.itemControlClass)) {
                scope.controlClass = scope.itemControlClass;
            }
            if (!SUIService.isNull(scope.itemContainerClass)) {
                scope.containerClass = scope.itemContainerClass;
            }

            if ((scope.itemType !== 'static') && (scope.itemReadonly !== true)) {
                scope.$watch("internalDataModel.dataItem", function() {
                    scope.$eval(attrs.itemDataModel + " = internalDataModel.dataItem");
                });
            }

            if (attrs.itemChange) {
                element.bind('change', function(e) {
                    scope.$apply(function() {
                        //$parse returns a getter function to be executed against an object
                        var fn = $parse(attrs.itemChange);
                        //In our case, we want to execute the statement in confirmAction i.e. 'doIt()' against the scope which this directive is bound
                        //Because the scope is a child scope, not an isolated scope, the prototypical inheritance of scopes will mean that it will eventually find a 'doIt' function bound against a parent's scope
                        fn(scope, {
                            $event: e
                        });
                    });
                    //scope.$apply(attrs.itemChange);
                });
            }

            if (attrs.itemStaticDataFormatter) {
                var fn = $parse(attrs.itemStaticDataFormatter);
                scope.itemStaticDataFormatter = fn(scope);
            }

            scope.getFormItemName = function(itemName) {
                return itemName;
            }

            scope.internalDataModel.dataItem = scope.itemDataModel;
        };

        function controller($scope, $element) {
            $scope.internalDataModel = {};

            $scope.isItemValid = function(itemName) {
                return $scope.isFormItemValid(itemName, "DEFAULT_FORM")
            }

            $scope.isFormItemValid = function(itemName, formName) {
                var isInValid = $scope.innerFormContainer[formName] &&
                    $scope.innerFormContainer[formName][itemName] &&
                    $scope.innerFormContainer[formName][itemName].$dirty &&
                    $scope.innerFormContainer[formName][itemName].$invalid;
                return !isInValid;
            }

            $scope.getFieldName = function(fieldName) {
                return fieldName;
            }

            $scope.getItem = function(itemName) {
                return $scope.getFormItem(itemName, "DEFAULT_FORM")
            }
            $scope.getFormItem = function(itemName, formName) {
                if ($scope.innerFormContainer[formName]) {
                    return $scope.innerFormContainer[formName][itemName];
                }
            }
        };

        return {
            priority: 1001,
            restrict: 'E',
            link: link,
            controller: controller,
            scope: true,
            transclude: true,
            templateUrl: 'suiFormItem.html'
        };
    }
]).directive('setName', ['$compile', '$parse',
    function($compile, $parse) {
        return {
            //terminal: true,
            //priority: 10000,
            restrict: 'A',
            link: function(scope, element, attrs) {
                var elemName = scope.$eval(attrs.setName) || attrs.setName;
                if (elemName) {
                    attrs.$set('name', elemName);
                    element.attr('name', elemName);
                }
            }
        }
    }
])

.directive('tabset', ['$rootScope', '$compile', '$parse', 'SUIUtils', '$timeout',
    function($rootScope, $compile, $parse, SUIUtils, $timeout) {
        function doTabContentAutoResize(element) {
            //var tabContentContainer = element.find(".tab-content .tab-pane.active").children(0);
            var tabContentContainer = element.find(".tab-content .tab-pane").children(0);
            if (tabContentContainer) {
                tabContentContainer.css({});
                $timeout(function() {
                    var elTop = element.position().top + 8;

                    if (element.is(':visible')) {
                        tabContentContainer.css({
                            "margin-top": (elTop + (element.outerHeight() - 8)) + "px"
                        });
                    }
                });
            }
        }
        return {
            restrict: 'E',
            link: function(scope, element, attrs) {
                if (!attrs.noAutoResize || ("" + attrs.noAutoResize !== "true")) {
                    $rootScope.$on("WINDOW_RESIZED", function() {
                        if (element.is(':visible')) {
                            doTabContentAutoResize(element);
                        }
                    });
                    $rootScope.$on(SUIService.VIEW_STATE_CHANGE, function() {
                        scope.$evalAsync(function() {
                            doTabContentAutoResize(element);
                        });
                    });

                    SUIUtils.observeDOM(element[0], function() {
                        doTabContentAutoResize(element)
                    });
                }
            }
        }
    }
]);
