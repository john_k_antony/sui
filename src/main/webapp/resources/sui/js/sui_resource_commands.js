'use strict';

define({
    reloadResourcesList: {
        name: "reloadResourcesList",
        iconClass: "glyphicon glyphicon-refresh",
        title: "Refresh",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            if($callerScope.reloadResourcesList) {
            	$callerScope.reloadResourcesList();
            } else {
            	$callerScope.$broadcast("reloadResourcesList");
            }
        }
    },

    newResourceCmd: {
        name: "newResourceCmd",
        title: "New",
        iconClass: "glyphicon glyphicon-file",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            var currentViewId = executeContext.viewId;
            if($callerScope.showResourceUpload) {
            	$callerScope.showResourceUpload(currentViewId);
            } else {
            	$callerScope.$broadcast("showResourceUpload", currentViewId);
            }
        }
    },

    deleteResourceCmd: {
        name: "deleteResourceCmd",
        title: "Delete",
        iconClass: "glyphicon glyphicon-trash",
        enabled: false,
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            if($callerScope.deleteResources) {
            	$callerScope.deleteResources();
            } else {
            	$callerScope.$broadcast("deleteResources");
            }
        }
    },

    setResourcePublic: {
        name: "setResourcePublic",
        iconClass: "glyphicon glyphicon glyphicon-globe",
        title: "Make Public",
        enabled: false,
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            if($callerScope.setResourcePublic) {
            	$callerScope.setResourcePublic();
            } else {
            	$callerScope.$broadcast("setResourceVisibility", 'private', 'public');
            }
        }
    },

    fetchResources: {
        name: "fetchResources",
        execute: function(executeContext, fetchCriteria, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "rest/resource",
                data: fetchCriteria ? fetchCriteria : {
                    fields: ["fileName", "fileType", "fileSize", "filePath", "fileVisibilityValue"]
                },
                willHandleError: executeContext.willHandleError,
                callback: fetchCallback,
                isGetFullResponse: executeContext.isGetFullResponse
            });
        }
    },


    setResourceVisibility: {
        name: "setResourceVisibility",
        execute: function(executeContext, resources, fromVisibility, toVisibility) {
            var $callerScope = executeContext.currentScope;
            var callback = $callerScope.reloadResourcesList;
            var selectedResources = $callerScope.getSelectedResources();
            if (selectedResources && selectedResources.length > 0) {
                var updateData = {};
                updateData.fileName = [];
                updateData.from_visibility = fromVisibility;
                updateData.to_visibility = toVisibility;
                if (selectedResources) {
                    if (SUIService.isArray(selectedResources)) {
                        for (var cc = 0; cc < selectedResources.length; cc++) {
                            updateData.fileName.push(selectedResources[cc].fileName);
                        }
                    } else {
                        updateData.fileName.push(selectedResources.fileName);
                    }
                }
                return SUIService.executeRestCall({
                    method: "PUT",
                    URL: "rest/resource",
                    data: {
                        dataMap: updateData
                    },
                    willHandleError: executeContext.willHandleError,
                    callback: callback,
                    isGetFullResponse: executeContext.isGetFullResponse
                });
            }
        }
    },

    deleteResourceInternal: {
        name: "deleteResourceInternal",
        execute: function(executeContext, resources, deleteCallback) {
            var updateData = {};
            updateData.fileName = [];
            updateData.visibility = "private";
            if (resources) {
                if (SUIService.isArray(resources)) {
                    for (var cc = 0; cc < resources.length; cc++) {
                        updateData.fileName.push(resources[cc].fileName);
                    }
                } else {
                    updateData.fileName.push(resources.fileName);
                }
            }
            return SUIService.executeRestCall({
                method: "DELETE",
                URL: "rest/resource",
                data: updateData,
                willHandleError: executeContext.willHandleError,
                callback: deleteCallback,
                isGetFullResponse: executeContext.isGetFullResponse
            });
        }
    }
});
