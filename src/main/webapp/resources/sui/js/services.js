'use strict'

/* Services */

angular.module('SUIApplication.services', [])
    .value('suiVersion', '1.0')
    .provider('SUIInternalController', [
        '$controllerProvider',
        '$provide',
        function($controllerProvider, $provide) {
            this.$get = function() {
                return {
                    controllerRegistrar: $controllerProvider,
                    serviceRegistrar: $provide
                }
            }
        }
    ])

.factory('SUIUtils', ['$rootScope', '$timeout', '$domUtilityService', function($rootScope, $timeout, $domUtilityService) {

        !(function($, undefined) {
            /// adapted http://jsfiddle.net/drzaus/Hgjfh/5/

            var get_selector = function(element) {
                var pieces = [];

                for (; element && element.tagName !== undefined; element = element.parentNode) {
                    if (element.className && element.className.split) {
                        var classes = element.className.split(' ');
                        for (var i in classes) {
                            if (classes.hasOwnProperty(i) && classes[i]) {
                                pieces.unshift(classes[i]);
                                pieces.unshift('.');
                            }
                        }
                    }
                    if (element.id && !/\s/.test(element.id)) {
                        pieces.unshift(element.id);
                        pieces.unshift('#');
                    }
                    pieces.unshift(element.tagName);
                    pieces.unshift(' > ');
                }

                return pieces.slice(1).join('');
            };

            $.fn.getSelector = function(only_one) {
                if (true === only_one) {
                    return get_selector(this[0]);
                } else {
                    return $.map(this, function(el) {
                        return get_selector(el);
                    });
                }
            };

        })(window.jQuery);

        /**
         * Author: Andrew Hedges, andrew@hedges.name
         * License: free to use, alter, and redistribute without attribution

         * Truncate a string to the given length, breaking at word boundaries and adding an elipsis
         * @param string str String to be and
         * @param integer limit Max length of the string
         * @return string
         */
        var truncate = function(str, limit) {
            var bits, i;
            if ("string" !== typeof str) {
                return '';
            }

            bits = str;

            if (bits.length > limit) {
                var availableChars = limit - 3;
                if (availableChars > 0) {
                    var firstHalfLen = Math.floor(availableChars / 2);
                    var secondHalfLen = limit - 3 - firstHalfLen;
                    if (firstHalfLen > 0) {
                        bits = str.substring(0, firstHalfLen);
                        bits += "...";
                        bits += str.substring((str.length - secondHalfLen), str.length);
                    }
                }
            }
            return bits;
        }

        var domUtilityServiceDigest = $domUtilityService.digest;

        $domUtilityService.digest = function($scope) {
            if (!$scope.$root) {
                return;
            } else {
                domUtilityServiceDigest.apply($domUtilityService, arguments);
            }
        }

        return {
            guid: function() {
                function _p8(s) {
                    var p = (Math.random().toString(16) + "000000000").substr(2, 8);
                    return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
                }
                return _p8() + _p8(true) + _p8(true) + _p8();
            },
            getUniqueId: function(prefix) {
                var d = new Date().getTime();
                d += (parseInt(Math.random() * 100)).toString();
                if (undefined === prefix) {
                    prefix = 'uid-';
                }
                d = prefix + d;
                return d;
            },
            smartConcat: function(items, delimiter) {
                var that = this;
                var d = that.isNull(delimiter) ? " " : delimiter,
                    out = "";
                angular.forEach(items, function(v, k) {
                    if (!that.isNullOrEmpty(v)) {
                        out += v.trim() + d;
                    }
                });
                if (out.length > d.length) {
                    out = out.substring(0, (out.length - d.length));
                }
                return out;
            },
            addThousandsSeparator: function(input) {
                var output = "" + input,
                    parts;
                if (parseFloat(input)) {
                    input = new String(input); // so you can perform string operations
                    parts = input.split("."); // remove the decimal part
                    parts[0] = parts[0].split("").reverse().join("").replace(/(\d{3})(?!$)/g, "$1,").split("").reverse().join("");
                    output = parts.join(".");
                }
                return output;
            },
            parseDate: function(dateString) {
                var d = new Date(dateString);
                return d;
            },
            truncateLabel: function(label, maxNumOfChars) {
                return truncate(label, maxNumOfChars);
            },
            scrollToTop: function(scrollerSelector, scrollDelay) {
                $timeout(function() {
                    $(scrollerSelector).animate({
                        scrollTop: 0
                    }, scrollDelay ? scrollDelay : 0);
                });
            },
            scrollToLocation: function(scrollerSelector, location, scrollDelay) {
                $timeout(function() {
                    $(scrollerSelector).animate({
                        scrollTop: location
                    }, scrollDelay ? scrollDelay : 0);
                });
            },
            scrollToBottom: function(scrollerSelector, overflowElemSelector, scrollDelay) {
                $timeout(function() {
                    $(scrollerSelector).animate({
                        scrollTop: $(overflowElemSelector).outerHeight()
                    }, scrollDelay ? scrollDelay : 0);
                });
            },
            loadCss: function(url) {
                var link = document.createElement("link");
                link.type = "text/css";
                link.rel = "stylesheet";
                link.href = url;
                document.getElementsByTagName("head")[0].appendChild(link);
            },
            observeDOM: (function() {
                var MutationObserver = window.MutationObserver || window.WebKitMutationObserver,
                    eventListenerSupported = window.addEventListener;

                return function(obj, callback) {
                    if (MutationObserver) {
                        // define a new observer
                        var obs = new MutationObserver(function(mutations, observer) {
                            if (mutations[0].addedNodes.length || mutations[0].removedNodes.length)
                                callback();
                        });
                        // have the observer observe foo for changes in children
                        obs.observe(obj, {
                            childList: true,
                            subtree: true
                        });
                    } else if (eventListenerSupported) {
                        obj.addEventListener('DOMNodeInserted', callback, false);
                        obj.addEventListener('DOMNodeRemoved', callback, false);
                    }
                }
            }()),
            ngGridPlugin: {
                autoRedrawOnDataChange: {
                    init: function(childScope, gridInstance) {
                        try {
                            if (gridInstance.eventProvider.setDraggables) {
                                var setD = gridInstance.eventProvider.setDraggables;
                                gridInstance.eventProvider.setDraggables = function() {
                                    if (!gridInstance.$root) {
                                        return;
                                    } else {
                                        setD.apply(gridInstance.eventProvider, arguments);
                                    }
                                }
                            }
                        } catch (e) {

                        }

                        childScope.$watch(gridInstance.config.data, function(newData, oldData) {
                            try {
                                $(gridInstance.$root.parent()).addClass("loadingAnim");
                                $(gridInstance.$root).css({
                                    visibility: "hidden"
                                });
                                if (!newData || newData.length === 0) {
                                    $(gridInstance.$root.parent()).removeClass("loadingAnim");
                                    return;
                                }
                            } catch (e) {
                                //TODO:: ignore
                            }
                            try {
                                $(gridInstance.$root.parent()).trigger('resize.nggrid');
                                $timeout(function() {
                                    $(gridInstance.$root.parent()).removeClass("loadingAnim");
                                    $(gridInstance.$root).css({
                                        visibility: "visible"
                                    });
                                }, 150);
                            } catch (e) {
                                //TODO:: ignore
                            }
                        });
                    }
                }
            },
            putToLocalStorage: function(key, value) {
                if (window.localStorage) {
                    window.localStorage.setItem(key, value);
                }
            },
            getFromLocalStorage: function(key) {
                if (window.localStorage) {
                    return window.localStorage.getItem(key);
                }
                return null;
            }
        }
    }])
    .factory('SUIServiceMessaging', ['$q', '$http', '$timeout', '$window', 'SUIUtils',
        function($q, $http, $timeout, $window, SUIUtils) {

            var messageListeners,
                request,
                socket,
                isInitialized = false;

            function _init() {
                if (!$window.atmosphere) {
                    return;
                }
                messageListeners = [];
                request = {
                    url: 'webmessage',
                    contentType: 'application/json',
                    transport: 'websocket',
                    //logLevel: 'debug',
                    fallbackTransport: 'long-polling',
                    reconnectInterval: 3000,
                    enableProtocol: false,
                    enableXDR: true,
                    timeout: 60000,
                    messageDelimiter: "|"
                };

                request.onError = function() {}

                request.onReconnect = function() {}

                request.onClientTimeout = function() {
                    $timeout(function() {
                        socket = atmosphere.subscribe(request);
                    }, request.reconnectInterval);
                }

                request.onReopen = function() {}
                request.onFailureToReconnect = function() {}
                request.onClose = function() {}
                request.openAfterResume = function() {}
                request.onOpen = function() {}

                //onMessage is triggered when Atmosphere server sends an asynchronous message to the browser.
                request.onMessage = function(response) {
                    try {
                        var responseBody = response.responseBody; // response body is <id> <messageDelimiter> <message>. Need to parse the message
                        if (responseBody) {
                            var msgFragments = responseBody.split(request.messageDelimiter)
                            if (msgFragments.length > 1) {
                                var message = atmosphere.util.parseJSON(msgFragments[1]);
                                //console.log(message);
                                if (message) {
                                    angular.forEach(messageListeners, function(v, k) {
                                        if (!v.__sui_msg_topics || v.__sui_msg_topics.indexOf(message.messageTopic) > -1)
                                            try {
                                                v.onMessage(message);
                                            } catch (e) {}
                                    });
                                }
                            }
                        }
                    } catch (e) {
                        console.log("On Message Error");
                        console.log(response.responseBody)
                        console.log(e);
                    }
                }
                socket = atmosphere.subscribe(request);
                isInitialized = true;
            }

            return {
                init: function() {
                    _init();
                },
                push: function(topic, data) {
                    if (!isInitialized) {
                        throw "Messaging service not initialized.";
                    }
                    socket.push(atmosphere.util.stringifyJSON(data));
                },
                subscribe: function(messageListener, messageTopic) {
                    if (!isInitialized) {
                        throw "Messaging service not initialized.";
                    }
                    if (messageTopic) {
                        if (!messageListener.__sui_msg_topics) {
                            messageListener.__sui_msg_topics = [];
                        }
                        messageListener.__sui_msg_topics.addIfNotPresent(messageTopic);
                    }
                    if (!messageListener.__sui_msg_listener_id) {
                        messageListener.__sui_msg_listener_id = SUIUtils.getUniqueId("MsgListner-");
                        messageListeners.push(messageListener);
                    }
                },
                unsubscribe: function(messageListener, messageTopic) {
                    if (!isInitialized) {
                        throw "Messaging service not initialized.";
                    }
                    if (messageTopic) {
                        if (messageListener.__sui_msg_topics) {
                            var ti = messageListener.__sui_msg_topics.indexOf(messageTopic)
                            if (ti > -1) {
                                messageListener.__sui_msg_topics.splice(ti, 1);
                            }
                        }
                    }

                    if (!messageTopic || !messageListener.__sui_msg_topics || messageListener.__sui_msg_topics.length === 0) {
                        var index = messageListeners.indexOf(messageListener);
                        if (index > -1) {
                            messageListeners.splice(index, 1);
                        }
                    }
                }
            };
        }
    ])
    .factory('SUIService', [
        '$rootScope',
        '$timeout',
        '$window',
        '$q',
        '$log',
        '$injector',
        '$compile',
        '$sce',
        '$location',
        '$interval',
        '$templateCache',
        '$http',
        'SUIServiceHTTP',
        'SUIInternalController',
        'SUIServiceMessaging',
        function($rootScope, $timeout, $window, $q, $log, $injector, $compile, $sce, $location, $interval, $templateCache, $http, SUIServiceHTTP, SUIInternalController, SUIServiceMessaging) {
            // Defining helper functions
            if (typeof Object.create !== 'function') {
                Object.create = function(o) {
                    function F() {}
                    F.prototype = o;
                    return new F();
                };
            }

            if (typeof Array.prototype.addIfNotPresent !== 'function') {
                Array.prototype.addIfNotPresent = function(newItem, equalsFn, isMergeIfFound) {
                    var isItemFound = false;
                    var foundItem = null;
                    this.forEach(function(value) {
                        if (!isItemFound) {
                            if (equalsFn) {
                                if (equalsFn(value, newItem)) {
                                    foundItem = value;
                                    isItemFound = true;
                                }
                            } else if (value === newItem) {
                                foundItem = value;
                                isItemFound = true;
                            }
                        }
                    });
                    if (!isItemFound) {
                        this.push(newItem);
                    } else if (isMergeIfFound) {
                        foundItem = angular.extend(foundItem, newItem);
                    }
                }
            }

            if (typeof Array.prototype.empty !== 'function') {
                Array.prototype.empty = function() {
                    while (this.length > 0) {
                        this.pop();
                    }
                }
            }

            if (typeof Array.prototype.intersectWith !== 'function') {
                Array.prototype.intersectWith = function(otherArray, equalsFn, updateIfPresent, updateFn) {
                    var c,
                        currValue,
                        foundValue,
                        foundItem;
                    for (c = this.length; c >= 0; c--) {
                        currValue = this[c];
                        foundValue = null;
                        foundItem = false;
                        otherArray.forEach(function(value) {
                            if (equalsFn) {
                                if (equalsFn(currValue, value)) {
                                    foundItem = true;
                                    foundValue = value;
                                }
                            } else if (currValue === value) {
                                foundItem = true;
                                foundValue = value;
                            }
                        });
                        if (!foundItem) {
                            this.splice(c, 1);
                        } else {
                            if (updateIfPresent) {
                                if (updateFn) {
                                    updateFn(foundValue, currValue);
                                } else {
                                    angular.copy(foundValue, currValue);
                                }
                            }
                        }
                    }
                }
            }

            var _appContextSetterDefer = $q.defer();

            var _resourceBundles = {};
            var _applicationRuntimeContext = {};
            var _applicationInitialized = false;

            var _USER_PROFILE_DATA_ATTR = "USER_PROFILE_DATA";

            var _applicationState = null;

            var _currentWorkspace = null;

            var _applicationContext = null;

            var applicationAPIs = {};
            var applicationCommands = {};
            var applicationWidgets = {};
            var applicationMenus = {};
            var applicationHeaderItems = {};
            var applicationSecondaryItems = {};
            var applicationWorkspaces = {};

            var applicationViews = {};

            var _HEADER_POS_LEFT = "header_pos_left";
            var _HEADER_POS_RIGHT = "header_pos_right";

            var _VIEW_ID_COUNTER = 0;

            var busyViewIdMap = {};

            var homeWSName = 'home';

            var suiCommands = [
                'resources/sui/js/sui_resource_commands',
            ]

            var nextIDCounter = -1;
            var idCounterMap = {};

            var subApplication = null;


            var longRunningCommandMap = {

            }

            var SUI_TEMPLATES = {
                "suiForm.html": "resources/sui/templates/html/suiForm.html",
                "suiFormItem.html": "resources/sui/templates/html/suiFormItem.html"
            }

            return {

                STATUS_CODE_UNAUTHORIZED: 401,
                STATUS_CODE_CSRF_TOKEN_INVALID: 9001,
                STATUS_CODE_OK: 1200,
                HTTP_STATUS_CODE_OK: 200,
                HTTP_STATUS_NO_DATA_OK: 204,

                //Application States
                APPLICATION_NOT_INITITALIZED: "APPLICATION_NOT_INITITALIZED",
                APPLICATION_INITIALIZING: "APPLICATION_INITIALIZING",
                APPLICATION_INITIALIZED: "APPLICATION_INITIALIZED",
                APPLICATION_ALREADY_INITIALIZED: "APPLICATION_ALREADY_INITIALIZED",
                APPLICATION_INIT_FAILED: "APPLICATION_INIT_FAILED",

                //Events
                COMMANDS_STATE_UPDATED: "COMMANDS_STATE_UPDATED",
                WORKSPACE_SWITCH: "WORKSPACE_SWITCH",
                VIEW_STATE_CHANGE: "VIEW_STATE_CHANGE",

                // Message Types
                FATAL_MESSAGE: "FATAL_MESSAGE",
                ERROR_MESSAGE: "ERROR_MESSAGE",
                WARNING_MESSAGE: "WARNING_MESSAGE",
                INFO_MESSAGE: "INFO_MESSAGE",
                ALERT_MESSAGE: "ALERT_MESSAGE",

                APPLICATION_ERROR: "APPLICATION_ERROR",
                INTERNAL_ERROR: "INTERNAL_ERROR",
                NULL_POINTER_EXCEPTION: "NULL_POINTER_EXCEPTION",
                ILLEGAL_ARGUMENT_EXCEPTION: "ILLEGAL_ARGUMENT_EXCEPTION",
                NO_APPLICATION_REGISTERED: "NO_APPLICATION_REGISTERED",

                MENU_SEPARATOR: "menu_separator",
                POSITION_BEFORE: "before",
                POSITION_AFTER: "after",
                TYPE_COMMAND: "command",
                TYPE_MENU_COMMAND: "menuCommand",
                TYPE_POPUP_MENU_COMMAND: "popupMenuCommand",
                TYPE_WIDGET: "widget",
                TYPE_MENU: "menu",
                TYPE_WORKSPACE: "workspace",
                HEADER_POSITION_LEFT: _HEADER_POS_LEFT,
                HEADER_POSITION_RIGHT: _HEADER_POS_RIGHT,

                HEADER_POSITIONS: [{
                    position: _HEADER_POS_LEFT,
                    class: "navbar-left"
                }, {
                    position: _HEADER_POS_RIGHT,
                    class: "navbar-right",
                    reverseOrder: true
                }],

                USER_ACTIONS_MENU_ITEM: "userActions",
                CONFIGURE_APPLICATION_MENU_ITEM: "configureApplication",
                ABOUT_APPLICATION_MENU_ITEM: "aboutApplication",

                DIALOG_TYPE_INFO: "info",
                DIALOG_TYPE_SUCCESS: "success",
                DIALOG_TYPE_WARNING: "warning",
                DIALOG_TYPE_ERROR: "error",
                DIALOG_TYPE_CONFIRM: "confirm",
                DIALOG_TYPE_CUSTOM: "custom",
                DIALOG_TYPE_PLAIN: "plain",

                DIALOG_OK: "OK",
                DIALOG_CANCEL: "CANCEL",
                DIALOG_RESET: "RESET",

                /* Command Icon Location */

                COMMAND_ICON_LOCATION_LEFT: "L",
                COMMAND_ICON_LOCATION_RIGHT: "R",
                COMMAND_ICON_LOCATION_TOP: "T",
                COMMAND_ICON_LOCATION_BOTTOM: "B",


                /* Menu Item Types */
                MENU_ITEM_SINGLE_COMMAND: "SC",
                MENU_ITEM_MULTI_COMMANDS: "MC",
                MENU_ITEM_SEPARATOR: "SI",
                MENU_ITEM_TEMPLATE: "MT",
                MENU_ITEM_STATIC_TEXT: "ST",

                ALIGN_LEFT: "l",
                ALIGN_CENTER: "c",
                ALIGN_RIGHT: "r",

                MenuItemBuilder: {
                    createSeparator: function() {
                        return {
                            name: SUIService.MENU_ITEM_SEPARATOR,
                            type: SUIService.MENU_ITEM_SEPARATOR,
                            staticItem: true
                        }
                    },
                    createStaticTextItem: function(text, textStyle) {
                        return {
                            name: SUIService.MENU_ITEM_STATIC_TEXT,
                            type: SUIService.MENU_ITEM_STATIC_TEXT,
                            param: text,
                            style: textStyle,
                            staticItem: true
                        }
                    },
                    createSingleCommandMenuItem: function(menuItemName, cmdName) {
                        return {
                            name: menuItemName,
                            type: SUIService.MENU_ITEM_SINGLE_COMMAND,
                            param: cmdName
                        }
                    },
                    createMultiCommandMenuItem: function(menuItemName, cmdNameList) {
                        return {
                            name: menuItemName,
                            type: SUIService.MENU_ITEM_MULTI_COMMANDS,
                            param: cmdNameList,
                            defaultCommand: null,
                            delayExecCommand: null,
                            executeCommand: function(cmdName, containerScope) {
                                if (this._execCommand) {
                                    return this._execCommand(cmdName);
                                } else {
                                    this.delayExecCommand = cmdName;
                                }
                            }
                        }
                    },
                    addCommandToMultiCommandMenuItem: function(menuItem, cmdName) {
                        if (menuItem) {
                            switch (menuItem.type) {
                                case SUIService.MENU_ITEM_MULTI_COMMANDS:
                                    if (menuItem.param.indexOf(cmdName) < 0) {
                                        menuItem.param.push(cmdName);
                                        if (menuItem.data) {
                                            menuItem.data.push(SUIService.getCommand(cmdName));
                                        }
                                    }
                                    break;
                            }
                        }
                    },
                    addCommandsToMultiCommandMenuItem: function(menuItem, cmdList) {
                        var that = this;
                        angular.forEach(cmdList, function(v, k) {
                            that.addCommandToMultiCommandMenuItem(menuItem, v);
                        });
                    },
                    createTemplateMenuItem: function(menuItemName, strTemplate) {
                        return {
                            name: menuItemName,
                            type: SUIService.MENU_ITEM_TEMPLATE,
                            param: strTemplate
                        }
                    },
                    _getMenuItemsMap: function(menuItems) {
                        var retMap = {};
                        angular.forEach(menuItems, function(val, key) {
                            if (!val.staticItem) {
                                retMap[val.name] = val;
                            }
                        });
                        return retMap;
                    },
                    _processMenuItem: function(menuItem, containerScope, menuItemScope) {

                        if (!SUIService.isNull(containerScope)) {
                            containerScope[menuItem.name] = {
                                radioModel: ""
                            }
                        }

                        if (menuItem) {
                            switch (menuItem.type) {
                                case SUIService.MENU_ITEM_SINGLE_COMMAND:
                                    menuItem.data = SUIService.getCommand(menuItem.param);
                                    break;
                                case SUIService.MENU_ITEM_MULTI_COMMANDS:
                                    menuItem.data = [];
                                    angular.forEach(menuItem.param, function(v, k) {
                                        menuItem.data.push(SUIService.getCommand(v));
                                    });
                                    menuItem._execCommand = function(cmdName) {
                                        if (menuItemScope && menuItemScope.externalExecuteItemCommand) {
                                            menuItemScope.externalExecuteItemCommand(this, cmdName);
                                        }
                                    }
                                    if (menuItem.delayExecCommand) {
                                        menuItem._execCommand(menuItem.delayExecCommand);
                                        menuItem.delayExecCommand = null;
                                    }
                                    break;
                                case SUIService.MENU_ITEM_TEMPLATE:
                                    menuItem.data = $sce.trustAsHtml(menuItem.param);
                                    // menuItem.data = menuItem.param;
                                    break;
                            }
                        }
                    }
                },

                Formatters: {
                    formatDate: function(date, isShowTime) {
                        var d = date,
                            retString;
                        if (angular.isNumber(d)) {
                            d = new Date(date);
                        }
                        if (d) {
                            retString = d.toLocaleDateString();
                            if (isShowTime) {
                                retString += " " + d.toLocaleTimeString();
                            }
                        }
                        return retString;
                    }
                },

                clone: function(originalObject) {
                    return angular.copy(originalObject);
                },

                isArray: function(arrayObj) {
                    return angular.isArray(arrayObj);
                },

                setSubApplication: function(subApp) {
                    subApplication = subApp;
                },

                getSubApplication: function() {
                    return subApplication;
                },

                getService: function(serviceName) {
                    return $injector.get(serviceName);
                },

                showAppBusy: function(message) {
                    $rootScope.showBusy = true;
                    $rootScope.waitMessage = message;
                },

                hideAppBusy: function() {
                    $rootScope.showBusy = false;
                    $rootScope.waitMessage = null;
                },

                showViewBusy: function(viewId, message) {
                    var viewScope = this.getView(viewId);
                    if (viewScope) {
                        viewScope.showBusy = true;
                        viewScope.waitMessage = message;
                    }
                },

                hideViewBusy: function(viewId) {
                    var viewScope = this.getView(viewId);
                    if (viewScope) {
                        viewScope.showBusy = false;
                        viewScope.waitMessage = null;
                    }
                },

                getApplicationState: function() {
                    if (_applicationState) {
                        return _applicationState;
                    } else {
                        return this.APPLICATION_NOT_INITITALIZED;
                    }
                },

                registerResourceBundle: function(bundleName, bundle) {
                    _resourceBundles["RB_" + bundleName] = bundle;
                },

                formatMessage: function(bundleName, bundleNameSapce, messageId, messageArgs) {
                    if (_resourceBundles["RB_" + bundleName] !== undefined) {
                        if (messageId != null) {
                            messageId = messageId.replace(/\./g, '_');
                        }
                        if (_resourceBundles["RB_" + bundleName][bundleNameSapce] &&
                            _resourceBundles["RB_" + bundleName][bundleNameSapce][messageId]) {
                            var msgStr = _resourceBundles["RB_" + bundleName][bundleNameSapce][messageId];
                            if (messageArgs !== null && messageArgs instanceof Array) {
                                return msgStr.replace(/\{(\d+)\}/g, function() {
                                    return messageArgs[arguments[1]];
                                });
                            } else {
                                return msgStr;
                            }
                        }
                    }
                    return messageId;
                },

                createMessage: function(messageType, exception, parameters, userMessage) {
                    return {
                        messageType: messageType,
                        exception: exception,
                        params: parameters,
                        userMessage: userMessage
                    };
                },

                setApplicationContext: function(appContext) {
                    _applicationContext = angular.copy(appContext);
                    _appContextSetterDefer.resolve();
                },

                getApplicationContext: function() {
                    return _applicationContext;
                },

                getUserProfileData: function() {
                    if (_applicationRuntimeContext[_USER_PROFILE_DATA_ATTR]) {
                        return angular.copy(_applicationRuntimeContext[_USER_PROFILE_DATA_ATTR]);
                    } else {
                        if (this.getApplicationContext().userDataProfile) {
                            return this.getApplicationContext().userDataProfile;
                        } else {
                            return {
                                userName: "<<unknown>>"
                            };
                        }
                    }
                },
                onMessage: function(message) {
                    if (message && message.messageTopic === "LR_UPDATE") {
                        this.updateCommandExecutionStatus(message.messageData);
                    }
                },
                initializeApplication: function(isForce) {
                    try {
                        SUIServiceMessaging.subscribe(this);
                    } catch (e) {
                        //Ignore. No messaging service. Contine without it.
                    }

                    var initDef = $q.defer(),
                        that = this;
                    $q.when(_appContextSetterDefer.promise).then(function() {
                        if ((!that.isAppInitialized() && !that.isAppInitializing()) || (isForce && isForce === true)) {
                            _applicationState = that.APPLICATION_INITIALIZING;
                            if (that.getApplicationContext().applicationStartupServerCommand) {
                                $q.when(SUIServiceHTTP.sendRequest(
                                    "GET",
                                    that.getApplicationContext().applicationStartupServerCommand,
                                    that.getApplicationContext().applicationStartupServerCommandParams,
                                    null,
                                    null,
                                    true,
                                    true
                                )).then(function(data) {
                                    if (data.status === that.HTTP_STATUS_CODE_OK && data.data.responseStatusCode === that.STATUS_CODE_OK) {
                                        _applicationRuntimeContext[_USER_PROFILE_DATA_ATTR] = data.data.data;
                                        that.initializeApplicationInternal(initDef);
                                    } else {
                                        _applicationState = that.APPLICATION_INIT_FAILED;
                                        initDef.reject(that.APPLICATION_INIT_FAILED, data);
                                    }
                                }, function(data) {
                                    _applicationState = that.APPLICATION_INIT_FAILED;
                                    initDef.reject(that.APPLICATION_INIT_FAILED, data);
                                });
                            } else {
                                that.initializeApplicationInternal(initDef);
                            }
                        } else {
                            if (that.isAppInitialized()) {
                                initDef.reject(that.APPLICATION_ALREADY_INITIALIZED);
                            } else if (that.isAppInitializing()) {
                                initDef.reject(that.APPLICATION_INITIALIZING);
                            }
                        }
                    });
                    return initDef.promise;
                },

                initializeApplicationInternal: function(initDef) {
                    var that = this;

                    $q.when(this.setupApplication()).then(
                        function() {
                            if (that.getApplicationContext().startupCallback) {
                                $q.when(that.getApplicationContext().startupCallback()).then(function() {
                                        _applicationState = that.APPLICATION_INITIALIZED;
                                        initDef.resolve(that.APPLICATION_INITIALIZED);
                                    },
                                    function(error) {
                                        _applicationState = that.APPLICATION_INIT_FAILED;
                                        initDef.reject(that.APPLICATION_INIT_FAILED, error);
                                    })


                            } else {
                                _applicationState = that.APPLICATION_INITIALIZED;
                                initDef.resolve(that.APPLICATION_INITIALIZED);
                            }
                        },
                        function(error) {
                            _applicationState = that.APPLICATION_INIT_FAILED;
                            initDef.reject(that.APPLICATION_INIT_FAILED, error);
                        }
                    );
                },

                isAppInitialized: function() {
                    return _applicationState && _applicationState === this.APPLICATION_INITIALIZED;
                },

                isAppInitializing: function() {
                    return _applicationState && _applicationState === this.APPLICATION_INITIALIZING;
                },

                loadApplicationCommands: function() {
                    var that = this;
                    var loadCmdDefer = $q.defer();

                    var loadSuiCmdDefer = $q.defer();

                    if (suiCommands && suiCommands.length > 0) {
                        requirejs(suiCommands,
                            function() {
                                for (var a = 0; a < arguments.length; a++) {
                                    for (var cmd in arguments[a]) {
                                        that.registerCommandObject(arguments[a][cmd]);
                                    }
                                }
                                loadSuiCmdDefer.resolve();
                            });
                    } else {
                        loadSuiCmdDefer.resolve();
                    }

                    $q.when(loadSuiCmdDefer.promise).then(function() {
                        if (that.getApplicationContext().applicationCommandResources &&
                            angular.isArray(that.getApplicationContext().applicationCommandResources)) {

                            requirejs(that.getApplicationContext().applicationCommandResources,
                                function() {
                                    for (var a = 0; a < arguments.length; a++) {
                                        for (var cmd in arguments[a]) {
                                            that.registerCommandObject(arguments[a][cmd]);
                                        }
                                    }
                                    loadCmdDefer.resolve();
                                });
                        } else {
                            loadCmdDefer.resolve();
                        }
                    })

                    return loadCmdDefer.promise;
                },

                loadApplicationControllers: function() {
                    var that = this;
                    var loadCtrlDefer = $q.defer();

                    if (this.getApplicationContext().applicationControllerResources &&
                        angular.isArray(this.getApplicationContext().applicationControllerResources)) {

                        requirejs(this.getApplicationContext().applicationControllerResources,
                            function() {
                                for (var a = 0; a < arguments.length; a++) {
                                    for (var ctrl in arguments[a]) {
                                        SUIInternalController.controllerRegistrar.register(ctrl, ['$scope', 'SUIService', arguments[a][ctrl]]);
                                    }
                                }
                                loadCtrlDefer.resolve();
                            });
                    } else {
                        loadCtrlDefer.resolve();
                    }
                    return loadCtrlDefer.promise;
                },

                loadApplicationServices: function() {
                    var that = this;
                    var loadSvcDefer = $q.defer();

                    if (this.getApplicationContext().applicationServiceResources &&
                        angular.isArray(this.getApplicationContext().applicationServiceResources)) {
                        requirejs(this.getApplicationContext().applicationServiceResources,
                            function() {
                                for (var a = 0; a < arguments.length; a++) {
                                    for (var svc in arguments[a]) {
                                        SUIInternalController.serviceRegistrar
                                            .service(svc, ['SUIService', arguments[a][svc]])
                                    }
                                }
                                loadSvcDefer.resolve();
                            });
                    } else {
                        loadSvcDefer.resolve();
                    }
                    return loadSvcDefer.promise;
                },

                loadApplicationFactories: function() {
                    var that = this;
                    var loadSvcDefer = $q.defer();
                    if (this.getApplicationContext().applicationFactoryResources &&
                        angular.isArray(this.getApplicationContext().applicationFactoryResources)) {

                        requirejs(this.getApplicationContext().applicationFactoryResources,
                            function() {
                                for (var a = 0; a < arguments.length; a++) {
                                    for (var svc in arguments[a]) {
                                        SUIInternalController.serviceRegistrar
                                            .factory(svc, ['SUIService', arguments[a][svc]])
                                    }
                                }
                                loadSvcDefer.resolve();
                            });
                    } else {
                        loadSvcDefer.resolve();
                    }
                    return loadSvcDefer.promise;
                },

                loadApplicationCSS: function() {
                    var that = this;
                    $q.when(_appContextSetterDefer.promise).then(function() {
                        if (that.getApplicationContext().applicationCssFile) {
                            var link = document.createElement("link");
                            link.type = "text/css";
                            link.rel = "stylesheet";
                            link.href = that.getApplicationContext().applicationCssFile;
                            document.getElementsByTagName("head")[0].appendChild(link);
                        }
                    });
                },

                loadSUITemplates: function() {
                    var allTemplateFetchDefer = [],
                        templateFetchDefer = $q.defer();
                    $q.when(_appContextSetterDefer.promise).then(function() {
                        angular.forEach(SUI_TEMPLATES, function(v, k) {
                            allTemplateFetchDefer.push(SUIServiceHTTP.sendRequest(
                                "GET",
                                v,
                                null,
                                null,
                                null,
                                true,
                                true, {
                                    name: k
                                }
                            ));
                        });

                        $q.all(allTemplateFetchDefer).then(function(responses) {
                                angular.forEach(responses, function(v, k) {
                                    $templateCache.put(v.SUI_REQUEST_META_DATA.name, v.data);
                                });
                                templateFetchDefer.resolve();
                            },
                            function() {
                                templateFetchDefer.reject();
                            });
                    });
                    return templateFetchDefer.promise;
                },

                setUserName: function(userName) {
                    var userMenuItem = applicationMenus[this.USER_ACTIONS_MENU_ITEM];
                    this.getApplicationContext().userDataProfile.userName = userName;
                    if (userMenuItem) {
                        userMenuItem.title = this.getUserDisplayName();
                    }
                },

                setupApplication: function() {

                    var that = this;

                    var setupDef = $q.defer();

                    var companyLogoHI = this.registerWidget("companyLogo", function() {
                        return "<img ng-src='{{suiService.getApplicationContext().appLogo}}'/>"
                    });
                    var appNameHI = this.registerWidget("applicationName", function() {
                        return "<a class='navbar-brand' href='#'>&nbsp;&nbsp;{{suiService.getApplicationContext().title}}</a>";
                    });
                    this.addHeaderItem(companyLogoHI, this.HEADER_POSITION_LEFT);
                    this.addHeaderItem(appNameHI, this.HEADER_POSITION_LEFT);

                    var userMenuHI = this.registerMenu(
                        this.USER_ACTIONS_MENU_ITEM,
                        this.getUserDisplayName(),
                        null,
                        "glyphicon glyphicon-user"
                    );

                    var configureMenuHI = this.registerMenu(
                        this.CONFIGURE_APPLICATION_MENU_ITEM,
                        "",
                        null,
                        "glyphicon glyphicon-cog"
                    );

                    var aboutAppHI = this.registerMenu(
                        this.ABOUT_APPLICATION_MENU_ITEM,
                        "",
                        null,
                        "glyphicon glyphicon-question-sign"
                    );

                    this.addHeaderItem(userMenuHI, this.HEADER_POSITION_RIGHT);
                    this.addHeaderItem(configureMenuHI, this.HEADER_POSITION_RIGHT);
                    this.addHeaderItem(aboutAppHI, this.HEADER_POSITION_RIGHT);

                    $q.all([this.loadApplicationServices(), this.loadApplicationFactories(), this.loadApplicationCommands(), this.loadApplicationControllers()]).then(function() {
                        if (that.getApplicationContext().bootstrapApplication) {
                            $q.when(that.getApplicationContext().bootstrapApplication()).then(
                                function() {
                                    var workspaces = that.getApplicationContext().getWorkspaces();
                                    angular.forEach(workspaces, function(ws, idx) {
                                        if (ws.isHome) {
                                            homeWSName = ws.name;
                                        }
                                        that.registerWorkspaceObject(ws);
                                    });
                                    setupDef.resolve();
                                },
                                function(error) {
                                    setupDef.reject(error);
                                }
                            );
                        } else {
                            setupDef.resolve();
                        }
                    });

                    return setupDef.promise;
                },

                getUserDisplayName: function() {
                    var usedDisplayName = "";
                    if (this.getUserProfileData().userFirstName &&
                        this.getUserProfileData().userFirstName.trim() !== "" &&
                        this.getUserProfileData().userFirstName.trim() !== "null") {
                        usedDisplayName += this.getUserProfileData().userFirstName.trim();
                    }
                    if (this.getUserProfileData().userLastName &&
                        this.getUserProfileData().userLastName.trim() !== "" &&
                        this.getUserProfileData().userLastName.trim() !== "null") {
                        if (usedDisplayName != "") {
                            usedDisplayName += " " + this.getUserProfileData().userLastName.trim();
                        } else {
                            usedDisplayName = this.getUserProfileData().userLastName.trim();
                        }
                    }
                    if (usedDisplayName === "") {
                        usedDisplayName = this.getUserProfileData().userName;
                    }
                    return usedDisplayName;
                },

                getHeaderView: function() {
                    var headerView = 'resources/sui/templates/html/headerNavBarView.html'
                    if (this.getApplicationContext().headerView) {
                        headerView = this.getApplicationContext().headerView;
                    }
                    return headerView;
                },

                getWorkspaceSwitchView: function() {
                    var wsSwitch = 'resources/sui/templates/html/workspaceSwitchView.html'
                    if (this.getApplicationContext().workspaceSwitchView) {
                        wsSwitch = this.getApplicationContext().workspaceSwitchView;
                    }
                    return wsSwitch;
                },

                getFooterView: function() {
                    var footerView = 'resources/sui/templates/html/footerView.html'
                    if (this.getApplicationContext().headerView) {
                        footerView = this.getApplicationContext().footerView;
                    }
                    return footerView;
                },

                registerCommandObject: function(cmdObj) {
                    if (cmdObj && cmdObj.name) {
                        if (applicationCommands[cmdObj.name]) {
                            $log.warn("SUIService : Trying to register command : '" + cmdObj.name + "' again. Will OVERWRITE the previous command definition!");
                        }

                        if (this.isNull(cmdObj.iconLocation)) {
                            cmdObj.iconLocation = this.COMMAND_ICON_LOCATION_LEFT;
                        }

                        if (this.isNull(cmdObj.class)) {
                            cmdObj.class = "btn btn-primary sui-toolbar-button";
                        }

                        //TODO: enable / disable command based on privileges
                        applicationCommands[cmdObj.name] = cmdObj;
                        cmdObj.enabled = (cmdObj.enabled === undefined) || (cmdObj.enabled !== true ? false : true);
                        cmdObj.SUIService = this;

                        if (cmdObj.menuItems) {
                            cmdObj.menuItemsMap = this.MenuItemBuilder._getMenuItemsMap(cmdObj.menuItems);
                        }

                        cmdObj.addMenuItem = function(menuItem) {
                            if (!this.menuItems) {
                                this.menuItems = [];
                            }
                            this.menuItems.push(menuItem);
                            this.menuItemsMap = SUIService.MenuItemBuilder._getMenuItemsMap(this.menuItems);
                        }

                        cmdObj.addMenuItems = function(menuItems, isClear) {
                            var that = this;
                            if (!this.menuItems) {
                                this.menuItems = [];
                            }
                            if (isClear && this.menuItems) {
                                this.menuItems.empty();
                            }
                            angular.forEach(menuItems, function(v, k) {
                                that.menuItems.push(v);
                            });
                            this.menuItemsMap = SUIService.MenuItemBuilder._getMenuItemsMap(this.menuItems);
                        }

                        return applicationCommands[cmdObj.name];
                    } else {
                        $log.warn("SUIService : Unable to register command : '" + cmdObj.name + "'.");
                    }
                },

                registerCommand: function(name, title, icon, iconClass, executeFn, onSuccessCB, onErrorCB, cmdPrivileges, enabled) {
                    var cmdObj = {
                        name: name,
                        type: this.TYPE_COMMAND,
                        title: title,
                        icon: icon,
                        iconClass: iconClass,
                        execute: executeFn,
                        onSuccessCB: onSuccessCB,
                        onErrorCB: onErrorCB,
                        cmdPrivileges: cmdPrivileges,
                        enabled: enabled
                    }
                    return this.registerCommandObject(cmdObj);
                },

                registerWorkspaceObject: function(wsObj) {
                    if (wsObj && wsObj.name) {
                        if (applicationWorkspaces[wsObj.name]) {
                            $log.warn("SUIService : Trying to register workspace : '" + wsObj.name + "' again. Will OVERWRITE the previous workspace definition!");
                        }
                        applicationWorkspaces[wsObj.name] = wsObj;
                        return applicationWorkspaces[wsObj.name];
                    } else {
                        $log.warn("SUIService : Unable to register workspace : '" + wsObj.name + "'.");
                    }
                },

                registerWorkspace: function(name, title, icon, iconClass, contentsURL, onBeforeLoad, onBeforeUnLoad, isHome) {
                    var wsObj =
                        wsObj = {
                            name: name,
                            type: this.TYPE_WORKSPACE,
                            title: title,
                            icon: icon,
                            iconClass: iconClass,
                            onBeforeLoad: onBeforeLoad,
                            onBeforeUnLoad: onBeforeUnLoad,
                            contentsURL: contentsURL,
                            isHome: isHome
                        }
                    return this.registerWorkspaceObject(cmdObj);
                },

                getAllWorkspaceNames: function() {
                    var allWorkspaces = this.getAllWorkspaces();
                    var wsNames = [];
                    for (var ws = 0; ws < allWorkspaces.length; ws++) {
                        wsNames.push(workspace.name);
                    }
                    return wsNames;
                },

                getAllWorkspaces: function() {
                    var workspaces = [];
                    for (var ws in applicationWorkspaces) {
                        var workspace = applicationWorkspaces[ws];
                        if (workspace && workspace.name) {
                            if (workspace.isHome) {
                                // Add Home workspace in the beginning
                                workspaces.splice(0, 0, workspace);
                            } else {
                                workspaces.push(workspace);
                            }
                        }
                    }
                    return workspaces;
                },

                getWorkspace: function(name) {
                    return applicationWorkspaces[name];
                },

                getCurrentWorkspace: function() {
                    return _currentWorkspace;
                },

                getCurrentWorkspaceName: function() {
                    return _currentWorkspace ? _currentWorkspace.name : null;
                },

                getWorkspaceContentsURL: function(workspaceName) {
                    if (!workspaceName) {
                        workspaceName = homeWSName;
                    }
                    var ws = this.getWorkspace(workspaceName);
                    if (ws) {
                        var contentsURL = this.getWorkspace(workspaceName).contentsURL;
                        return contentsURL
                    } else {
                        this._selectWorkspaceInternal(homeWSName);
                        return null;
                    }
                },

                navigateToWorkspace: function(workspaceName) {
                    if (!workspaceName) {
                        workspaceName = homeWSName;
                    }
                    var ws = this.getWorkspace(workspaceName);
                    if (ws) {
                        $location.path("/" + workspaceName);
                    } else {
                        $log.warn("SUIService: Unable to navigate to workspace '" + workspaceName + "'. Workspace DOES NOT exist.");
                    }
                },

                _selectWorkspaceInternal: function(workspaceName, doNotNavigate, cb, initParams) {
                    if (!workspaceName) {
                        workspaceName = homeWSName;
                    }
                    var that = this;
                    var ws = this.getWorkspace(workspaceName);
                    if (ws) {
                        var oldWSName = null;
                        if (_currentWorkspace) {
                            _currentWorkspace.active = false;
                            oldWSName = _currentWorkspace.name;
                        }
                        ws.active = true;
                        ws.loaded = true;
                        _currentWorkspace = ws;
                        if (ws.rendered) {
                            if (cb) {
                                $rootScope.$evalAsync(function() {
                                    $timeout(function() {
                                        if (cb) {
                                            cb();
                                        }
                                    })
                                });
                            }
                        } else {
                            ws.onRenderCb = cb;
                        }
                        $rootScope.$broadcast(this.WORKSPACE_SWITCH, {
                            "new": workspaceName,
                            "old": oldWSName,
                            "initParams": initParams
                        });
                        $rootScope.$broadcast(this.VIEW_STATE_CHANGE);
                    } else {
                        $log.warn("SUIService: Unable to select workspace '" + workspaceName + "'. Workspace DOES NOT exist.");
                    }
                },

                selectWorkspace: function(workspaceName, cb, initParams) {
                    if (!workspaceName) {
                        workspaceName = homeWSName;
                    }
                    var that = this;
                    var ws = this.getWorkspace(workspaceName);
                    if (ws) {
                        if (ws.onBeforeLoad) {
                            $q.when(ws.onBeforeLoad()).then(
                                function() {
                                    that._selectWorkspaceInternal(workspaceName, null, cb, initParams);
                                },
                                function() {
                                    $log.warn("SUIService: Cannot select workspace '" + workspaceName + "'. Workspace DOES NOT want to load.");
                                    if (_currentWorkspace) {
                                        that._selectWorkspaceInternal(_currentWorkspace.name);
                                    } else {
                                        that._selectWorkspaceInternal(homeWSName);
                                    }
                                });
                        } else {
                            that._selectWorkspaceInternal(workspaceName, null, cb, initParams);
                        }
                    } else {
                        $log.warn("SUIService: Unable to select workspace '" + workspaceName + "'. Workspace DOES NOT exist.");
                    }
                },

                onWorkspaceSelect: function(workspaceName) {
                    if (!workspaceName) {
                        workspaceName = homeWSName;
                    }
                    if (workspaceName === this.getCurrentWorkspaceName()) {
                        return;
                    }
                    var that = this;
                    if (_currentWorkspace != null) {
                        if (_currentWorkspace.onBeforeUnload) {
                            $q.when(_currentWorkspace.onBeforeUnload()).then(
                                function() {
                                    that.selectWorkspace(workspaceName);
                                },
                                function() {
                                    $log.warn("SUIService: Cannot select workspace '" + workspaceName + "'. Previous workspace '" + _currentWorkspace.name + "' NOT unloading.");
                                    that._selectWorkspaceInternal(_currentWorkspace.name);
                                });
                        } else {
                            that.selectWorkspace(workspaceName);
                        }
                    } else {
                        that.selectWorkspace(workspaceName);
                    }
                },

                workspaceRendered: function(workspace) {
                    $rootScope.$evalAsync(function() {
                        //$timeout(function() { //TODO: Is this requird when $evalAsync is used above.
                        workspace.rendered = true;
                        if (workspace.onRenderCb) {
                            workspace.onRenderCb();
                        }
                        workspace.onRenderCb = null;
                        //});
                    });
                },

                setCommandAttribute: function(cmdName, attribute, value) {
                    if (attribute && attribute.trim().length > 0 && cmdName && applicationCommands[cmdName]) {
                        applicationCommands[cmdName][attribute] = value;
                    } else {
                        $log.warn("SUIService : Error setting attribute : '[" + attribute + "] = " + value + "' to command : '" + cmdName);
                    }
                },

                getCommand: function(name, isDebug, requestingScope) {
                    if (isDebug) {
                        console.log("Requesting Command : " + name);
                        console.log(requestingScope);
                    }
                    if (requestingScope && requestingScope.parentViewID) {
                        var parentView = this.getView(requestingScope.parentViewID);
                        if (parentView) {
                            return parentView.getCommand(name);
                        }
                    } else {
                        var cmdObj = applicationCommands[name];
                        if (cmdObj && cmdObj.isCreateNewInstance) {
                            var copiedCmdObj = angular.copy(cmdObj);
                            if (copiedCmdObj.menuItems) {
                                copiedCmdObj.menuItemsMap = this.MenuItemBuilder._getMenuItemsMap(copiedCmdObj.menuItems);
                            }
                            return copiedCmdObj;
                        } else {
                            return cmdObj;
                        }
                    }
                },

                registerWidget: function(name, renderFN) {
                    applicationWidgets[name] = {
                        name: name,
                        type: this.TYPE_WIDGET,
                        renderFN: renderFN,
                    }
                    return applicationWidgets[name];
                },

                registerMenu: function(name, title, icon, iconClass, commandList) {
                    applicationMenus[name] = {
                        name: name,
                        type: this.TYPE_MENU,
                        icon: icon,
                        iconClass: iconClass,
                        title: title,
                        commandList: [],
                        widgetClass: ''
                    }
                    if (commandList) {
                        applicationMenus[name].commandList = commandList;
                    }
                    return applicationMenus[name];
                },

                //TODO:: Add 'addMenuItems(menuName, cmdList, insertOption) API'
                addMenuItem: function(menuName, command, insertOption) {
                    //TODO:: handle insert options
                    if (command && applicationMenus[menuName]) {
                        applicationMenus[menuName].commandList.push(command);
                    } else {
                        $log.warn("SUIService : Error adding command : '" + command + " to menu : '" + menuName);
                    }
                },

                addHeaderItem: function(headerItem, headerPosition, isSecondary) {

                    var headerItemContainer = applicationHeaderItems;
                    if (isSecondary) {
                        headerItemContainer = applicationSecondaryItems;
                    }

                    //TODO:: handle header position options
                    if (headerItem && headerPosition) {
                        if (!headerItemContainer[headerPosition]) {
                            headerItemContainer[headerPosition] = [];
                        }
                        headerItemContainer[headerPosition].push(headerItem);
                    }
                },

                getHeaderItemLocations: function() {
                    return this.HEADER_POSITIONS;
                },

                getHeaderItems: function(headerPosition, reverseOrder, isSecondary) {
                    var headerItemContainer = applicationHeaderItems;
                    if (isSecondary) {
                        headerItemContainer = applicationSecondaryItems;
                    }
                    if (headerItemContainer[headerPosition]) {
                        if (reverseOrder === true) {
                            return headerItemContainer[headerPosition].slice().reverse();
                        } else {
                            return headerItemContainer[headerPosition].slice();
                        }
                    } else {
                        return [];
                    }
                },

                getHeaderItemClass: function(headerItem) {
                    switch (headerItem.type) {
                        case this.TYPE_MENU:
                            return "dropdown";
                    }
                },

                cancelCommandExecution: function(longRunningTaskId) {
                    var cmdExecData = longRunningCommandMap[longRunningTaskId];
                    if (cmdExecData) {
                        cmdExecData.parentViewScope.waitMessage = "Cancelling " + cmdExecData.parentViewScope.waitMessage;
                        cmdExecData.parentViewScope.canCancelAction = false;
                    }
                    this.executeRestCall({
                        method: "GET",
                        URL: "command/sui.cancelLongRunningTask",
                        data: {
                            "longRunningTaskId": longRunningTaskId
                        },
                        isGetFullResponse: false
                    });
                },

                updateCommandExecutionStatus: function(updatedResponse) {
                    if (updatedResponse) {
                        var cmdExecData = longRunningCommandMap[updatedResponse.longRunningTaskId];
                        if (cmdExecData) {
                            if (cmdExecData.parentViewScope) {
                                cmdExecData.parentViewScope.canCancelAction = !updatedResponse.cancelled;
                                cmdExecData.parentViewScope.waitMessage = (updatedResponse.cancelled ? "Cancelled " : "") + updatedResponse.responseMessage;
                                cmdExecData.parentViewScope.progressPercent = updatedResponse.percentCompleted;
                                cmdExecData.parentViewScope.$apply();
                            }
                            if (!updatedResponse.longRunning) {
                                if (updatedResponse.responseStatusCode === this.STATUS_CODE_OK) {
                                    cmdExecData.commandExecDefer.resolve(updatedResponse);
                                } else {
                                    var errorMessage = (updatedResponse.responseMessage ? (" " + updatedResponse.responseMessage) : " " + "An internal server error has occured. Please contact your system administrator. Error Code : " + updatedResponse.responseStatusCode);
                                    cmdExecData.commandExecDefer.reject(errorMessage);
                                }
                                if (cmdExecData.parentViewScope) {
                                    cmdExecData.parentViewScope.longRunningTaskId = null;
                                }
                                delete longRunningCommandMap[updatedResponse.longRunningTaskId];
                            }
                        }
                    }
                },

                /*
      executeContext.event
      executeContext.scope
      executeContext.prompt
      executeContext.dontShowBusy
      executeContext.viewId
*/
                executeCommand: function(name, args, executeContext) {

                    //TODO: Privilege Check

                    var cmdObj = this.getCommand(name);

                    if (name) {
                        cmdObj = this.getCommand(name);
                    } else {
                        $log.warn("SUIService : Unable to execute command '" + name + "'. Command NOT Defined!");
                        return;
                    }

                    if (cmdObj) {
                        if (cmdObj.isCreateNewInstance) {
                            try {
                                cmdObj = this.getCommand(name, false, executeContext.scope);
                            } catch (e) {

                            }
                        }
                    } else {
                        $log.warn("SUIService : Unable to execute command '" + name + "'. Command NOT Defined!");
                        return;
                    }

                    if (cmdObj.enabled) {

                        var that = this;

                        if (!cmdObj || !cmdObj.execute) {
                            return;
                        }

                        var rootScope = angular.element(document).scope();
                        var currentScope = null;

                        var currentEventOrScope = null;

                        var busyDialogPrompt = executeContext ? executeContext.prompt : null;
                        var dontShowBusy = executeContext ? executeContext.dontShowBusy : null;
                        var viewId = executeContext ? executeContext.viewId : null;
                        var immediateParentViewScope_showBusy = false;

                        if (executeContext && executeContext.scope) {
                            currentEventOrScope = executeContext.scope;
                        } else if (executeContext && executeContext.event) {
                            currentEventOrScope = executeContext.event;
                        }

                        if (!currentEventOrScope) {
                            currentEventOrScope = $window.event;
                        }

                        if (currentEventOrScope && (currentEventOrScope.target || currentEventOrScope.srcElement)) {

                            if (currentEventOrScope) {
                                try {
                                    currentScope = angular.element(currentEventOrScope.target || currentEventOrScope.srcElement).scope();
                                } catch (e) {
                                    //TODO:: ignore
                                }
                            }
                        } else {
                            currentScope = currentEventOrScope;
                        }

                        if (!currentScope) {
                            currentScope = rootScope;
                        }

                        var immediateParentViewScope = null;

                        if (!dontShowBusy) {
                            if (viewId) {
                                immediateParentViewScope = this.getView(viewId);
                            } else {
                                if (currentScope && currentScope.parentViewIDs && currentScope.parentViewIDs.length > 0) {
                                    viewId = currentScope.parentViewIDs[currentScope.parentViewIDs.length - 1];
                                    immediateParentViewScope = this.getView(viewId);
                                }
                            }
                            if (!immediateParentViewScope) {
                                immediateParentViewScope = rootScope;
                            }
                            if (immediateParentViewScope) {
                                //if immediate parent scope is executing a long running task
                                // dont proceed with this command execution.
                                // Just return from here.
                                if (immediateParentViewScope.longRunningTaskId) {
                                    return false;
                                }

                                if (busyViewIdMap[viewId]) {
                                    busyViewIdMap[viewId] = busyViewIdMap[viewId] + 1;
                                } else {
                                    busyViewIdMap[viewId] = 1;
                                }
                                immediateParentViewScope.progressPercent = 0;
                                immediateParentViewScope.showProgressPercent = false;
                                immediateParentViewScope.canCancelAction = false;
                                immediateParentViewScope_showBusy = true;

                                if (!busyDialogPrompt) {
                                    busyDialogPrompt = "Executing command : " + (cmdObj.title ? cmdObj.title : cmdObj.name) + " ...";
                                }
                                immediateParentViewScope.waitMessage = busyDialogPrompt;
                            }
                        }

                        var argArray = null;
                        try {
                            if (args) {
                                if (angular.isArray(args)) {
                                    argArray = args;
                                } else {
                                    argArray = [args];
                                }
                            } else {
                                argArray = [];
                            }
                            var currentExecContext = {};
                            if (executeContext) {
                                currentExecContext = executeContext;
                            }
                            currentExecContext.prompt = busyDialogPrompt;
                            currentExecContext.dontShowBusy = dontShowBusy;
                            currentExecContext.viewId = viewId;
                            currentExecContext.currentScope = currentScope;
                            argArray.splice(0, 0, currentExecContext);

                            var postCommandProcDefer = $q.defer();

                            $timeout(function() {
                                if (immediateParentViewScope && !dontShowBusy) {
                                    immediateParentViewScope.showBusy = immediateParentViewScope_showBusy;
                                }
                                $q.when(cmdObj.execute.apply(that, argArray)).then(
                                    function(data) {
                                        if (data && data.longRunning) {
                                            if (immediateParentViewScope && !dontShowBusy) {
                                                immediateParentViewScope.progressPercent = data.percentCompleted;
                                                immediateParentViewScope.showProgressPercent = true;
                                                immediateParentViewScope.canCancelAction = true;
                                                immediateParentViewScope.waitMessage = data.responseMessage;
                                                immediateParentViewScope.longRunningTaskId = data.longRunningTaskId;
                                            }
                                            longRunningCommandMap[data.longRunningTaskId] = {
                                                parentViewScope: immediateParentViewScope,
                                                commandExecDefer: postCommandProcDefer,
                                                currentData: data
                                            }
                                        } else {
                                            if (immediateParentViewScope && !dontShowBusy) {
                                                immediateParentViewScope.progressPercent = 100;
                                            }
                                            postCommandProcDefer.resolve(data);
                                        }
                                    },
                                    function(errors) {
                                        postCommandProcDefer.reject(errors);
                                    }
                                );
                            })

                            $q.when(postCommandProcDefer.promise).then(function(data) {
                                if (immediateParentViewScope && !dontShowBusy) {
                                    if (busyViewIdMap[viewId]) {
                                        busyViewIdMap[viewId] = busyViewIdMap[viewId] - 1;
                                    }
                                    if (busyViewIdMap[viewId] === 0) {
                                        immediateParentViewScope.showBusy = false;
                                        immediateParentViewScope.waitMessage = "Command : " + (cmdObj.title ? cmdObj.title : cmdObj.name) + " executed successfully."
                                        delete busyViewIdMap[viewId];
                                    }
                                }
                                currentScope["command"] = {};
                                currentScope["command"][cmdObj.name] = {
                                    errors: null
                                };
                                if (cmdObj && cmdObj.onSuccessCB) {
                                    cmdObj.onSuccessCB.apply(that, [data].concat(argArray));
                                }
                            }, function(errors) {
                                if (immediateParentViewScope && !dontShowBusy) {
                                    if (busyViewIdMap[viewId]) {
                                        busyViewIdMap[viewId] = busyViewIdMap[viewId] - 1;
                                    }
                                    if (busyViewIdMap[viewId] === 0) {
                                        immediateParentViewScope.showBusy = false;
                                        immediateParentViewScope.waitMessage = "Command : " + (cmdObj.title ? cmdObj.title : cmdObj.name) + " failed."
                                        delete busyViewIdMap[viewId];
                                    }
                                }

                                var errorsToSend = null;
                                if (errors) {
                                    if (SUIService.isArray(errors)) {
                                        errorsToSend = errors;
                                    } else {
                                        errorsToSend = [];
                                        if (!angular.isObject(errors)) {
                                            errorsToSend.push(errors);
                                        } else {
                                            // The only object that will be parsed here should have the following structure
                                            //     {
                                            //         data:{
                                            //             responseMessage: "<error message>"
                                            //         }
                                            //     }
                                            if (errors.data && errors.data.responseMessage) {
                                                errorsToSend.push(errors.data.responseMessage);
                                            } else if (errors.status && errors.statusText) {
                                                errorsToSend.push(errors.status + " : " + errors.statusText);
                                            }
                                        }
                                    }
                                }
                                currentScope["command"] = {};
                                currentScope["command"][cmdObj.name] = {
                                    errors: errorsToSend
                                };
                                if (cmdObj && cmdObj.onErrorCB) {
                                    cmdObj.onErrorCB.apply(that, [errors].concat(argArray));
                                }
                            });
                        } catch (e) {
                            $log.warn("SUIService : Unable to execute command '" + name + "'");
                            $log.warn(e);
                        }
                        return null;
                    } else {
                        if (name && this.getCommand(name)) {
                            $log.warn("SUIService : Unable to execute command '" + name + "'. Command NOT Enabled!");
                        } else {
                            $log.warn("SUIService : Unable to execute command '" + name + "'. Command NOT Defined!");
                        }
                    }
                },

                registerAPI: function(apiName, apiScope, apiFunction) {
                    applicationAPIs[apiName] = {
                        apiScope: apiScope,
                        apiFunction: apiFunction
                    }
                },

                executeAPI: function(apiName, paramsArray) {
                    var apiDef = applicationAPIs[apiName],
                        apiFn;
                    if (apiDef) {
                        apiFn = apiDef.apiFunction || (apiDef.apiScope && apiDef.apiScope[apiName]);
                        if (apiFn && angular.isFunction(apiFn)) {
                            apiFn.apply(apiDef.apiScope, paramsArray);
                        }
                    } else {
                        $log.warn("SUIService : Unable to execute api '" + apiName + "'. API definition NOT Found!");
                    }
                },

                performLogout: function() {
                    this.executeCommand('logout')
                },

                registerView: function(viewScope) {
                    if (viewScope) {
                        var vId = viewScope["UniqueVewID"];
                        if (!vId) {
                            vId = this.getNextViewId();
                            viewScope["UniqueVewID"] = vId;
                        }
                        applicationViews[vId] = viewScope;
                    }
                    return vId;
                },

                getView: function(viewId) {
                    return applicationViews[viewId];
                },

                getNextViewId: function() {
                    var vId = "View_" + _VIEW_ID_COUNTER++;
                    while (applicationViews[vId]) {
                        vId = "View_" + _VIEW_ID_COUNTER++;
                    }
                    return vId;
                },

                getNextID: function(prefix) {
                    if (prefix) {
                        if (idCounterMap[prefix] === undefined) {
                            idCounterMap[prefix] = -1;
                        }
                        idCounterMap[prefix] += 1;
                        return prefix + idCounterMap[prefix];
                    } else {
                        nextIDCounter += 1;
                        return nextIDCounter;
                    }
                },

                resetNextIDCounter: function(prefix) {
                    if (prefix) {
                        if (idCounterMap[prefix]) {
                            idCounterMap[prefix] = -1;
                        }
                    } else {
                        nextIDCounter = -1;
                    }
                },

                resetAllNextIDCounters: function() {
                    resetNextIDCounter();
                    $.each(idCounterMap, function(key, value) {
                        resetNextIDCounter(key);
                    });
                },

                showDialogMessage: function(messageData) {
                    var that = this;
                    var modalInstance = SUIService.getService("$modal").open({
                        templateUrl: "resources/sui/templates/html/genericDialog.html",
                        size: messageData.dialogSize ? messageData.dialogSize : "lg",
                        backdrop: messageData.isModal ? 'static' : null,
                        keyboard: messageData.isModal ? false : true,
                        controller: function($scope, $timeout) {

                            var dialogType = messageData.dialogType;
                            if (!dialogType) {
                                dialogType = that.DIALOG_TYPE_PLAIN;
                            }
                            $scope.dialogTitle = messageData.dialogTitle ? messageData.dialogTitle : "";
                            $scope.dialogTitleIconClass = messageData.dialogTitleIconClass ? messageData.dialogTitleIconClass : null;
                            $scope.dialogTypeClass = messageData.dialogTypeClass ? messageData.dialogTypeClass : "";
                            $scope.dialogMessage = messageData.dialogMessage;
                            $scope.okButtonTitle = messageData.okButtonTitle ? messageData.okButtonTitle : "OK";
                            $scope.cancelButtonTitle = messageData.cancelButtonTitle ? messageData.cancelButtonTitle : "Cancel";

                            $scope.hideHeader = messageData.hideHeader;
                            $scope.hideOkButton = messageData.hideOkButton;
                            $scope.hideCancelButton = messageData.hideCancelButton;
                            $scope.hideFooter = messageData.hideFooter;

                            $scope.handleKeyPress = function($event) {
                                if ($event.keyCode === 13) {
                                    $event.preventDefault();
                                    $event.stopPropagation();
                                    $scope.$close(SUIService.DIALOG_OK)
                                }
                            }

                            $scope.closeDialog = function(param, $event) {
                                $scope.$close(param)
                            }

                            switch (dialogType) {
                                case that.DIALOG_TYPE_INFO:
                                    $scope.hideCancelButton = true;
                                    $scope.dialogTypeClass = "alert-info";
                                    $scope.dialogTitleIconClass = "glyphicon glyphicon-info-sign";
                                    break;

                                case that.DIALOG_TYPE_SUCCESS:
                                    $scope.hideCancelButton = true;
                                    $scope.dialogTypeClass = "alert-success";
                                    $scope.dialogTitleIconClass = "glyphicon glyphicon-ok-sign";
                                    break;

                                case that.DIALOG_TYPE_WARNING:
                                    $scope.hideCancelButton = true;
                                    $scope.dialogTypeClass = "alert-warning";
                                    $scope.dialogTitleIconClass = "glyphicon glyphicon-exclamation-sign";
                                    break;

                                case that.DIALOG_TYPE_ERROR:
                                    $scope.hideCancelButton = true;
                                    $scope.dialogTypeClass = "alert-danger";
                                    $scope.dialogTitleIconClass = "glyphicon glyphicon-remove-sign";
                                    break;

                                case that.DIALOG_TYPE_CUSTOM:
                                    break;

                                case that.DIALOG_TYPE_PLAIN:
                                    $scope.hideCancelButton = true;
                                    break;

                                case that.DIALOG_TYPE_CONFIRM:
                                    $scope.dialogTypeClass = "alert-warning";
                                    $scope.dialogTitleIconClass = "glyphicon glyphicon-question-sign";
                                    break;
                            }
                            $timeout(function() {
                                angular.element("button#genericDialogOkButton").focus();
                            })
                        }
                    });
                    $q.when(modalInstance.result).then(function(result) {
                        if (messageData.callbackFn) {
                            messageData.callbackFn(result);
                        }
                    });
                },

                getObjectProperty: function(dataObject, fieldName, defaultProps) {
                    if (!dataObject) {
                        return null;
                    }
                    var splitFields = fieldName.split('.');
                    var tempObj = dataObject;
                    var retObj = null;
                    var isPropFound = true;
                    for (var sf = 0; sf < splitFields.length; sf++) {
                        if (this.isNull(tempObj[splitFields[sf]])) {
                            isPropFound = false;
                            break;
                        }
                        tempObj = tempObj[splitFields[sf]];
                    }

                    if (isPropFound) {
                        retObj = tempObj;
                    }

                    if (retObj && defaultProps) {
                        for (var defaultlProp in defaultProps) {
                            if (this.isNull(retObj[defaultlProp])) {
                                retObj[defaultlProp] = defaultProps[defaultlProp];
                            }
                        }
                    }
                    return retObj;
                },

                _shouldIncludeField: function(bean) {
                    return function(field) {
                        return SUIService.shouldIncludeField(field, bean);
                    }
                },

                shouldIncludeField: function(field, bean) {
                    var _isFieldOpt = SUIService.isNull(field.isRequired) ? false : !field.isRequired,
                        _fName = field.name || field,
                        _oValue = SUIService.getObjectProperty(bean, _fName);
                    return !_isFieldOpt || !SUIService.isNull(_oValue);
                },

                isNull: function(value) {
                    return value === null || value === undefined;
                },

                generateMockKeyDown: function(elemId) {
                    try {
                        angular.element("#" + elemId).val("e");
                        angular.element("#" + elemId).trigger('input');
                        var e = angular.element.Event("keydown");
                        e.which = 13;
                        angular.element("#" + elemId).trigger(e);
                    } catch (ex) {
                        console.log(ex);
                    }
                },

                fixPopupPosition: function(popupElem) {

                    // popupElem.css({
                    //     left: "auto",
                    //     right: "auto",
                    //     top: "auto",
                    //     bottom: "auto"
                    // })

                    if (popupElem.offset().left < 8) {
                        popupElem.offset({
                            left: 8
                        })
                    }

                    if (popupElem.offset().top < 32) {
                        popupElem.css({
                            top: 32
                        })
                    }

                    if ((popupElem.offset().left + popupElem.outerWidth()) > ($(window).width() - 8)) {
                        popupElem.offset({
                            left: ($(window).width() - 8) - popupElem.outerWidth()
                        })
                    }
                    if ((popupElem.offset().top + popupElem.outerHeight()) > ($(window).height() - 8)) {
                        popupElem.offset({
                            top: ($(window).height() - 8) - popupElem.outerHeight()
                        })
                    }

                    // if ((popupElem.offset().left + popupElem.outerWidth()) > ($(window).width() - 42)) {
                    //     popupElem.offset({
                    //         left: $(window).width() - 42 - popupElem.outerWidth()
                    //     })
                    // }
                    // if ((popupElem.offset().top + popupElem.outerHeight()) > ($(window).height() - 16)) {
                    //     popupElem.offset({
                    //         top: $(window).height() - 16 - popupElem.outerHeight(),
                    //         left: popupElem.offset().left - 42
                    //     })
                    // }
                },

                /* Request Object Attributes
                  method = "GET"/"POST"/"PUT"/"DELETE"
                  URL
                  data = Post / parameterized get data
                  params = URL Params
                  willHandleError = should the backend call handle error and show a message 
                  callback = callback function to call after server response.      
                */
                executeRestCall: function(request) {
                    return SUIServiceHTTP.sendRequest(
                        request.method,
                        request.URL,
                        request.params,
                        request.data,
                        function(responseData, statusCode) {
                            if (request.callback && angular.isFunction(request.callback)) {
                                request.callback(responseData, statusCode);
                            }
                        },
                        request.willHandleError,
                        request.isGetFullResponse,
                        request.metaData,
                        request.config
                    );
                }
            }
        }
    ])
    .service('SUIServiceHTTP', ['$q', '$http',
        function($q, $http) {
            this.sendRequest = function(
                operation,
                url,
                params,
                data,
                callback,
                willHandleErrorMessage,
                isGetFullResponse,
                metaData,
                reqConfig) {

                var that = this;
                var httpDeferred = null;
                var dataPayload = null;
                var dataPayloadField = "payload";

                if (reqConfig && reqConfig.httpDeferred) {
                    httpDeferred = reqConfig.httpDeferred
                } else {
                    httpDeferred = $q.defer();
                }

                if (data) {
                    if (reqConfig && reqConfig.isRemoveDataPayloadField) {
                        dataPayload = data;
                    } else {
                        if (reqConfig && reqConfig.dataPayloadField) {
                            dataPayloadField = reqConfig.dataPayloadField;
                        }
                        dataPayload = {};
                        dataPayload[dataPayloadField] = data;;
                    }
                }

                var updatedOperation = operation;
                if (operation && operation.toUpperCase() === "GET" && data) {
                    updatedOperation = "POST";
                    dataPayload["IS_PARAMETRIZED_GET"] = true;
                }
                $q.when($http({
                    method: updatedOperation,
                    url: url,
                    params: params,
                    data: dataPayload,
                })).then(function(httpResponse) {

                        if (reqConfig && reqConfig.preProcess) {
                            if (!reqConfig.preProcess(httpResponse, httpDeferred, reqConfig)) {
                                return;
                            }
                        }

                        httpResponse.SUI_REQUEST_META_DATA = metaData;
                        //httpResponse is provided by $http angular service
                        //httpResponse.data points to the response data set by the server call.
                        // sUI encapsulates the entire server data inside an object called data.
                        // now to get the actual response to a sui server call, access httpResponse.data.data
                        if (httpResponse.status === SUIService.HTTP_STATUS_CODE_OK) {
                            if (isGetFullResponse) {
                                if (callback) {
                                    callback(httpResponse, metaData);
                                }
                                httpDeferred.resolve(httpResponse);
                            } else {
                                if (httpResponse.data.responseStatusCode === SUIService.STATUS_CODE_OK) {
                                    if (callback) {
                                        callback(httpResponse.data.data, httpResponse.data.responseStatusCode, metaData);
                                    }
                                    httpDeferred.resolve(httpResponse.data);
                                } else {
                                    var errorMessage = (httpResponse.data.responseMessage ? (" " + httpResponse.data.responseMessage) : " " + "An internal server error has occured. Please contact your system administrator. Error Code : " + httpResponse.data.responseStatusCode);
                                    if (!willHandleErrorMessage) {
                                        SUIService.showDialogMessage({
                                            dialogType: SUIService.DIALOG_TYPE_ERROR,
                                            dialogMessage: errorMessage,
                                            dialogTitle: "Error",
                                            isModal: true
                                        });
                                        httpDeferred.reject(httpResponse);
                                    } else {
                                        if (callback) {
                                            callback(errorMessage, httpResponse.data.responseStatusCode);
                                        }
                                        httpDeferred.reject(errorMessage);
                                    }
                                }
                            }
                        } else if (httpResponse.status === SUIService.STATUS_CODE_UNAUTHORIZED) {
                            if (!reqConfig || !reqConfig.isHandleUnAuthorizedError) {
                                SUIService.showDialogMessage({
                                    dialogType: SUIService.DIALOG_TYPE_ERROR,
                                    dialogMessage: "You login session expired. Redirecting to login page!",
                                    dialogTitle: "Access Error",
                                    isModal: true,
                                    callbackFn: function() {
                                        SUIService.getService('$window').location.reload();
                                    }
                                });
                            }
                            httpDeferred.reject(httpResponse);
                        } else { // Internal Server Error like 500 error from the server
                            var errorMessage = "An internal server error has occured. Please contact your system administrator. Error Code : " + httpResponse.status;
                            if (!willHandleErrorMessage) {
                                SUIService.showDialogMessage({
                                    dialogType: SUIService.DIALOG_TYPE_ERROR,
                                    //dialogMessage: (httpResponse.data.responseMessage ? (" " + httpResponse.data.responseMessage) : ""),
                                    dialogMessage: errorMessage,
                                    dialogTitle: "Internal Error",
                                    isModal: true
                                });
                                httpDeferred.reject(httpResponse);
                            }
                            if (!isGetFullResponse) {
                                httpDeferred.reject(errorMessage);
                            } else {
                                httpDeferred.reject(httpResponse);
                            }
                        }
                    },
                    function(httpResponse) {

                        if (reqConfig && reqConfig.preProcess) {
                            if (!reqConfig.preProcess(httpResponse, httpDeferred, reqConfig)) {
                                return;
                            }
                        }

                        if (httpResponse.status === SUIService.STATUS_CODE_UNAUTHORIZED) {
                            if (!reqConfig || !reqConfig.isHandleUnAuthorizedError) {
                                SUIService.showDialogMessage({
                                    dialogType: SUIService.DIALOG_TYPE_ERROR,
                                    dialogMessage: "You login session expired. Redirecting to login page!",
                                    dialogTitle: "Access Error",
                                    isModal: true,
                                    callbackFn: function() {
                                        SUIService.getService('$window').location.reload();
                                    }
                                });
                            }
                            httpDeferred.reject(httpResponse);
                        } else {
                            var errorMessage = "An internal server error has occured. Please contact your system administrator. Error Code : " + httpResponse.status;
                            if (!willHandleErrorMessage) {
                                SUIService.showDialogMessage({
                                    dialogType: SUIService.DIALOG_TYPE_ERROR,
                                    //dialogMessage: (httpResponse.data.responseMessage ? (" " + httpResponse.data.responseMessage) : ""),
                                    dialogMessage: errorMessage,
                                    dialogTitle: "Internal Error",
                                    isModal: true
                                });
                            }
                            if (!isGetFullResponse) {
                                httpDeferred.reject(errorMessage);
                            } else {
                                httpDeferred.reject(httpResponse);
                            }
                        }
                    });
                return httpDeferred.promise;
            }
        }
    ])

.factory("stacktraceService", [function() {
    return ({
        process: function(e, cb) {
            if (cb) {
                cb(e, printStackTrace({
                    e: e
                }));
            }
        }
    });
}])

.provider("$exceptionHandler", [function() {
    this.$get = ['SUIServiceExceptionHandler', function(SUIServiceExceptionHandler) {
        return (SUIServiceExceptionHandler.log);
    }]
}])

.factory("SUIServiceExceptionHandler", ["$log", "$window", "stacktraceService",

        function($log, $window, stacktraceService) {
            var exHandlerCB;
            return {
                log: function(exception, cause) {
                    // Pass off the error to the default error handler
                    // on the AngualrJS logger. This will output the
                    // error to the console (and let the application
                    // keep running normally for the user).
                    $log.error.apply($log, arguments);
                    try {
                        if (exHandlerCB) {
                            stacktraceService.process(exception, exHandlerCB);
                        }
                    } catch (loggingError) {
                        // For Developers - log the log-failure.
                        $log.warn("Error logging failed");
                        $log.log(loggingError);
                    }
                },
                registerExceptionHandler: function(cb) {
                    exHandlerCB = cb;
                    if (exHandlerCB) {
                        window.onerror = function(e) {
                            stacktraceService.process(e, exHandlerCB);
                        }
                    }
                }
            }
        }
    ])
    /* CCI - Cross Cutting Intercepter */
    .provider('SUIServiceCCI', ['$provide',
        function($provide) {

            var events = ["click", "mousemove", "keypress"],
                i,
                internalMsgLogs = [],
                activeIndex = 0,
                currentIndex = 0,
                config = {
                    logging: {
                        enabled: false
                    },
                    cfg: {

                    }
                },
                LEVEL_TRACE = 1,
                LEVEL_DEBUG = 2,
                LEVEL_INFO = 3,
                LEVEL_WARN = 4,
                LEVEL_ERROR = 5,
                LEVEL_FATAL = 6,
                lastEventTime;

            function addLog(logData) {
                if (!config.logging.enabled) {
                    return;
                }
                var d = new Date(),
                    includeLog = true;
                logData.t = d;
                logData.i = (currentIndex + 1);
                if (logData.s !== "SYS") {
                    if (config.logging.cfg) {
                        if (config.logging.cfg.idleNoticationThreshold) {
                            if (lastEventTime) {
                                var idleTime = (logData.t.getTime() - lastEventTime.getTime());
                                if (idleTime > config.logging.cfg.idleNoticationThreshold) {
                                    addLog({
                                        t: d,
                                        e: "IDLE_NOTIFICATION",
                                        l: LEVEL_WARN,
                                        d: {
                                            idleTime: idleTime,
                                            wakeupEvent: logData.e,
                                            wakeupSource: logData.s
                                        },
                                        s: "SYS"
                                    })
                                }
                            }
                        }
                    }
                }
                if (logData.s !== "SYS") {
                    lastEventTime = logData.t;
                }


                if (config.logging.cfg.logsFilterFn) {
                    includeLog = config.logging.cfg.logsFilterFn(logData);
                }

                if (includeLog) {
                    internalMsgLogs.push(logData);
                    currentIndex++;
                    if (config.logging.updateCB) {
                        config.logging.updateCB(logData, (internalMsgLogs.length - activeIndex));
                    }
                    if (config.logging._internalAddLogCB) {
                        config.logging._internalAddLogCB(logData, (internalMsgLogs.length - activeIndex));
                    }
                }
            }

            function fireWindowUnload() {
                addLog({
                    e: "WINDOW_UNLOAD",
                    l: LEVEL_WARN,
                    d: {},
                    s: "UA"
                });
                storeLogs();
            }

            function markReadIndex(index) {
                activeIndex = index;
            }

            function getLogs() {
                return internalMsgLogs.slice(activeIndex);
            }

            function getAllLogs() {
                return internalMsgLogs.slice(0);
            }

            function clearAllLogs() {
                internalMsgLogs.empty();
            }

            function purgeSavedLogs() {
                if (window.localStorage) {
                    window.localStorage.setItem("APP_LOGS", "");
                }
            }

            function storeLogs() {
                if (config.logging.cfg) {
                    var filterFn = config.logging.cfg.logsFilterFn || function(v) {
                        if ((v.s !== "UA") || (v.l > LEVEL_INFO)) {
                            return true;
                        } else {
                            return false;
                        }
                    }
                    var storedLogs = internalMsgLogs.slice(activeIndex).filter(filterFn);
                    if (window.localStorage) {
                        window.localStorage.setItem("APP_LOGS", JSON.stringify(storedLogs));
                    }
                }
            }

            function getStoredLogs() {
                var retLogs = [],
                    logsStr;
                try {
                    if (window.localStorage) {
                        logsStr = window.localStorage.getItem("APP_LOGS");
                        if (logsStr) {
                            retLogs = JSON.parse(logsStr);
                        }
                    }
                } catch (e) {
                    //TODO: ignore
                }
                return retLogs;
            }

            $(document).ready(function() {
                $(window).blur(function() {
                    addLog({
                        e: "WINDOW_BLUR",
                        l: LEVEL_INFO,
                        d: {},
                        s: "UA"
                    });
                });

                $(window).focus(function() {
                    addLog({
                        e: "WINDOW_FOCUS",
                        l: LEVEL_INFO,
                        d: {},
                        s: "UA"
                    });
                });

                $(window).unload(function() {
                    fireWindowUnload();
                });

                for (i = 0; i < events.length; i++) {
                    $(document).on(events[i], (function(eventName) {
                        return function(e) {
                            addLog({
                                e: eventName,
                                l: LEVEL_INFO,
                                d: {
                                    context: $(e.target).getSelector()[0],
                                    //data: e
                                },
                                s: "UA"
                            });
                        }
                    }(events[i])));
                }
            });

            this.$get = ["$rootScope", "$q", "$timeout", function($rootScope, $q, $timeout) {

                var saveInProgress = false,
                    saveLogTimer;

                function saveLogs() {
                    var oldLogs,
                        allLogsToSave,
                        currIndex,
                        logMsgs,
                        logsWithIndex;

                    if (config.logging.enabled && config.logging.cfg.saveLogs) {

                        oldLogs = getStoredLogs();
                        allLogsToSave = [];
                        currIndex = currentIndex
                        logMsgs = getLogs();

                        saveInProgress = true;

                        if (oldLogs && oldLogs.length) {
                            allLogsToSave = oldLogs;
                        }

                        if (logMsgs && logMsgs.length) {
                            allLogsToSave = allLogsToSave.concat(logMsgs)
                        }

                        if (allLogsToSave.length > 0 && config.logging.cfg.httpService) {
                            $q.when(config.logging.cfg.httpService.sendRequest(
                                config.logging.cfg.logSaveMethod || "POST",
                                config.logging.cfg.logSaveURL,
                                null,
                                allLogsToSave,
                                true
                            )).then(function(response) {
                                purgeSavedLogs();
                                markReadIndex(currIndex);
                                saveInProgress = false;
                                startSaveLogTimer();
                            }, function(response) {
                                if (response.status === 204) {
                                    purgeSavedLogs();
                                    markReadIndex(currIndex);
                                }
                                saveInProgress = false;
                                startSaveLogTimer();
                            })
                        } else {
                            saveInProgress = false;
                            startSaveLogTimer();
                        }
                    }
                }

                function addLogCB(logRec, activeCount) {
                    if (activeCount === config.logging.cfg.logSaveTriggerCount || (logRec.l === LEVEL_FATAL)) {
                        if (!saveInProgress) {
                            saveLogs();
                        }
                    }
                }

                function startSaveLogTimer() {
                    if (saveLogTimer) {
                        $timeout.cancel(saveLogTimer);
                    }
                    if (config.logging.cfg.logSaveTriggerInterval) {
                        saveLogTimer = $timeout(function() {
                            if (!saveInProgress) {
                                saveLogs();
                            }
                        }, config.logging.cfg.logSaveTriggerInterval);
                    }
                }

                config.logging._internalAddLogCB = addLogCB;

                return {
                    logging: {
                        LEVEL_TRACE: LEVEL_TRACE,
                        LEVEL_DEBUG: LEVEL_DEBUG,
                        LEVEL_INFO: LEVEL_INFO,
                        LEVEL_WARN: LEVEL_WARN,
                        LEVEL_ERROR: LEVEL_ERROR,
                        LEVEL_FATAL: LEVEL_FATAL,
                        enableLogging: function() {
                            config.logging.enabled = true;
                        },
                        disableLogging: function() {
                            config.logging.enabled = false;
                        },
                        isLoggingEnabled: function() {
                            return config.logging.enabled === true;
                        },
                        getAllLogs: function() {
                            return getAllLogs();
                        },
                        getLogs: function() {
                            return getLogs();
                        },
                        markReadIndex: function(index) {
                            markReadIndex(index);
                        },
                        getCurrentIndex: function() {
                            return currentIndex;
                        },
                        getStoredLogs: function() {
                            return getStoredLogs();
                        },
                        purgeSavedLogs: function() {
                            purgeSavedLogs();
                        },
                        registerLogConfig: function(cfg) {
                            config.logging.updateCB = cfg ? cfg.updateCB : null;
                            config.logging.cfg = cfg || {};
                            //Save any old logs that are present in the local storage and not saved from the previous session
                            saveLogs();
                            startSaveLogTimer();
                        },
                        log: function(eventName, level, context, data, source) {
                            addLog({
                                e: eventName,
                                l: level,
                                d: {
                                    context: context,
                                    data: data
                                },
                                s: source || "APP"
                            });
                        }
                    }
                }
            }]
        }
    ]);
