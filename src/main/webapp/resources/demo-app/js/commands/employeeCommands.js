'use strict';

define({
    reloadEmployeeList: {
        name: "reloadEmployeeList",
        iconClass: "glyphicon glyphicon-refresh",
        title: "Refresh",
        execute: function(executeContext) {
            var $callerScope = executeContext.currentScope;
            $callerScope.reloadEmployeeList(arguments);
        }
    },

    newEmployee: {
        name: "newEmployee",
        iconClass: "glyphicon glyphicon-file",
        title: "New",
        execute: function(executeContext, parentEmployee) {
            var $callerScope = executeContext.currentScope;
            var currentViewId = executeContext.viewId;
            var employee = {};
            employee.id = -1;
            if (parentEmployee) {
                employee.managerId = parentEmployee.id;
            }
            $callerScope.showEditEmployee(employee, true, parentEmployee, currentViewId);
        }
    },

    editEmployee: {
        name: "editEmployee",
        iconClass: "glyphicon glyphicon-pencil",
        title: "Edit",
        execute: function(executeContext, employee) {
            var $callerScope = executeContext.currentScope;
            var currentViewId = executeContext.viewId;
            $callerScope.showEditEmployee(employee, false, null, currentViewId);
        }
    },

    deleteEmployee: {
        name: "deleteEmployee",
        iconClass: "glyphicon glyphicon-trash",
        title: "Delete",
        enabled: true,
        execute: function(executeContext, employee) {
            var $callerScope = executeContext.currentScope;
            $callerScope.deleteEmployee(employee);
        }
    },

    makePermanentEmployee: {
        name: "makePermanentEmployee",
        iconClass: "glyphicon glyphicon-random",
        title: "Make Permanent",
        execute: function(executeContext, employee) {
            var $callerScope = executeContext.currentScope;
            var callback = $callerScope.openEmployee;
            executeContext.prompt = "Making employee permanent : " + employee.name + "...";
            return SUIService.executeCommand('updateEmployeeInternal', [{
                    dataMap: {
                        id: employee.id,
                        state: 'permanent'
                    }
                },
                employee,
                callback
            ], executeContext);
        }
    },

    updateEmployeeInternal: {
        name: 'updateEmployeeInternal',
        execute: function(executeContext, criteria, employee, updateCallback) {
            var updateData = null;
            if (criteria) {
                updateData = criteria;
            } else {
                updateData = {
                    dataMap: {
                        id: employee.id,
                        employee: employee
                    }
                }
            }
            return SUIService.executeRestCall({
                method: "PUT",
                URL: "rest/employee",
                data: updateData,
                willHandleError: executeContext.willHandleError,
                callback: function(data, status) {
                    if (updateCallback) {
                        updateCallback(employee, data, status);
                    }
                },
                isGetFullResponse: false
            });
        }
    },

    deleteEmployeeInternal: {
        name: 'deleteEmployeeInternal',
        execute: function(executeContext, criteria, employee, deleteCallback) {
            var updateData = {};
            updateData.criteria = criteria;
            updateData.id = [];
            if (employee) {
                if (SUIService.isArray(employee)) {
                    for (var cc = 0; cc < employee.length; cc++) {
                        updateData.id.push(employee[cc].id);
                    }
                } else {
                    updateData.id.push(employee.id);
                }
            }
            return SUIService.executeRestCall({
                method: "DELETE",
                URL: "rest/employee",
                data: updateData,
                willHandleError: executeContext.willHandleError,
                callback: function(data) {
                    if (deleteCallback) {
                        deleteCallback(employee, data, status);
                    }
                },
                isGetFullResponse: false
            });
        }
    },

    fetchEmployees: {
        name: "fetchEmployees",
        execute: function(executeContext, fetchCriteria, fetchCallback) {
            return SUIService.executeRestCall({
                method: "GET",
                URL: "rest/employee",
                data: fetchCriteria ? fetchCriteria : {
                    fields: ["id", "name", "type", "privateField"]
                },
                willHandleError: executeContext.willHandleError,
                callback: fetchCallback,
                isGetFullResponse: false
            });
        }
    }
});
