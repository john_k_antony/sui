var SUIApplication = function(SUIService) {
    'use strict';

    this.getApplicationContext = function() {
        var that = this;
        return {
            appName: "SUI_DEMO",
            title: "sUI Demo",
            loginView: 'resources/demo-app/templates/html/login.html',
            logoutView: 'resources/demo-app/templates/html/login.html',
            leftMainView: null,
            appLogo: 'resources/demo-app/images/sUI.png',
            showHeader: true,
            showSecondaryHeader: false,
            showLeftMain: false,
            showFooter: false,
            initialContext: {},
            isUsingCustomBackend: false,
            workspaceSwitcherType: 'tabs',
            applicationStartupServerCommand: 'userProfileData',
            applicationControllerResources: [
                'resources/demo-app/js/controllers/chatController',
                'resources/demo-app/js/controllers/lrController',
                'resources/demo-app/js/controllers/employeeControllers',
                'resources/demo-app/js/controllers/resourcesControllers'
            ],
            applicationCommandResources: [
                'resources/demo-app/js/commands/aboutCommands',
                'resources/demo-app/js/commands/authCommands',
                'resources/demo-app/js/commands/lrCommands',
                'resources/demo-app/js/commands/chatCommands',
                'resources/demo-app/js/commands/employeeCommands'
            ],
            bootstrapApplication: function() {
                SUIService.addMenuItem(SUIService.USER_ACTIONS_MENU_ITEM, 'logout');

                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, 'help');
                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, SUIService.MENU_SEPARATOR);
                SUIService.addMenuItem(SUIService.ABOUT_APPLICATION_MENU_ITEM, 'about');
            },
            getWorkspaces: function() {
                return getWorkspaces();
            },
            getBootstrapCommands: function() {
                return getBootstrapCommands();
            }
        }
    }

    var getWorkspaces = function() {
        var workspaces = [];

        var homeWS = {
            name: 'home',
            title: 'Home',
            iconClass: "glyphicon glyphicon-home",
            contentsURL: 'resources/demo-app/templates/html/workspaces/HomeWS.html',
            isHome: true
        };

        var employeeWS = {
            name: 'employee',
            title: 'Employees',
            contentsURL: 'resources/demo-app/templates/html/workspaces/EmployeeWS.html'
        };

        var resourceWS = {
            name: 'resource',
            title: 'Resources',
            contentsURL: 'resources/demo-app/templates/html/workspaces/ResourcesWS.html'
        };        

        var lrWS = {
            name: 'lr',
            title: 'Long Running Opertion Demo',
            contentsURL: 'resources/demo-app/templates/html/workspaces/longRunningWS.html'
        };        

        var chatWS = {
            name: 'chat',
            title: 'Chat Demo',
            contentsURL: 'resources/demo-app/templates/html/workspaces/chatWS.html'
        };        

        workspaces.push(homeWS);
        if (SUIService) {
            if (SUIService.getUserProfileData().isSuperUser) {
                workspaces.push(employeeWS);
            }
        }
        workspaces.push(resourceWS);
        workspaces.push(lrWS);
        workspaces.push(chatWS);
        return workspaces;
    }

    var getBootstrapCommands = function() {
        var bootstrapCommands = [];
        bootstrapCommands.push({
            name: "login",
            title: "Login",
            iconClass: "glyphicon glyphicon-log-in",
            execute: function() {
                var $callerScope = arguments[0].currentScope;

                return SUIService.executeRestCall({
                    method: "POST",
                    URL: "doLogin",
                    data: $callerScope ? $callerScope.userPrincipal : null,
                    willHandleError: true,
                    isGetFullResponse: false
                });
            },
            onSuccessCB: function() {
                window.location.reload();
            }
        });
        return bootstrapCommands;
    }
}
