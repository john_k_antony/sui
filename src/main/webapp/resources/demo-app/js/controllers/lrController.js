'use strict';

define({

    LongRunningWorkspaceCtrl: function($scope, SUIService) {
        $scope.lrCommands = ["startLongRunningOp"];

        $scope.selectEmplyee = function(id) {
            SUIService.selectWorkspace("employee", function() {
                SUIService.executeAPI("openEmployeeWithId", [id])
            });
        }
    },

    JSTreeDemoController: function($scope, SUIService) {

        //This is the data structure that holds the tree data.
        //Node that the root node is added by default and it should have the special id "#"
        //This is a requirement from the jsTree control that is being used to render the tree.
        //Please look at the URL : http://www.jstree.com/ for more configuration option for the tree        
        var demoTreeData = [{
            id: "#"
        }]

        $scope.selectedTreeItems = "No Selection.";

        $scope.demoTree = {
            treeData: demoTreeData,
            treeConfig: {
                core: {
                    "multiple": false,
                    "themes": {
                        "icons": false
                    }
                },
                version: 1
            }
        };

        $scope.onTreeContainerInit = function() {
            $scope.demoTree.treeData = function(node, cb) {
                var that = this;
                if (!node) {
                    if (cb) {
                        cb.call(this, []);
                    } else {
                        return demoTreeData;
                    }
                } else {
                    $scope.reloadDemoTree(node, function(children) {
                        cb.call(that, children);
                    });
                }
            };
            $scope.demoTree.treeConfig.version++;
        }

        $scope.initCB = function() {

        }

        $scope.changedCB = function(e, data) {
            var i, j, r = [];
            for (i = 0, j = data.selected.length; i < j; i++) {
                r.push(data.instance.get_node(data.selected[i]).text);
            }
            $scope.selectedTreeItems = r.join(', ');
            //It is important to call $scope.$apply() here to update the $scope.selectedTreeItems and do a UI redraw.
            //This callback was called outside of angular's "watch" radar and we need to do explicitly call $apply()
            $scope.$apply();
        }

        $scope.createNodeCB = function(node) {}

        $scope.loadNodeCB = function(e, node, status) {
            if ($scope.onLoadCallBack) {
                $scope.onLoadCallBack();
                $scope.onLoadCallBack = null;
            }
        }

        $scope.reloadDemoTree = function(parentNode, cb) {

            if ($scope.parentView) {
                $scope.parentView.waitMessage = "Loading...";
                $scope.parentView.showBusy = false;
            }

            var allRESTPromises = [];
            var that = this;

            var reqParam = "root"; // TODO: Change this value to what ever the backend expects to get the top level tree items or immediate children of root

            if (parentNode.id !== "#") {
                reqParam = parentNode.id;
            }

            //TODO: The following code is for demostration. 
            //In real usecase, uncomment the code below starting at "Tree Data from Backend"

            var retData = [];
            for (var i = 0; i < 3; i++) {
                retData.push({
                    text: "Child " + i + " of " + (parentNode.text || "Root"),
                    children: true,
                    parentId: parentNode.id
                });
            }

            cb(retData)

            // End of demo code

            /******** Tree Data from Backend 

                var treeFetchURL = ""; // TODO: construct the URL to get the children of the current parentNode.

                allRESTPromises.push(
                    http.sendRequest(
                        "GET",
                        treeFetchURL,
                        null,
                        null,
                        null,
                        true,
                        true, {
                            parentId: parentNode.id
                        }
                    )
                );

                q.all(allRESTPromises).then(
                    function(data) {
                        var retData = data;
                        if (retData.SUI_REQUEST_META_DATA.parentId === "#") {
                            $scope.reformatDataAndAdd([$scope.ontologyRoot], parentNode);
                            $scope.onLoadCallBack = function() {
                                $scope.ontologyTree.treeInstance.jstree(true).open_node(parentNode.id);
                            }
                        } else {
                            $scope.reformatDataAndAdd(retData.data, parentNode);
                        }
                        if ($scope.parentView) {
                            $scope.parentView.waitMessage = "";
                            $scope.parentView.showBusy = false;
                        }
                        if (cb) {
                            cb(retData.data);
                        }
                    },
                    function(errorData) {
                        if ($scope.parentView) {
                            $scope.parentView.waitMessage = "";
                            $scope.parentView.showBusy = false;
                        }
                    }
                );
            */
        }

        //This is a utility function that will set the "children" property of the data to true / false based 
        // on whethere the current node has children. If the children property is not set to true, 
        // you will not get the '+' sign to expand the node.
        $scope.reformatDataAndAdd = function(nodes, parentNode) {
            if (parentNode) {
                angular.forEach(nodes, function(v, k) {
                    v.text = v.name;
                    //TODO. Set the v.children property to a boolesn flag that says whether the current node has children or not. 
                    //Use a property from the backend data / logic that will return a boolean
                    v.children = !v.leaf;
                })
            } else {

            }
        }
    }
});
