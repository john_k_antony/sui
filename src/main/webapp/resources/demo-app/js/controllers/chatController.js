'use strict';

define({

    ChatWorkspaceCtrl: function($scope, SUIService) {
        var messagingSvc = SUIService.getService("SUIServiceMessaging");

        messagingSvc.subscribe($scope, "CHAT");

        $scope.chatCommands = [];
        $scope.chat = {
            message: "",
            targetUser: "",
            incomingMessages: ""
        }

        $scope.isSendDisabled = function() {
            return (!$scope.chat.targetUser || !$scope.chat.targetUser.trim().length === 0) ||
                (!$scope.chat.message || !$scope.chat.message.trim().length === 0);
        }

        $scope.sendMessage = function() {
            SUIService.executeRestCall({
                method: "POST",
                URL: "command/employee.sendMessageCmd",
                data: {
                    "targetUser": $scope.chat.targetUser,
                    "message": $scope.chat.message,
                },
                isGetFullResponse: false,
                callback: function(responseData, statusCode) {
                    $scope.chat.message = "";
                }
            });
        }

        $scope.onMessage = function(message) {
            $scope.chat.incomingMessages += message.messageData;
            $scope.chat.incomingMessages += "\n";
            $scope.$apply();
        }
    }
});
