'use strict';

define({

    SubbordinatesListViewCtrl: function($scope, SUIService) {

        $scope.getSubbordinatesGridOptions = function() {
            return {
                plugins: [new ngGridFlexibleHeightPlugin(), SUIService.getService("SUIUtils").ngGridPlugin.autoRedrawOnDataChange],
                data: 'subbordinateList',
                multiSelect: false,
                columnDefs: [{
                    field: 'id',
                    displayName: $scope.empService.formatPropertyName('id'),
                    width: '10%',
                    cellTemplate: '<div class="ngCellText">{{row.getProperty(col.field)}}</div>'
                }, {
                    field: 'name',
                    displayName: $scope.empService.formatPropertyName('name'),
                    width: '50%',
                    cellTemplate: '<div class="ngCellText"><a ng-click="openEmployee(row.entity);">{{row.getProperty(col.field)}}</a></div>'
                }, {
                    field: 'state',
                    displayName: $scope.empService.formatPropertyName('state'),
                    width: '20%',
                    cellTemplate: '<div class="ngCellText">{{empService.getEmployeeStateString(row.getProperty(col.field))}}</div>'
                }, {
                    field: 'type',
                    displayName: $scope.empService.formatPropertyName('type'),
                    width: '20%',
                    cellTemplate: '<div class="ngCellText">{{empService.getEmployeeTypeString(row.getProperty(col.field))}}</div>'
                }]
            };
        }
    },

    SubbordinatesChartViewCtrl: function($scope, SUIService) {

        $scope.employeeSubbordinateChartConfig = {
            yAxisTickFormat: ".0f",
            getMappedChartData: function(originalData) {

                var that = this;
                var groupFields = [];
                var mappedData = [];
                var tempDataMap = {};

                angular.forEach($scope.subbordinateList, function(v1, k1) {
                    var groupName = v1["type"];
                    if (groupName) {
                        groupFields.addIfNotPresent({
                            name: groupName,
                            title: groupName
                        }, function(o1, o2) {
                            if (o1 && o2) {
                                return o1.name === o2.name;
                            } else {
                                return false;
                            }
                        });
                        if (!tempDataMap[groupName]) {
                            tempDataMap[groupName] = 0;
                        }
                        tempDataMap[groupName]++;
                    }
                });

                var ctr = 0;
                angular.forEach(tempDataMap, function(value, key) {
                    mappedData.push({
                        name: key,
                        title: $scope.empService.getEmployeeTypeString(key),
                        data: [{
                            name: "count",
                            color: SUIService.getService("ChartFactory").getChartColor(ctr),
                            count: value
                        }]
                    });
                    ctr++;
                });
                return mappedData;
            },
            getSubGroupDataValue: function(dataItem, idx) {
                try {
                    return dataItem.count;
                } catch (e) {
                    return 0;
                }
            },
            getSubGroupTitle: function(dataItem, idx) {
                return $scope.empService.getEmployeeTypeString(dataItem.name);
            },
            getColor: function(data, index) {
                if (!SUIService.isNull(data.x)) {
                    // for pie chart
                    return SUIService.getService("ChartFactory").getChartColor(data.x)
                } else {
                    // for bar chart
                    return SUIService.getService("ChartFactory").getChartColor(index)
                }
            }
        };

        $scope.employeeSubbordinateChartConfigPie =
            Object.create($scope.employeeSubbordinateChartConfig);

        $scope.employeeSubbordinateChartConfigPie.chartMargin = {
            top: -45,
            right: 45,
            bottom: -45,
            left: 10
        }
    },

    EmployeeTabCtrl: function($scope, SUIService) {

        $scope.subbordinateList = [];
        $scope.empService.getSubbordinates($scope.employee.id).then(function(data) {
            $scope.subbordinateList = data;
        });
    },

    EmployeeWorkspaceCtrl: function($scope, SUIService) {

        var $q = SUIService.getService("$q");
        var initDefer = $q.defer();

        $scope.$on(SUIService.WORKSPACE_SWITCH, function(event, wsData) {
            if (wsData.new === 'employee') {
                if (!$scope.employeeListViewLoaded) {
                    $scope.onEmployeeListViewLoaded();
                }
            }
        });

        $scope.empService = SUIService.getService("EmployeeService");

        $scope.formatPropertyName = function(property) {
            return $scope.empService.formatPropertyName(property);
        }

        $scope.formatPropertyValue = function(property, bean, value) {
            return $scope.empService.formatPropertyValue(property, bean, value);
        }

        $scope.employeeList = [];
        $scope.gridOptions = {
            plugins: [new ngGridFlexibleHeightPlugin(), SUIService.getService("SUIUtils").ngGridPlugin.autoRedrawOnDataChange],
            data: 'employeeList',
            multiSelect: false,
            columnDefs: [{
                field: 'id',
                displayName: $scope.formatPropertyName('id'),
                width: '10%'
            }, {
                field: 'name',
                displayName: $scope.formatPropertyName('name'),
                width: '70%',
                cellTemplate: '<div class="ngCellText"><a ng-click="openEmployee(row.entity);">{{row.getProperty(col.field)}}</a></div>'
                    //cellTemplate: '<div class="ngCellText"><a ng-click="grid.appScope.openEmployee(row.entity);">{{grid.getCellValue(row, col)}}</a></div>'
            }, {
                field: 'type',
                displayName: $scope.formatPropertyName('type'),
                width: '20%',
                cellTemplate: '<div class="ngCellText">{{empService.getEmployeeTypeString(row.getProperty(col.field))}}</div>'
                    //cellTemplate: '<div class="ngCellText">{{grid.appScope.empService.getEmployeeTypeString(grid.getCellValue(row, col))}}</div>'
            }]
        };

        $scope.openEmployees = [];

        $scope.openEmployeeWithId = function(id) {
            $q.when(initDefer.promise).then(function() {
                $scope.openEmployeeWithIdInternal(id);
            });
        }

        SUIService.registerAPI("openEmployeeWithId", $scope);

        $scope.openEmployeeWithIdInternal = function(employeeId) {
            var employee;
            angular.forEach($scope.employeeList, function(v, k) {
                if ("" + v.id === "" + employeeId) {
                    employee = v;
                }
            });

            if (employee) {
                $scope.openEmployee(employee);
            } else {
                SUIService.showDialogMessage({
                    isModal: true,
                    size: "md",
                    hideCancelButton: true,
                    dialogMessage: "Cannot find employee with id : " + employeeId,
                    dialogType: SUIService.DIALOG_TYPE_ERROR,
                    dialogTitle: "Cannot Open Employee"
                });
            }
        }

        $scope.openEmployee = function(employee) {
            var executeContext = {
                scope: $scope,
                viewId: $scope.employeeListViewId,
                prompt: "Loading employee - " + employee.name + "..."
            }
            SUIService.executeCommand('fetchEmployees', [{
                    id: employee.id,
                    fields: ["id", "name", "dob", "sex", "type", "state", "managerId", "managerName", "skills"]
                },
                $scope.setEmployee
            ], executeContext);
        };

        $scope.setEmployee = function(data) {
            if (data && data.length == 1 && data[0]) {
                var employee = data[0];

                var employeeFound = false;
                for (var emIndex = 0; emIndex < $scope.openEmployees.length; emIndex++) {
                    if ($scope.openEmployees[emIndex].id === employee.id) {
                        $scope.openEmployees[emIndex] = angular.copy(employee);
                        $scope.openEmployees[emIndex].active = true;
                        employeeFound = true;
                    }
                }
                if (!employeeFound) {
                    $scope.openEmployees.push(angular.copy(employee));
                    $scope.openEmployees[$scope.openEmployees.length - 1].active = true;
                }
                $scope.employeeListTabActive = false;

                console.log(employee);
            } else {
                //TODO::
            }
        };

        $scope.closeEmployee = function(employee) {
            for (var emIndex = 0; emIndex < $scope.openEmployees.length; emIndex++) {
                if ($scope.openEmployees[emIndex].id === employee.id) {
                    $scope.openEmployees.splice(emIndex, 1);
                }
            }
        };

        $scope.activateEmployeeListTab = function() {
            $scope.employeeListTabActive = true;
        };

        $scope.employeeListTabSelected = function() {
            $scope.employeeListTabActive = true;
            $scope.reloadEmployeeList();
            SUIService.getService("$rootScope").$broadcast(SUIService.VIEW_STATE_CHANGE);
        };

        $scope.employeeTabSelected = function() {
            SUIService.getService("$rootScope").$broadcast(SUIService.VIEW_STATE_CHANGE);
        };

        $scope.reloadEmployeeList = function() {
            if ($scope.isLoadEmployeeListInProgress) {
                return;
            }
            if (!$scope.employeeListViewLoaded || !$scope.employeeListTabActive) {
                return;
            }
            $scope.isLoadEmployeeListInProgress = true;
            var executeContext = {
                scope: $scope,
                viewId: $scope.employeeListViewId,
                prompt: "Loading employees..."
            }
            SUIService.executeCommand('fetchEmployees', [null, $scope.setEmployeeList], executeContext);
        };

        $scope.setEmployeeList = function(data) {
            $scope.isLoadEmployeeListInProgress = false;
            $scope.employeeList = data;
            initDefer.resolve();
        };

        $scope.employeeViewLoaded = function(employee) {};

        $scope.onEmployeeListViewLoaded = function() {
            $scope.employeeListViewLoaded = true;
            $scope.activateEmployeeListTab();
            $scope.reloadEmployeeList();
        };

        $scope.employeeListViewId = "employeeListView";

        $scope.employeeListViewCommands = ["reloadEmployeeList", "newEmployee"];

        $scope.employeeViewCommands = ["makePermanentEmployee", "newEmployee", "editEmployee", "deleteEmployee"];

        $scope.isCommandEnabled = function(cmd, contextObject) {
            var isEnabled = false;
            switch (cmd) {
                case "newEmployee":
                    if (contextObject && contextObject.type === "DEVELOPER") {
                        isEnabled = false;
                    } else {
                        isEnabled = true;
                    }
                    break;
                case "makePermanentEmployee":
                    if (contextObject && contextObject.state === "PROBATION") {
                        isEnabled = true;
                    } else {
                        isEnabled = false;
                    }
                    break;
                case "deleteEmployee":
                    if (contextObject && contextObject.type === "CEO") {
                        isEnabled = false;
                    } else {
                        isEnabled = true;
                    }
                    break;
                default:
                    isEnabled = true;
            }
            return isEnabled;
        }

        $scope.showEditEmployee = function(employee, isNew, parentEmployee, viewId) {
            var modalInstance = SUIService.getService("$modal").open({
                templateUrl: 'resources/demo-app/templates/html/dialogs/editEmployee.html',
                controller: EditEmployeeModalInstanceCtrl,
                size: 'lg',
                backdrop: 'static',
                scope: $scope,
                resolve: {
                    employee: function() {
                        return employee;
                    }
                }
            });

            modalInstance.result.then(function() {
                if (isNew) {
                    if (viewId === $scope.employeeListViewId) {
                        $scope.reloadEmployeeList();
                    } else {
                        if (parentEmployee) {
                            $scope.openEmployee(parentEmployee);
                        } else {
                            $scope.openEmployee(employee);
                        }
                    }
                } else {
                    $scope.openEmployee(employee);
                }
            }, function() {
                // TODO: close handler
            });
        };

        var EditEmployeeModalInstanceCtrl = function($scope, $modalInstance, employee) {

            $scope.masterEmployee = angular.copy(employee);
            $scope.employee = angular.copy($scope.masterEmployee);
            $scope.dateFormat = "longDate";

            $scope.allManagers = [];

            var qSvc = SUIService.getService("$q");

            qSvc.when($scope.empService.getAllManagers()).then(
                function(data) {
                    $scope.allManagers = data;
                }
            );

            try {
                if ($scope.employee.dob && !angular.isDate($scope.employee.dob) && $scope.employee.dob > 0) {
                    $scope.employee.dob = new Date($scope.employee.dob);
                }
            } catch (e) {

            }

            $scope.dateOptions = {
                startingDay: 1,
                showWeeks: false
            };

            $scope.openCalendar = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $scope.calendarOpened = true;
            };

            $scope.ok = function() {
                var saveObj = angular.copy($scope.employee);
                if (angular.isDate(saveObj.dob)) {
                    saveObj.dob = saveObj.dob.getTime();
                }
                var executeContext = {
                    scope: $scope,
                    viewId: 'editEmployeeView' + $scope.employee.id,
                    prompt: "Saving employee " + $scope.employee.name + "...",
                    willHandleError: true
                }
                SUIService.executeCommand('updateEmployeeInternal', [
                    null, saveObj,
                    function(updatedEmployee, responseData, responseStatus) {
                        if (responseStatus === SUIService.STATUS_CODE_OK) {
                            $modalInstance.close();
                        }
                    }
                ], executeContext);
            }

            $scope.cancel = function() {
                $modalInstance.dismiss('cancel');
            };

            $scope.reset = function() {
                $scope.employee = angular.copy($scope.masterEmployee);
            };

            $scope.dialogAPI = {};

            $scope.editEmployeeExtraButtons = [
                /*            
                                {
                                    title: "Custom Validate",
                                    click: function() {
                                        if($scope.dialogAPI.addCustomMessage) {
                                            $scope.dialogAPI.setIsCustomValid(false);
                                            $scope.dialogAPI.addCustomMessage({
                                                id: "VALIDATE_DONE",
                                                type: "info",
                                                message: "Done Validate",
                                                icon: "glyphicon glyphicon-info-sign"
                                            });    
                                        }
                                    }                    
                                },
                                {
                                    title: "Custom OK",
                                    click: function() {
                                        if($scope.dialogAPI.addCustomMessage) {
                                            $scope.dialogAPI.removeCustomMessage("VALIDATE_DONE")
                                            $scope.dialogAPI.setIsCustomValid(true);
                                            $scope.dialogAPI.addCustomMessage({
                                                id: "OK_DONE",
                                                type: "danger",
                                                message: "Cannot save!!",
                                                icon: "glyphicon glyphicon-info-sign"
                                            });    
                                        }                        
                                    }
                                },
                                {
                                    title: "Custom Reset",
                                    click: function() {
                                        $scope.reset();
                                    },
                                    class: "btn btn-primary pull-left"
                                }
                */
            ]
        };

        $scope.deleteEmployee = function(employee) {
            SUIService.showDialogMessage({
                isModal: true,
                dialogTitle: "Delete Confirmation",
                dialogMessage: "Are you sure you want to delete '" + employee.name + "' ?",
                dialogType: SUIService.DIALOG_TYPE_CONFIRM,
                callbackFn: function(dialogResult) {
                    if (dialogResult === SUIService.DIALOG_OK) {
                        var executeContext = {
                            scope: $scope,
                            viewId: 'editEmployeeView' + employee.id,
                            prompt: "Deleting employee " + employee.name + "..."
                        }
                        SUIService.executeCommand('deleteEmployeeInternal', [
                            null, employee,
                            function() {
                                $scope.closeEmployee(employee);
                            }
                        ], executeContext);
                    }
                }
            });
        }
    }
});
