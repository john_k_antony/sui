'use strict';

/* Controllers */

angular.module('SUIApplication.controllers')
    .controller('EmployeeApplicationController', ['$log', '$timeout', '$injector', '$rootScope', '$scope', '$q', 'SUIService', 'SUIServiceHTTP', 'SUIServiceCCI', 'SUIServiceExceptionHandler', '$sce',
        function($log, $timeout, $injector, $rootScope, $scope, $q, SUIService, SUIServiceHTTP, SUIServiceCCI, SUIServiceExceptionHandler, $sce) {
			$scope.handleException = function(e, trace) {
			    $scope.log("INTERNAL_JAVASCRIPT_ERROR", SUIServiceCCI.logging.LEVEL_FATAL, {
			        error: e.toString ? e.toString() : e,
			        trace: trace
			    });
			}

			SUIServiceExceptionHandler.registerExceptionHandler($scope.handleException);

			$scope.registerForLogging = function() {
			    SUIServiceCCI.logging.registerLogConfig({
			        updateCB: $scope.onLogMessageUpdate,
			        logsFilterFn: function(logRec) {
			            if (logRec.l > SUIServiceCCI.logging.LEVEL_DEBUG && (logRec.s !== "UA") || (logRec.l > SUIServiceCCI.logging.LEVEL_INFO)) {
			                //if (true) {
			                return true;
			            } else {
			                return false;
			            }
			        },
			    });
			}

			$scope.onLogMessageUpdate = function(logMsg, count) {
			}

			$scope.log = function(eventName, level, data) {
			    SUIServiceCCI.logging.log(eventName, level, {}, data);
			}			
		}]);

