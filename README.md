# README # - TODO

sUI - simple UI

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up

To setup demo app

1. mvn -Dapp.type=demo -f setup-app.xml clean install
2. mvn clean install

To clean up demo app

1. mvn -Dapp.type=demo -f setup-app.xml clean

Note: clean will create a backup of the generated/copied files as part of previous setup under /setup-app-resources/_backup/<folder with current timestamp>

To setup main app

1. mvn -Dapp.type=main -Dapp.groupId=<> -Dapp.artifactId=<> -Dapp.name=<> -Dapp.description=<> -f setup-app.xml clean install
2. mvn clean install

To clean up main app

1. mvn -Dapp.type=main -f setup-app.xml clean

Note: Once you do not need demo app anymore, delete the following folders and update root .gitignore: 

1. src/main/webapp/resources/demo-app and update the root .gitignore file to exclude src/main/webapp/resources/demo-app/*
2. sui/src/main/java/com/acme and update the root .gitignore file to exclude sui/src/main/java/com/acme/*

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact